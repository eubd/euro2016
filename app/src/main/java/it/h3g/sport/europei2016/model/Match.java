package it.h3g.sport.europei2016.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by oneenam on 10/05/16.
 */
public class Match implements Parcelable {
    int id;
    String idmatch;
    String status;
    String matchdate;
    String matchtime;

    //teamhome
    String idTeam_home;
    String teamname_home;
    String numgoal_home;
    String teamlogo_home;

    //teamaway
    String idTeam_away;
    String teamname_away;
    String numgoal_away;
    String teamlogo_away;

    //links
    String commentary;
    String matchdetails;
    String media;


    //
    String header;

    String viewType;


    public String getIdmatch() {
        return idmatch;
    }

    public void setIdmatch(String idmatch) {
        this.idmatch = idmatch;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMatchdate() {
        return matchdate;
    }

    public void setMatchdate(String matchdate) {
        this.matchdate = matchdate;
    }

    public String getMatchtime() {
        return matchtime;
    }

    public void setMatchtime(String matchtime) {
        this.matchtime = matchtime;
    }

    public String getIdTeam_home() {
        return idTeam_home;
    }

    public void setIdTeam_home(String idTeam_home) {
        this.idTeam_home = idTeam_home;
    }

    public String getTeamname_home() {
        return teamname_home;
    }

    public void setTeamname_home(String teamname_home) {
        this.teamname_home = teamname_home;
    }

    public String getNumgoal_home() {
        return numgoal_home;
    }

    public void setNumgoal_home(String numgoal_home) {
        this.numgoal_home = numgoal_home;
    }

    public String getTeamlogo_home() {
        return teamlogo_home;
    }

    public void setTeamlogo_home(String teamlogo_home) {
        this.teamlogo_home = teamlogo_home;
    }

    public String getIdTeam_away() {
        return idTeam_away;
    }

    public void setIdTeam_away(String idTeam_away) {
        this.idTeam_away = idTeam_away;
    }

    public String getTeamname_away() {
        return teamname_away;
    }

    public void setTeamname_away(String teamname_away) {
        this.teamname_away = teamname_away;
    }

    public String getNumgoal_away() {
        return numgoal_away;
    }

    public void setNumgoal_away(String numgoal_away) {
        this.numgoal_away = numgoal_away;
    }

    public String getTeamlogo_away() {
        return teamlogo_away;
    }

    public void setTeamlogo_away(String teamlogo_away) {
        this.teamlogo_away = teamlogo_away;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public String getMatchdetails() {
        return matchdetails;
    }

    public void setMatchdetails(String matchdetails) {
        this.matchdetails = matchdetails;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }


    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Match(){}
    public Match(Parcel source){

        setId(source.readInt());

        setIdmatch(source.readString());
        setStatus(source.readString());
        setMatchdate(source.readString());
        setMatchtime(source.readString());

        setIdTeam_home(source.readString());
        setTeamname_home(source.readString());
        setNumgoal_home(source.readString());
        setTeamlogo_home(source.readString());

        setIdTeam_away(source.readString());
        setTeamname_away(source.readString());
        setNumgoal_away(source.readString());
        setTeamlogo_away(source.readString());

        setCommentary(source.readString());
        setMatchdetails(source.readString());
        setMedia(source.readString());

        setHeader(source.readString());

        setViewType(source.readString());

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(getId());

        dest.writeString(getIdmatch());
        dest.writeString(getStatus());
        dest.writeString(getMatchdate());
        dest.writeString(getMatchtime());

        dest.writeString(getIdTeam_home());
        dest.writeString(getTeamname_home());
        dest.writeString(getNumgoal_home());
        dest.writeString(getTeamlogo_home());

        dest.writeString(getIdTeam_away());
        dest.writeString(getTeamname_away());
        dest.writeString(getNumgoal_away());
        dest.writeString(getTeamlogo_away());

        dest.writeString(getCommentary());
        dest.writeString(getMatchdetails());
        dest.writeString(getMedia());

        dest.writeString(getHeader());

        dest.writeString(getViewType());

    }

    public static final Creator<Match> CREATOR = new Creator<Match>() {
        public Match createFromParcel(Parcel source) {
            return new Match(source);
        }

        public Match[] newArray(int size) {
            return new Match[size];
        }
    };

}
