package it.h3g.sport.europei2016.view.diretta;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

//import com.adjust.sdk.Adjust;

import com.facebook.drawee.view.SimpleDraweeView;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.viewpagerindicator.TabPageIndicator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.model.Match;


public class LiveMatchActivity extends AppCompatActivity {
    LivePagerAdapter adapterPager ;

    private ArrayList<ArrayList<HashMap<String, String>>> leftFormation;
    private ArrayList<ArrayList<HashMap<String, String>>> rightFormation;
    private String rightTeamModule;
    private String rightTeamName;
    private String leftTeamModule;
    private String leftTeamName;

    ArrayList<HashMap<String, String>> events;
    ArrayList<HashMap<String, String>> overviewItems;
    ArrayList<HashMap<String, String>> leftTeamListItems;
    ArrayList<HashMap<String, String>> rightTeamListItems;
    ArrayList<HashMap<String, String>> scorecardItems;



    @Override
    public void onBackPressed() {
        finish();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_match);

        CommonMethods.applyCustomFont(this, findViewById(android.R.id.content));

        events = new ArrayList<>();
        overviewItems = new ArrayList<>();
        leftTeamListItems = new ArrayList<>();
        scorecardItems = new ArrayList<>();


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewTitle.setText("");

        ImageButton imageButton = (ImageButton) findViewById(R.id.imageButton);
        ImageButton imageButtonRefresh = (ImageButton) findViewById(R.id.imageButtonRefresh);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        imageButtonRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshLiveView();
            }
        });


        if (getIntent() != null) {
            Match match = getIntent().getParcelableExtra("match"); //bundle.getParcelable("match");
            if (match != null) {
                textViewTitle.setText("" + match.getTeamname_home() + " - " + match.getTeamname_away());
            }
        }

        final ViewPager vpLiveContent = (ViewPager) findViewById(R.id.vpLiveContent);
        adapterPager = new LivePagerAdapter(getSupportFragmentManager());
        vpLiveContent.setAdapter(adapterPager);



        TabPageIndicator tpiSections = (TabPageIndicator) findViewById(R.id.tpiSections);
        // Hack per la libreria di stocazzo
        tpiSections.setVisibility(View.VISIBLE);
        tpiSections.setViewPager(vpLiveContent);
        tpiSections.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                //((RelativeLayout.LayoutParams)findViewById(R.id.ivChevron).getLayoutParams()).leftMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35+(90*i), getResources().getDisplayMetrics());
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        tpiSections.setBackgroundColor(ContextCompat.getColor(LiveMatchActivity.this, R.color.colorPrimary));

        refreshLiveView();
    }


    protected void onResume() {
        super.onResume();
        //Adjust.onResume(this);
    }
    protected void onPause() {
        super.onPause();
        //Adjust.onPause();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        FlurryAgent.onStartSession(this, CommonMethods.FLURRY_ID);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }


    //loading all data here
    private void refreshLiveView() {
        ((TextView)findViewById(R.id.tvScore)).setText("---");
        ((TextView)findViewById(R.id.tvMatchStatus)).setText("CARICAMENTO...");
        ((TextView)findViewById(R.id.tvTeamHome)).setText("");
        ((TextView)findViewById(R.id.tvTeamAway)).setText("");

        //Log.d("URL", "Loading live feed at URL: " + Constants.BASE_URL + getIntent().getStringExtra("liveFeedURL"));
        //final String path = ((CampionatoApplication)getApplication()).mSerie;
        String url = getIntent().getStringExtra("liveFeedURL");

        //live feed
        String urlLiveFeed = getIntent().getStringExtra("liveFeedURL");
        System.out.println("Live path url: " + urlLiveFeed);
        String fileNameLiveFeed = urlLiveFeed.substring(urlLiveFeed.lastIndexOf('/') + 1, urlLiveFeed.length());

        //loading file
        Ion.with(LiveMatchActivity.this)
                .load(urlLiveFeed)
                .setHeader("Authorization", CommonMethods.basicAuth)
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        //System.out.println("" + downloaded + " / " + total);
                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                    }
                })
                .write(new File(getFilesDir().getPath() + File.separator + fileNameLiveFeed))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {

                        if(e == null) {
                            if (file.exists()) {
                                System.out.println("Live feed path: " + file.getAbsolutePath());


                                Ion.with(LiveMatchActivity.this)
                                        //.load("http://europei.apps.tre.it/data/europei/giornate/2_giornata/3223966_commentary.json")
                                        .load("file://"+file.getAbsolutePath())
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                //String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                if (e != null) {
                                                    Snackbar.make(findViewById(R.id.llLiveMatch), "Live Feed: "+e.toString(), Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    parseLiveFeed(result);
                                                }
                                            }

                                            //Commentary
                                            private void parseLiveFeed(JsonObject result) {
                                                events = new ArrayList<HashMap<String, String>>();
                                                JsonArray jaEvents;
                                                if(result.has("eventmatch")) {
                                                    if (result.get("eventmatch").getClass().equals(JsonObject.class)) {
                                                        jaEvents = new JsonArray();
                                                        jaEvents.add(result.getAsJsonObject("eventmatch"));
                                                    } else {
                                                        jaEvents = result.getAsJsonArray("eventmatch");
                                                    }

                                                    for (int i = 0; i < jaEvents.size(); i++) {
                                                        HashMap<String, String> event = new HashMap<String, String>();

                                                        event.put("text", jaEvents.get(i).getAsJsonObject().get("description").getAsString());
                                                        if (jaEvents.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("extratime").getAsInt() != 0)
                                                            event.put("time", jaEvents.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("minute").getAsString() + "' + " + jaEvents.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("extratime").getAsString());
                                                        else
                                                            event.put("time", jaEvents.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("minute").getAsString() + "'");
                                                        if (jaEvents.get(i).getAsJsonObject().getAsJsonObject("card").getAsJsonObject("@attributes").get("type").getAsString().equals("giallo"))
                                                            event.put("type", "yellowCard");
                                                        else if (jaEvents.get(i).getAsJsonObject().getAsJsonObject("card").getAsJsonObject("@attributes").get("type").getAsString().equals("rosso"))
                                                            event.put("type", "redCard");
                                                        else if (jaEvents.get(i).getAsJsonObject().getAsJsonObject("goal").getAsJsonObject("@attributes").get("type").getAsString().equals("tiro"))
                                                            event.put("type", "goal");
                                                        else if (jaEvents.get(i).getAsJsonObject().getAsJsonObject("substitution").getAsJsonObject("@attributes").get("type").getAsString().equals("sostituzione"))
                                                            event.put("type", "substitution");
                                                        else
                                                            event.put("type", "generic");

                                                        events.add(event);
                                                    }
                                                }

                                                Log.e(getLocalClassName(), "Total events " + events.size());
                                                //adapterPager.notifyDataSetChanged();
                                                //getSupportFragmentManager().findFragmentByTag("")
                                                if(getSupportFragmentManager().getFragments().size()>0) {
                                                    Fragment page = getSupportFragmentManager().getFragments().get(0);
                                                    //Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.vpLiveContent + ":" + 0);
                                                    if (page != null) {
                                                        ((TimeLineFragment) page).update();
                                                    }
                                                }
                                            }

                                        });



                            }
                        }else{
                            //error loading file
                        }

                    }
                });


        //Overview
        String urlOverview = getIntent().getStringExtra("overviewURL");
        //System.out.println("Overview path url: " + urlOverview);
        String fileNameOverview = urlOverview.substring(urlOverview.lastIndexOf('/') + 1, urlOverview.length());

        //loading file
        Ion.with(LiveMatchActivity.this)
                .load(urlOverview)
                .setHeader("Authorization", CommonMethods.basicAuth)
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        //System.out.println("" + downloaded + " / " + total);
                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                    }
                })
                .write(new File(getFilesDir().getPath() + File.separator + fileNameOverview))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {

                        if(e == null) {
                            if (file.exists()) {
                                //System.out.println("Overview path: " + file.getAbsolutePath());


                                Ion.with(LiveMatchActivity.this)
                                        .load("file://"+file.getAbsolutePath())
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                //String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                if (e != null) {
                                                    Snackbar.make(findViewById(R.id.llLiveMatch), "Overview "+e.toString(), Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    parseMatchOverview(result);
                                                }
                                            }

                                            //here
                                            private synchronized void parseMatchOverview(JsonObject result) {
                                                overviewItems = new ArrayList<HashMap<String, String>>();
                                                HashMap<String, String> overviewItem = new HashMap<String, String>();

                                                // Gol
                                                overviewItem.put("type", "separator");
                                                overviewItem.put("text", "Gol");
                                                overviewItems.add(overviewItem);

                                                JsonArray leftGoals = new JsonArray();
                                                if(result.has("teamhome")) {
                                                    if (result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("goals").get("goal") != null) {
                                                        if (result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("goals").get("goal").getClass() == JsonObject.class) {
                                                            leftGoals.add(result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("goals").getAsJsonObject("goal"));
                                                        } else {
                                                            leftGoals = result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("goals").getAsJsonArray("goal");
                                                        }
                                                    }
                                                }

                                                JsonArray rightGoals = new JsonArray();
                                                if(result.has("teamaway")) {
                                                    if (result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("goals").get("goal") != null) {
                                                        if (result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("goals").get("goal").getClass() == JsonObject.class) {
                                                            rightGoals.add(result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("goals").getAsJsonObject("goal"));
                                                        } else {
                                                            rightGoals = result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("goals").getAsJsonArray("goal");
                                                        }
                                                    }
                                                }
                                                for (int i = 0; i < Math.max(leftGoals.size(), rightGoals.size()); i++) {
                                                    overviewItem = new HashMap<String, String>();
                                                    overviewItem.put("type", "goal");
                                                    overviewItem.put("leftName", "");
                                                    overviewItem.put("leftTime", "");
                                                    overviewItem.put("rightName", "");
                                                    overviewItem.put("rightTime", "");
                                                    // Abbiamo un gol a sinistra?
                                                    if (i < leftGoals.size()) {
                                                        String name = leftGoals.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString();
                                                        if (name.length() > 0)
                                                            name = name.substring(0, 1);
                                                        String surname = leftGoals.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString();
                                                        overviewItem.put("leftName", name + "." + surname);
                                                        System.err.println(surname);

                                                        overviewItem.put("leftTime", leftGoals.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("minute").getAsString() + "'");
                                                    }

                                                    if (i < rightGoals.size()) {
                                                        String name = rightGoals.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString();
                                                        if (name.length() > 0)
                                                            name = name.substring(0, 1);
                                                        overviewItem.put("rightName", name + "." + rightGoals.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                        overviewItem.put("rightTime", rightGoals.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("minute").getAsString() + "'");
                                                    }
                                                    overviewItems.add(overviewItem);
                                                }

                                                overviewItem = new HashMap<String, String>();

                                                // Cartellini
                                                overviewItem.put("type", "separator");
                                                overviewItem.put("text", "Cartellini");
                                                overviewItems.add(overviewItem);

                                                //TODO
                                                JsonArray leftCards = new JsonArray();
                                                if(result.has("teamhome")) {
                                                    if (result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("cards").get("card") != null) {
                                                        if (result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("cards").get("card").getClass() == JsonObject.class) {
                                                            leftCards.add(result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("cards").getAsJsonObject("card"));
                                                        } else {
                                                            leftCards = result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("cards").getAsJsonArray("card");
                                                        }
                                                    }
                                                }

                                                JsonArray rightCards = new JsonArray();
                                                if(result.has("teamaway")) {
                                                    if (result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("cards").get("card") != null) {
                                                        if (result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("cards").get("card").getClass() == JsonObject.class) {
                                                            rightCards.add(result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("cards").getAsJsonObject("card"));
                                                        } else {
                                                            rightCards = result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("cards").getAsJsonArray("card");
                                                        }
                                                    }
                                                }

                                                for (int i = 0; i < Math.max(leftCards.size(), rightCards.size()); i++) {
                                                    overviewItem = new HashMap<String, String>();
                                                    overviewItem.put("type", "card");
                                                    overviewItem.put("leftColor", "");
                                                    overviewItem.put("leftName", "");
                                                    overviewItem.put("leftTime", "");
                                                    overviewItem.put("rightColor", "");
                                                    overviewItem.put("rightName", "");
                                                    overviewItem.put("rightTime", "");

                                                    // Abbiamo un gol a sinistra?
                                                    if (i < leftCards.size()) {
                                                        String name = leftCards.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString();
                                                        if (name.length() > 0)
                                                            name = name.substring(0, 1);
                                                        overviewItem.put("leftName", name + "." + leftCards.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                        overviewItem.put("leftTime", leftCards.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("minute").getAsString() + "'");
                                                        Log.d("VAFFANCULO", "PERCHE??? " + leftCards.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("type").getAsString());
                                                        if (leftCards.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("type").getAsString().equals("1AM")) {
                                                            overviewItem.put("leftColor", "yellow");
                                                        } else {
                                                            overviewItem.put("leftColor", "red");
                                                        }
                                                    }

                                                    if (i < rightCards.size()) {
                                                        String name = rightCards.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString();
                                                        if (name.length() > 0)
                                                            name = name.substring(0, 1);
                                                        overviewItem.put("rightName", name + "." + rightCards.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                        overviewItem.put("rightTime", rightCards.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("minute").getAsString() + "'");
                                                        if (rightCards.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("type").getAsString().equals("1AM")) {
                                                            overviewItem.put("rightColor", "yellow");
                                                        } else {
                                                            overviewItem.put("rightColor", "red");
                                                        }
                                                    }
                                                    overviewItems.add(overviewItem);
                                                }

                                                overviewItem = new HashMap<String, String>();

                                                // Sostituzioni
                                                overviewItem.put("type", "separator");
                                                overviewItem.put("text", "Sostituzioni");
                                                overviewItems.add(overviewItem);

                                                //TODO
                                                JsonArray leftSubstitutions = new JsonArray();
                                                if(result.has("teamhome")) {
                                                    if (result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("substitutions").get("substitution") != null) {
                                                        if (result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("substitutions").get("substitution").getClass() == JsonObject.class) {
                                                            leftSubstitutions.add(result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("substitutions").getAsJsonObject("substitution"));
                                                        } else {
                                                            leftSubstitutions = result.getAsJsonObject("teamhome").getAsJsonObject("general").getAsJsonObject("substitutions").getAsJsonArray("substitution");
                                                        }
                                                    }
                                                }

                                                JsonArray rightSubstitutions = new JsonArray();
                                                if(result.has("teamaway")) {
                                                    if (result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("substitutions").get("substitution") != null) {
                                                        if (result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("substitutions").get("substitution").getClass() == JsonObject.class) {
                                                            rightSubstitutions.add(result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("substitutions").getAsJsonObject("substitution"));
                                                        } else {
                                                            rightSubstitutions = result.getAsJsonObject("teamaway").getAsJsonObject("general").getAsJsonObject("substitutions").getAsJsonArray("substitution");
                                                        }
                                                    }
                                                }

                                                for (int i = 0; i < Math.max(leftSubstitutions.size(), rightSubstitutions.size()); i++) {
                                                    overviewItem = new HashMap<String, String>();
                                                    overviewItem.put("type", "substitution");
                                                    overviewItem.put("leftType", "");
                                                    overviewItem.put("leftName", "");
                                                    overviewItem.put("leftTime", "");
                                                    overviewItem.put("rightType", "");
                                                    overviewItem.put("rightName", "");
                                                    overviewItem.put("rightTime", "");
                                                    // Abbiamo una sostituzione a sinistra?
                                                    if (i < leftSubstitutions.size()) {
                                                        String name = leftSubstitutions.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString();
                                                        if (name.length() > 0)
                                                            name = name.substring(0, 1);
                                                        overviewItem.put("leftName", name + "." + leftSubstitutions.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                        overviewItem.put("leftTime", leftSubstitutions.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("minute").getAsString() + "'");
                                                        if (leftSubstitutions.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("type").getAsString().equals("IN")) {
                                                            overviewItem.put("leftType", "in");
                                                        } else if (leftSubstitutions.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("type").getAsString().equals("OUT")) {
                                                            overviewItem.put("leftType", "out");
                                                        }
                                                    }

                                                    if (i < rightSubstitutions.size()) {
                                                        String name = rightSubstitutions.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString();
                                                        if (name.length() > 0)
                                                            name = name.substring(0, 1);
                                                        overviewItem.put("rightName", name + "." + rightSubstitutions.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                        overviewItem.put("rightTime", rightSubstitutions.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("minute").getAsString() + "'");
                                                        if (rightSubstitutions.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("type").getAsString().equals("IN")) {
                                                            overviewItem.put("rightType", "in");
                                                        } else if (rightSubstitutions.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("type").getAsString().equals("OUT")) {
                                                            overviewItem.put("rightType", "out");
                                                        }
                                                    }
                                                    overviewItems.add(overviewItem);
                                                }

                                                if(getSupportFragmentManager().getFragments().size()>1) {
                                                    Fragment page = getSupportFragmentManager().getFragments().get(1); //getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.vpLiveContent + ":" + 1);
                                                    if (page != null) {
                                                        ((OverviewFragment) page).update();
                                                    }
                                                }

                                                leftTeamListItems = new ArrayList<HashMap<String, String>>();

                                                //Log.d("JSON", "Exception: " + e + " Result: " + result);
                                                //TODO
                                                JsonArray leftPlayers = new JsonArray();
                                                if(result.has("teamhome")) {
                                                    if (result.getAsJsonObject("teamhome").getAsJsonObject("players").get("player") != null) {
                                                        if (result.getAsJsonObject("teamhome").getAsJsonObject("players").get("player").getClass() == JsonObject.class) {
                                                            leftPlayers.add(result.getAsJsonObject("teamhome").getAsJsonObject("players").getAsJsonObject("player"));
                                                        } else {
                                                            leftPlayers = result.getAsJsonObject("teamhome").getAsJsonObject("players").getAsJsonArray("player");
                                                        }
                                                    }
                                                }

                                                HashMap<String, String> formationItem = new HashMap<String, String>();
                                                formationItem.put("type", "separator");
                                                formationItem.put("text", "Riserve");

                                                leftTeamListItems.add(formationItem);
                                                String leftCoach = "";

                                                //TODO
                                                ArrayList<String> leftTeamRows = new ArrayList<String>();
                                                if(result.has("teamhome")) {
                                                    leftTeamModule = result.getAsJsonObject("teamhome").getAsJsonObject("@attributes").get("module").getAsString();
                                                    if (leftTeamModule.length() > 7)
                                                        leftTeamModule = leftTeamModule.substring(0, 7);
                                                    leftTeamName = result.getAsJsonObject("teamhome").getAsJsonObject("@attributes").get("team").getAsString();
                                                    leftTeamRows.addAll(Arrays.asList(result.getAsJsonObject("teamhome").getAsJsonObject("@attributes").get("module").getAsString().split("-")));
                                                }
                                                leftTeamRows.add(0, "1");

                                                int fillingRow = 0;

                                                ArrayList<HashMap<String, String>> goalies = new ArrayList<HashMap<String, String>>();
                                                ArrayList<HashMap<String, String>> defense = new ArrayList<HashMap<String, String>>();
                                                ArrayList<HashMap<String, String>> midfield = new ArrayList<HashMap<String, String>>();
                                                ArrayList<HashMap<String, String>> forward = new ArrayList<HashMap<String, String>>();
                                                ArrayList<HashMap<String, String>> centerForward = new ArrayList<HashMap<String, String>>();

                                                leftFormation = new ArrayList<ArrayList<HashMap<String, String>>>();

                                                leftFormation.add(goalies);
                                                leftFormation.add(defense);
                                                leftFormation.add(midfield);
                                                leftFormation.add(forward);
                                                if (leftTeamRows.size() > 4)
                                                    leftFormation.add(centerForward);


                                                for (int i=0; i<leftPlayers.size(); i++)
                                                {
                                                    if (leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("headline").getAsString().equals("S"))
                                                    {
                                                        if (leftTeamModule.equals(""))
                                                        {
                                                            if (leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("role").getAsString().equals("ALL"))
                                                            {
                                                                leftCoach = leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString();
                                                            } else {
                                                                formationItem = new HashMap<String, String>();
                                                                formationItem.put("type", "player");
                                                                formationItem.put("leftName", leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                                leftTeamListItems.add(formationItem);
                                                            }
                                                        } else {
                                                            if (leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("role").getAsString().equals("ALL")) {
                                                                leftCoach = leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString();
                                                            } else {
                                                                if (fillingRow < leftFormation.size()) {
                                                                    if (leftFormation.get(fillingRow).size() < Integer.valueOf(leftTeamRows.get(fillingRow))) {
                                                                        formationItem = new HashMap<String, String>();
                                                                        formationItem.put("type", "player");
                                                                        formationItem.put("leftName", leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                                        formationItem.put("leftNumber", leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("number").getAsString());

                                                                        if (fillingRow < leftFormation.size())
                                                                            leftFormation.get(fillingRow).add(formationItem);
                                                                    } else {
                                                                        fillingRow++;
                                                                        formationItem = new HashMap<String, String>();
                                                                        formationItem.put("type", "player");
                                                                        formationItem.put("leftName", leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                                        formationItem.put("leftNumber", leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("number").getAsString());

                                                                        if (fillingRow < leftFormation.size())
                                                                            leftFormation.get(fillingRow).add(formationItem);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        formationItem = new HashMap<String, String>();
                                                        formationItem.put("type", "player");
                                                        formationItem.put("leftName", leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                        leftTeamListItems.add(formationItem);
                                                    }
                                                }

                                                formationItem = new HashMap<String, String>();
                                                formationItem.put("type", "separator");
                                                formationItem.put("text", "Allenatore");
                                                leftTeamListItems.add(formationItem);
                                                formationItem = new HashMap<String, String>();
                                                formationItem.put("type", "player");
                                                formationItem.put("leftName", leftCoach);
                                                leftTeamListItems.add(formationItem);

                                                // Seconda squadra

                                                JsonArray rightPlayers = new JsonArray();
                                                //TODO
                                                if(result.has("teamaway")) {
                                                    if (result.getAsJsonObject("teamaway").getAsJsonObject("players").get("player") != null) {
                                                        if (result.getAsJsonObject("teamaway").getAsJsonObject("players").get("player").getClass() == JsonObject.class) {
                                                            rightPlayers.add(result.getAsJsonObject("teamaway").getAsJsonObject("players").getAsJsonObject("player"));
                                                        } else {
                                                            rightPlayers = result.getAsJsonObject("teamaway").getAsJsonObject("players").getAsJsonArray("player");
                                                        }
                                                    }
                                                }

                                                rightTeamListItems = new ArrayList<>();

                                                formationItem = new HashMap<>();
                                                formationItem.put("type", "separator");
                                                formationItem.put("text", "Riserve");

                                                rightTeamListItems.add(formationItem);
                                                String rightCoach = "";

                                                ArrayList<String> rightTeamRows = new ArrayList<String>();

                                                if(result.has("teamaway")) {
                                                    rightTeamModule = result.getAsJsonObject("teamaway").getAsJsonObject("@attributes").get("module").getAsString();
                                                    if (rightTeamModule.length() > 7)
                                                        rightTeamModule = rightTeamModule.substring(0, 7);
                                                    rightTeamName = result.getAsJsonObject("teamaway").getAsJsonObject("@attributes").get("team").getAsString();
                                                    rightTeamRows.addAll(Arrays.asList(result.getAsJsonObject("teamaway").getAsJsonObject("@attributes").get("module").getAsString().split("-")));

                                                }
                                                rightTeamRows.add(0, "1");

                                                fillingRow = 0;

                                                goalies = new ArrayList<HashMap<String, String>>();
                                                defense = new ArrayList<HashMap<String, String>>();
                                                midfield = new ArrayList<HashMap<String, String>>();
                                                forward = new ArrayList<HashMap<String, String>>();
                                                centerForward = new ArrayList<HashMap<String, String>>();

                                                rightFormation = new ArrayList<ArrayList<HashMap<String, String>>>();

                                                rightFormation.add(goalies);
                                                rightFormation.add(defense);
                                                rightFormation.add(midfield);
                                                rightFormation.add(forward);
                                                if (rightTeamRows.size() > 4)
                                                    rightFormation.add(centerForward);


                                                for (int i=0; i<rightPlayers.size(); i++)
                                                {
                                                    if (rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("headline").getAsString().equals("S"))
                                                    {
                                                        if (rightTeamModule.equals(""))
                                                        {
                                                            if (rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("role").getAsString().equals("ALL"))
                                                            {
                                                                rightCoach = rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString();
                                                            } else {
                                                                formationItem = new HashMap<String, String>();
                                                                formationItem.put("type", "player");
                                                                formationItem.put("leftName", rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                                rightTeamListItems.add(formationItem);
                                                            }
                                                        } else {
                                                            if (rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("role").getAsString().equals("ALL")) {
                                                                rightCoach = rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString();
                                                            } else {
                                                                if (fillingRow < rightFormation.size()) {
                                                                    if (rightFormation.get(fillingRow).size() < Integer.valueOf(rightTeamRows.get(fillingRow))) {
                                                                        formationItem = new HashMap<String, String>();
                                                                        formationItem.put("type", "player");
                                                                        formationItem.put("leftName", rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                                        formationItem.put("leftNumber", rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("number").getAsString());

                                                                        if (fillingRow < rightFormation.size())
                                                                            rightFormation.get(fillingRow).add(formationItem);
                                                                    } else {
                                                                        fillingRow++;
                                                                        formationItem = new HashMap<String, String>();
                                                                        formationItem.put("type", "player");
                                                                        formationItem.put("leftName", rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                                        formationItem.put("leftNumber", rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("number").getAsString());

                                                                        if (fillingRow < rightFormation.size())
                                                                            rightFormation.get(fillingRow).add(formationItem);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        formationItem = new HashMap<String, String>();
                                                        formationItem.put("type", "player");
                                                        formationItem.put("leftName", rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString() + " " + rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                        rightTeamListItems.add(formationItem);
                                                    }
                                                }

                                                formationItem = new HashMap<String, String>();
                                                formationItem.put("type", "separator");
                                                formationItem.put("text", "Allenatore");
                                                rightTeamListItems.add(0, formationItem);
                                                formationItem = new HashMap<String, String>();
                                                formationItem.put("type", "player");
                                                formationItem.put("leftName", rightCoach);
                                                rightTeamListItems.add(1, formationItem);

                                                Collections.reverse(rightFormation);



                                                if(getSupportFragmentManager().getFragments().size()>2) {
                                                    Fragment page = getSupportFragmentManager().getFragments().get(2); //getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.vpLiveContent + ":" + 2);
                                                    if (page != null) {
                                                        ((FormationsFragment) page).update();
                                                    }
                                                }

                                                scorecardItems = new ArrayList<HashMap<String, String>>();
                                                HashMap<String, String> scorecardItem = new HashMap<String, String>();
                                                scorecardItem.put("type", "separator");

                                                if(result.has("teamhome"))
                                                    scorecardItem.put("text", result.getAsJsonObject("teamhome").getAsJsonObject("@attributes").get("team").getAsString());

                                                scorecardItems.add(scorecardItem);

                                                for (int i = 0; i < leftPlayers.size(); i++) {
                                                    String vote = leftPlayers.get(i).getAsJsonObject().getAsJsonObject("vote").getAsJsonObject("@attributes").get("value").getAsString();
                                                    if (!vote.equals("0")) {
                                                        scorecardItem = new HashMap<String, String>();
                                                        scorecardItem.put("type", "scorecard");
                                                        scorecardItem.put("number", leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("number").getAsString());
                                                        String name = leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString();
                                                        if (name.length() > 0)
                                                            name = name.substring(0, 1);
                                                        scorecardItem.put("name", name + "." + leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                        scorecardItem.put("role", leftPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("role").getAsString());
                                                        scorecardItem.put("score", leftPlayers.get(i).getAsJsonObject().getAsJsonObject("vote").getAsJsonObject("@attributes").get("value").getAsString());
                                                        scorecardItems.add(scorecardItem);
                                                    }
                                                }

                                                scorecardItem = new HashMap<String, String>();
                                                scorecardItem.put("type", "separator");
                                                if(result.has("teamaway"))
                                                    scorecardItem.put("text", result.getAsJsonObject("teamaway").getAsJsonObject("@attributes").get("team").getAsString());
                                                scorecardItems.add(scorecardItem);

                                                for (int i = 0; i < rightPlayers.size(); i++) {
                                                    String vote = rightPlayers.get(i).getAsJsonObject().getAsJsonObject("vote").getAsJsonObject("@attributes").get("value").getAsString();
                                                    if (!vote.equals("0")) {
                                                        scorecardItem = new HashMap<String, String>();
                                                        scorecardItem.put("type", "scorecard");
                                                        scorecardItem.put("number", rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("number").getAsString());
                                                        String name = rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("name").getAsString();
                                                        if (name.length() > 0)
                                                            name = name.substring(0, 1);
                                                        scorecardItem.put("name", name + "." + rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                                                        scorecardItem.put("role", rightPlayers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("role").getAsString());
                                                        scorecardItem.put("score", rightPlayers.get(i).getAsJsonObject().getAsJsonObject("vote").getAsJsonObject("@attributes").get("value").getAsString());
                                                        scorecardItems.add(scorecardItem);
                                                    }
                                                }

                                                if(getSupportFragmentManager().getFragments().size()==3) {
                                                    Fragment page = getSupportFragmentManager().getFragments().get(3);//getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.vpLiveContent + ":" + 3);
                                                    if (page != null) {
                                                        ((ScorecardsFragment) page).update();
                                                    }
                                                }


                                                // Impostiamo i dettagli della partita
                                                if(result.has("@attributes")) {
                                                    JsonObject attributes = result.getAsJsonObject("@attributes");
                                                    if(attributes.has("status")) {
                                                        if (attributes.get("status").getAsString().equals("FORMAZIONI")) {
                                                            ((TextView) findViewById(R.id.tvScore)).setText(" - ");
                                                        } else {
                                                            ((TextView) findViewById(R.id.tvScore)).setText(leftGoals.size() + " - " + rightGoals.size());
                                                        }

                                                        ((TextView) findViewById(R.id.tvMatchStatus)).setText(attributes.get("status").getAsString());
                                                    }

                                                }

                                                if(result.has("teamhome")) {
                                                    String leftTeam = result.getAsJsonObject("teamhome").getAsJsonObject("@attributes").get("team").getAsString();
                                                    leftTeamName = leftTeam;
                                                    ((TextView) findViewById(R.id.tvTeamHome)).setText(leftTeam);
                                                    String urlHome = CommonMethods.BASE_URL + result.getAsJsonObject("teamhome").getAsJsonObject("@attributes").get("logo").getAsString().replace("_t", "");
                                                    if (urlHome != null) {
                                                        Uri uri = Uri.parse(urlHome);
                                                        ((SimpleDraweeView)findViewById(R.id.ivLeftTeamLogo)).setImageURI(uri);
                                                    }
                                                }
                                                //ImageLoader.getInstance().displayImage(Constants.BASE_URL + result.getAsJsonObject("teamaway").getAsJsonObject("@attributes").get("logo").getAsString().replace("_t", ""), ((ImageView) findViewById(R.id.ivRightTeamLogo)));
                                                if(result.has("teamaway")) {
                                                    String rightTeam = result.getAsJsonObject("teamaway").getAsJsonObject("@attributes").get("team").getAsString();
                                                    rightTeamName = rightTeam;
                                                    ((TextView) findViewById(R.id.tvTeamAway)).setText(rightTeam);
                                                    String urlAway = CommonMethods.BASE_URL + result.getAsJsonObject("teamaway").getAsJsonObject("@attributes").get("logo").getAsString().replace("_t", "");
                                                    if (urlAway != null) {
                                                        Uri uri = Uri.parse(urlAway);
                                                        ((SimpleDraweeView)findViewById(R.id.ivRightTeamLogo)).setImageURI(uri);
                                                    }
                                                }
                                            }

                                        });



                            }
                        }else{
                            //error loading file
                        }

                    }
                });

    }

    //pager adapter
    private class LivePagerAdapter extends FragmentStatePagerAdapter {
        public LivePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // Carichiamo il Fragment corretto a seconda della sezione selezionata nel pager
            if (position == 0) {
                TimeLineFragment fragment = new TimeLineFragment();
                fragment.update();
                return fragment;
            } else if (position == 1) {
                OverviewFragment fragment = new OverviewFragment();
                fragment.update();
                return fragment;
            } else if (position == 2) {
                FormationsFragment fragment = new FormationsFragment();
                fragment.update();
                return fragment;
            } else {
                ScorecardsFragment fragment = new ScorecardsFragment();
                fragment.update();
                return fragment;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position)
            {
                case 0:
                    return "Cronaca";
                case 1:
                    return "Tabellino";
                case 2:
                    return "Formazioni";
                default:
                    return "Pagelle";
            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }


    //Cronaca
    public static class TimeLineFragment extends Fragment {

        class EventAdapter extends BaseAdapter {
            private LayoutInflater mInflater;
            ArrayList<HashMap<String, String>> items;

            public EventAdapter(Context context, ArrayList<HashMap<String, String>> items) {
                mInflater = LayoutInflater.from(context);
                this.items = items;
            }

            @Override
            public int getCount() {
                return items.size() ;
            }

            @Override
            public Object getItem(int i) {
                return items.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                // Carichiamo il layout opportuno per l'elemento (primo, ultimo o in mezzo)

                if (position == 0 && getCount() == 1) {
                    view = mInflater.inflate(R.layout.diretta_cronaca_row_first, null);
                }else if (position == getCount()) {
                    view = mInflater.inflate(R.layout.diretta_cronaca_row_last, null);
                }else
                    view = mInflater.inflate(R.layout.diretta_cronaca_row, null);



                CommonMethods.applyCustomFont(getActivity(), view);

                TextView tvTime = (TextView) view.findViewById(R.id.tvTime);
                ImageView ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
                TextView tvText = (TextView) view.findViewById(R.id.tvText);

                int adjustedPosition = position;
                //int adjustedPosition = position - adAvailable;

                tvTime.setText(items.get(adjustedPosition).get("time"));
                String icon = items.get(adjustedPosition).get("type");
                if (icon.equals("yellowCard"))
                {
                    ivIcon.setImageResource(R.drawable.cartellino_giallo);
                } else if (icon.equals("redCard")) {
                    ivIcon.setImageResource(R.drawable.cartellino_rosso);
                } else if (icon.equals("substitution")) {
                    ivIcon.setImageResource(R.drawable.ico_sostituizione);
                } else if (icon.equals("goal")) {
                    ivIcon.setImageResource(R.drawable.palla);
                } else if (icon.equals("generic")) {
                    ivIcon.setImageResource(R.drawable.cartellino_grigio);
                }

                tvText.setText(items.get(adjustedPosition).get("text"));

                return view;
            }

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(
                    R.layout.fragment_live_timeline, container, false);
            CommonMethods.applyCustomFont(getActivity(), rootView);

            return rootView;
        }

        public void update() {
            if (getView() != null) {
                ListView lvEventTimeline = (ListView) getView().findViewById(R.id.lvEventTimeline);
                lvEventTimeline.setAdapter(new EventAdapter(getActivity(), ((LiveMatchActivity) getActivity()).events));
            }
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);



            if (getView() != null) {
                System.err.println("Cronaca");

                ListView lvEventTimeline = (ListView) getView().findViewById(R.id.lvEventTimeline);
                EventAdapter adapter = new EventAdapter(getActivity(), ((LiveMatchActivity)getActivity()).events);
                lvEventTimeline.setAdapter(adapter);
                lvEventTimeline.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
            }
        }
    }


    //Tabellino
    public static class OverviewFragment extends Fragment {

        //TeadsVideo teadsVideo;

        public void update() {
            if (getView() != null) {
                ListView lvEventTimeline = (ListView) getView().findViewById(R.id.lvEventTimeline);
                lvEventTimeline.setAdapter(new OverviewAdapter(getActivity(), ((LiveMatchActivity) getActivity()).overviewItems));
            }
        }

        class OverviewAdapter extends ArrayAdapter<HashMap<String, String>> {
            private LayoutInflater mInflater;

            public OverviewAdapter(Context context, List<HashMap<String, String>> items) {
                super(context, 0, items);
                mInflater = LayoutInflater.from(context);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                //Log.d("Test", "Item: " + getItem(position));
                if (getItem(position).get("type").equals("separator")) {
                    view = mInflater.inflate(R.layout.list_header, null);
                    CommonMethods.applyCustomFont(getActivity(), view);

                    TextView tvHeaderText = (TextView)view.findViewById(R.id.tvHeaderText);
                    tvHeaderText.setText(getItem(position).get("text"));
                } else if (getItem(position).get("type").equals("goal")) {
                    view = mInflater.inflate(R.layout.overview_item_goal, null);
                    CommonMethods.applyCustomFont(getActivity(), (ViewGroup)view);

                    TextView tvLeftGoalName = (TextView) view.findViewById(R.id.tvLeftGoalName);
                    TextView tvLeftGoalTime = (TextView) view.findViewById(R.id.tvLeftGoalTime);
                    TextView tvRightGoalName = (TextView) view.findViewById(R.id.tvRightGoalName);
                    TextView tvRightGoalTime = (TextView) view.findViewById(R.id.tvRightGoalTime);

                    tvLeftGoalName.setText(getItem(position).get("leftName"));
                    tvLeftGoalTime.setText(getItem(position).get("leftTime"));
                    tvRightGoalName.setText(getItem(position).get("rightName"));
                    tvRightGoalTime.setText(getItem(position).get("rightTime"));
                } else if (getItem(position).get("type").equals("card")) {
                    view = mInflater.inflate(R.layout.overview_item_card, null);

                    TextView tvLeftCardName = (TextView) view.findViewById(R.id.tvLeftCardName);
                    TextView tvLeftCardTime = (TextView) view.findViewById(R.id.tvLeftCardTime);
                    ImageView ivLeftCardIcon = (ImageView) view.findViewById(R.id.ivLeftCardIcon);
                    if (getItem(position).get("leftColor").equals("yellow"))
                        ivLeftCardIcon.setImageResource(R.drawable.ico_cartellino_giallo);
                    else if (getItem(position).get("leftColor").equals("red"))
                        ivLeftCardIcon.setImageResource(R.drawable.ico_cartellino_rosso);
                    else
                        ivLeftCardIcon.setImageDrawable(null);
                    tvLeftCardName.setText(getItem(position).get("leftName"));
                    tvLeftCardTime.setText(getItem(position).get("leftTime"));

                    TextView tvRightCardName = (TextView) view.findViewById(R.id.tvRightCardName);
                    TextView tvRightCardTime = (TextView) view.findViewById(R.id.tvRightCardTime);
                    ImageView ivRightCardIcon = (ImageView) view.findViewById(R.id.ivRightCardIcon);
                    if (getItem(position).get("rightColor").equals("yellow"))
                        ivRightCardIcon.setImageResource(R.drawable.ico_cartellino_giallo);
                    else if (getItem(position).get("rightColor").equals("red"))
                        ivRightCardIcon.setImageResource(R.drawable.ico_cartellino_rosso);
                    else
                        ivRightCardIcon.setImageDrawable(null);
                    tvRightCardName.setText(getItem(position).get("rightName"));
                    tvRightCardTime.setText(getItem(position).get("rightTime"));
                } else {
                    view = mInflater.inflate(R.layout.overview_item_substitution, null);

                    ImageView ivLeftSubstitutionIcon = (ImageView) view.findViewById(R.id.ivLeftSubstitutionIcon);
                    TextView tvLeftSubstitutionName = (TextView) view.findViewById(R.id.tvLeftSubstitutionName);
                    TextView tvLeftSubstitutionTime = (TextView) view.findViewById(R.id.tvLeftSubstitutionTime);

                    if (getItem(position).get("leftType").equals("in"))
                    {
                        ivLeftSubstitutionIcon.setImageResource(R.drawable.ico_sostituizione_rosso);
                    } else if (getItem(position).get("leftType").equals("out")) {
                        ivLeftSubstitutionIcon.setImageResource(R.drawable.ico_sostituizione_verde);
                    } else {
                        ivLeftSubstitutionIcon.setImageDrawable(null);
                    }
                    tvLeftSubstitutionName.setText(getItem(position).get("leftName"));
                    tvLeftSubstitutionTime.setText(getItem(position).get("leftTime"));

                    ImageView ivRightSubstitutionIcon = (ImageView) view.findViewById(R.id.ivRightSubstitutionIcon);
                    TextView tvRightSubstitutionName = (TextView) view.findViewById(R.id.tvRightSubstitutionName);
                    TextView tvRightSubstitutionTime = (TextView) view.findViewById(R.id.tvRightSubstitutionTime);

                    if (getItem(position).get("rightType").equals("in"))
                    {
                        ivRightSubstitutionIcon.setImageResource(R.drawable.ico_sostituizione_rosso);
                    } else if (getItem(position).get("rightType").equals("out")) {
                        ivRightSubstitutionIcon.setImageResource(R.drawable.ico_sostituizione_verde);
                    } else {
                        ivRightSubstitutionIcon.setImageDrawable(null);
                    }
                    tvRightSubstitutionName.setText(getItem(position).get("rightName"));
                    tvRightSubstitutionTime.setText(getItem(position).get("rightTime"));
                }

                return view;
            }

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(
                    R.layout.fragment_live_timeline, container, false);
            CommonMethods.applyCustomFont(getActivity(), rootView);

            return rootView;
        }

        @Override
        public void onActivityCreated( Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            if (getView() != null) {
                //TeadsConfiguration teadsConfig = new TeadsConfiguration();
                //teadsConfig.adPosition = 12;



                ListView lvEventTimeline = (ListView) getView().findViewById(R.id.lvEventTimeline);

                /*teadsVideo = new TeadsVideo.TeadsVideoBuilder(getActivity(), getString(R.string.Cronaca_Inread))
                        .viewGroup(lvEventTimeline)
                        .containerType(TeadsContainerType.inRead)
                        .configuration(teadsConfig)
                        //.eventListener(this)
                        .build();*/

                lvEventTimeline.setAdapter(new OverviewAdapter(getActivity(), ((LiveMatchActivity) getActivity()).overviewItems));



            }
        }

        /*@Override
        public void onResume() {
            super.onResume();
            if(teadsVideo.isLoaded()){
                teadsVideo.load();
                teadsVideo.onResume();
            }
        }

        @Override
        public void onPause() {
            super.onPause();
            if(teadsVideo!=null)
                teadsVideo.onPause();
        }*/
    }

    //formazioni
    public static class FormationsFragment extends Fragment {

        //private ArrayList<HashMap<String, String>> mItems;

        public void update() {
            if (getView() != null) {

                //((TextView)getView().findViewById(R.id.tvLeftTeamName)).setText("FUCK");
                ((TextView)getView().findViewById(R.id.tvLeftTeamName)).setText(((LiveMatchActivity) getActivity()).leftTeamName);
                ((TextView)getView().findViewById(R.id.tvLeftTeamModule)).setText(((LiveMatchActivity) getActivity()).leftTeamModule);

                ((TextView)getView().findViewById(R.id.tvRightTeamName)).setText(((LiveMatchActivity) getActivity()).rightTeamName);
                ((TextView)getView().findViewById(R.id.tvRightTeamModule)).setText(((LiveMatchActivity) getActivity()).rightTeamModule);

                final ListView lvLeftPlayers = (ListView) getView().findViewById(R.id.lvFirstTeamFormation);
                lvLeftPlayers.setAdapter(new FormationsAdapter(getActivity(), ((LiveMatchActivity) getActivity()).leftTeamListItems));
                adjustListViewHeight(lvLeftPlayers);


                //TODO
                ListView lvRightPlayers = (ListView) getView().findViewById(R.id.lvSecondTeamFormation);
                if( ((LiveMatchActivity) getActivity()).rightTeamListItems != null){
                    lvRightPlayers.setAdapter(new FormationsAdapter(getActivity(), ((LiveMatchActivity) getActivity()).rightTeamListItems));
                    adjustListViewHeight(lvRightPlayers);
                }

                final ScrollView svFormations = (ScrollView)getView().findViewById(R.id.svFormations);
                //Log.d("Shit", "Fuck: " + svFormations.getScrollY());

                if (svFormations.getScrollY() == 0)
                {
                    svFormations.post(new Runnable() {
                        public void run() {
                            Resources r = getResources();
                            float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 183, r.getDisplayMetrics());
                            svFormations.scrollTo(0, (int) (lvLeftPlayers.getMeasuredHeight() + px));
                        }
                    });
                }

                for (int i = 0; i < ((LiveMatchActivity) getActivity()).leftFormation.size(); i++)
                {
                    for (int j = 0; j < ((LiveMatchActivity) getActivity()).leftFormation.get(i).size(); j++)
                    {
                        HashMap<String, String> player = ((LiveMatchActivity) getActivity()).leftFormation.get(i).get(j);
                        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
                        View playerView = inflater.inflate(R.layout.field_player, null);
                        CommonMethods.applyCustomFont(getActivity(), (ViewGroup)playerView);

                        ((TextView)playerView.findViewById(R.id.tvPlayerNumber)).setText(player.get("leftNumber"));
                        ((TextView)playerView.findViewById(R.id.tvPlayerName)).setText(player.get("leftName"));

                        switch (i)
                        {
                            case 0:
                                ((LinearLayout)getView().findViewById(R.id.llLeftTeamRow1)).addView(playerView);
                                break;
                            case 1:
                                ((LinearLayout)getView().findViewById(R.id.llLeftTeamRow2)).addView(playerView);
                                break;
                            case 2:
                                ((LinearLayout)getView().findViewById(R.id.llLeftTeamRow3)).addView(playerView);
                                break;
                            case 3:
                                ((LinearLayout)getView().findViewById(R.id.llLeftTeamRow4)).addView(playerView);
                                break;
                            case 4:
                                ((LinearLayout)getView().findViewById(R.id.llLeftTeamRow5)).addView(playerView);
                                break;
                        }
                    }
                }

                for (int i = 0; i < ((LiveMatchActivity) getActivity()).rightFormation.size(); i++)
                {
                    for (int j = 0; j < ((LiveMatchActivity) getActivity()).rightFormation.get(i).size(); j++)
                    {
                        HashMap<String, String> player = ((LiveMatchActivity) getActivity()).rightFormation.get(i).get(j);
                        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
                        View playerView = inflater.inflate(R.layout.field_player, null);
                        CommonMethods.applyCustomFont(getActivity(), playerView);

                        ((ImageView)playerView.findViewById(R.id.ivJersey)).setImageResource(R.drawable.maglia_squadra_a);
                        ((TextView)playerView.findViewById(R.id.tvPlayerNumber)).setText(player.get("leftNumber"));
                        ((TextView)playerView.findViewById(R.id.tvPlayerNumber)).setTextColor(Color.WHITE);
                        ((TextView)playerView.findViewById(R.id.tvPlayerName)).setText(player.get("leftName"));

                        int correctedI = i;
                        if (((LiveMatchActivity) getActivity()).rightFormation.size() == 4)
                            correctedI++;
                        switch (correctedI)
                        {
                            case 0:
                                ((LinearLayout)getView().findViewById(R.id.llRightTeamRow1)).addView(playerView);
                                break;
                            case 1:
                                ((LinearLayout)getView().findViewById(R.id.llRightTeamRow2)).addView(playerView);
                                break;
                            case 2:
                                ((LinearLayout)getView().findViewById(R.id.llRightTeamRow3)).addView(playerView);
                                break;
                            case 3:
                                ((LinearLayout)getView().findViewById(R.id.llRightTeamRow4)).addView(playerView);
                                break;
                            case 4:
                                ((LinearLayout)getView().findViewById(R.id.llRightTeamRow5)).addView(playerView);
                                break;
                        }
                    }
                }
            }
        }

        class FormationsAdapter extends ArrayAdapter<HashMap<String, String>> {
            private LayoutInflater mInflater;

            public FormationsAdapter(Context context, List<HashMap<String, String>> items) {
                super(context, 0, items);
                mInflater = LayoutInflater.from(context);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                if (getItem(position).get("type").equals("separator")) {
                    view = mInflater.inflate(R.layout.list_header, null);
                    TextView tvHeaderText = (TextView)view.findViewById(R.id.tvHeaderText);
                    tvHeaderText.setText(getItem(position).get("text"));
                } else {
                    view = mInflater.inflate(R.layout.formations_item, null);

                    TextView tvLeftPlayerName = (TextView) view.findViewById(R.id.tvLeftPlayerName);

                    tvLeftPlayerName.setText(getItem(position).get("leftName"));
                }

                return view;
            }

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(
                    R.layout.fragment_live_formations, container, false);
            CommonMethods.applyCustomFont(getActivity(), rootView);

            return rootView;
        }

        public static void adjustListViewHeight(ListView listView) {

            ListAdapter mAdapter = listView.getAdapter();

            int totalHeight = 0;

            for (int i = 0; i < mAdapter.getCount(); i++) {
                View mView = mAdapter.getView(i, null, listView);

                mView.measure(
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),

                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

                totalHeight += mView.getMeasuredHeight();
                Log.w("HEIGHT" + i, String.valueOf(totalHeight));

            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight
                    + (listView.getDividerHeight() * (mAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();

        }

        @Override
        public void onActivityCreated( Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            if (getView() != null) {
                update();
            }
        }
    }

    //pagelle
    public static class ScorecardsFragment extends Fragment {

        //private ArrayList<HashMap<String, String>> mItems;

        public void update() {
            if (getView() != null) {
                ListView lvEventTimeline = (ListView) getView().findViewById(R.id.lvEventTimeline);
                lvEventTimeline.setAdapter(new ScorecardAdapter(getActivity(), ((LiveMatchActivity) getActivity()).scorecardItems));
            }
        }

        class ScorecardAdapter extends ArrayAdapter<HashMap<String, String>> {
            private LayoutInflater mInflater;

            public ScorecardAdapter(Context context, List<HashMap<String, String>> items) {
                super(context, 0, items);
                mInflater = LayoutInflater.from(context);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                HashMap<String, String> item = getItem(position);

                if (item.get("type").equals("separator")) {
                    view = mInflater.inflate(R.layout.list_header, null);
                    TextView tvHeaderText = (TextView)view.findViewById(R.id.tvHeaderText);
                    tvHeaderText.setText(getItem(position).get("text"));
                } else {
                    view = mInflater.inflate(R.layout.scorecard_item, null);

                    TextView tvPlayerNumber = (TextView) view.findViewById(R.id.tvPlayerNumber);
                    TextView tvPlayerName = (TextView) view.findViewById(R.id.tvPlayerName);
                    TextView tvPlayerRole = (TextView) view.findViewById(R.id.tvPlayerRole);
                    TextView tvPlayerScore = (TextView) view.findViewById(R.id.tvPlayerScore);

                    tvPlayerNumber.setText(item.get("number"));
                    tvPlayerName.setText(item.get("name"));
                    tvPlayerRole.setText(item.get("role"));
                    tvPlayerScore.setText(item.get("score"));

                }

                return view;
            }

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(
                    R.layout.fragment_live_timeline, container, false);
            CommonMethods.applyCustomFont(getActivity(), rootView);

            return rootView;
        }

        @Override
        public void onActivityCreated( Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            if (getView() != null) {
                update();
            }
        }
    }


}
