package it.h3g.sport.europei2016.view.tabellone;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.model.Match;

/**
 * Created by oneenam on 12/05/16.
 */
public class TabellonePagerAdapter extends FragmentStatePagerAdapter {
    ArrayList<Match> calendarList;
    ArrayList<Match> direttaList;
    Context context;

    public TabellonePagerAdapter(FragmentManager fm, Context context, ArrayList<Match> calendarList, ArrayList<Match> direttaList) {
        super(fm);
        this.context = context;
        this.calendarList = calendarList;
        this.direttaList = direttaList;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            CalendarioFragment fragment = new CalendarioFragment();
            Bundle bundle = new Bundle();
            //bundle.putSerializable("calendar",calendarList);
            bundle.putParcelableArrayList("calendar",this.calendarList);
            fragment.setArguments(bundle);
            return fragment;

            //return CalendarioFragment.newInstance(this.calendarList);
        }else if (position == 1) {

            GironiFragment fragment = new GironiFragment();
            return fragment;
        }else {

            EliminaDirettaFragment fragment = new EliminaDirettaFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("eliminaDiretta", this.direttaList);
            fragment.setArguments(bundle);
            return fragment;

            //return EliminaDirettaFragment.newInstance(this.direttaList);
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position)
        {
            case 0:
                return this.context.getString(R.string.Calendario);
            case 1:
                return this.context.getString(R.string.Gironi);
            case 2:
                return this.context.getString(R.string.Elim_Diretta);
            default:
                return this.context.getString(R.string.Calendario);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

}
