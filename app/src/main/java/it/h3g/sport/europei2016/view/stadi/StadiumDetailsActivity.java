package it.h3g.sport.europei2016.view.stadi;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;

/**
 * Created by oneenam on 09/05/16.
 */
public class StadiumDetailsActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stadium_details_activity);


         /*
          replace for toolbar
         */
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        //toolbar.setBackgroundColor((((CampionatoApplication) getApplication()).barColor));
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView textViewTitle = (TextView)findViewById(R.id.textViewTitle);
        //textViewTitle.setText(getString(R.string.Notifiche));
        textViewTitle.setText("");

        ImageButton imageButton = (ImageButton)findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        SimpleDraweeView sdvStadium = (SimpleDraweeView)findViewById(R.id.sdvStadium);
        TextView tvStadium = (TextView)findViewById(R.id.tvStadium);
        //WebView wvDetails = (WebView)findViewById(R.id.webViewDescription);
        TextView tvDetails = (TextView)findViewById(R.id.tvDetails);


        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){

            String name = bundle.getString("name");
            if(name!=null){
                textViewTitle.setText("" + name);
                tvStadium.setText("" + name);
            }


            String url = bundle.getString("image");
            if(url!=null){
                Log.e(getLocalClassName(),"stadium image "+url);
                Uri uri = Uri.parse(url);
                sdvStadium.setImageURI(uri);
            }

            StringBuilder details = new StringBuilder();
            //details.append("<html><body>");


            details.append("<strong>Città:</strong> ");
            String city = bundle.getString("city");
            if(city!=null){
                details.append(""+city);
            }

            details.append("<br />");
            details.append("<strong>Anno di costruzione:</strong> ");
            String year = bundle.getString("year");
            if(year!=null){
                details.append(""+year);
            }

            details.append("<br />");
            details.append("<strong>Capacità:</strong> ");
            String capability = bundle.getString("capability");
            if(capability!=null){
                details.append(""+capability);
            }

            details.append("<br />");
            details.append("<b>Descrizione:</b> ");
            String descrizione = bundle.getString("descrizione");
            if(descrizione!=null){
                details.append(""+descrizione);
            }
            //details.append("<b>This is bold</b>");
            //details.append("</body></html>");

            /*String pish = "<html><head><style type=\"text/css\">@font-face {font-family: AvenirLTStd-Roman;src: url(\"file:///android_asset/avenir_roman.ttf\")}body {font-family: AvenirLTStd-Roman;font-size: medium;text-align: justify;}</style></head><body>";
            String pas = "</body></html>";
            String myHtmlString = pish + details + pas;
            wvDetails.getSettings();
            wvDetails.setBackgroundColor(Color.TRANSPARENT);
            wvDetails.loadDataWithBaseURL(null, myHtmlString, "text/html", "UTF-8", null);*/


            //tvDetails.setText(""+ Html.fromHtml(details.toString()));
            tvDetails.setText(Html.fromHtml(details.toString()));

        }


        CommonMethods.applyCustomFont(StadiumDetailsActivity.this, findViewById(R.id.llStadiumDetails));
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onResume() {
        super.onResume();
        //Adjust.onResume(this);
    }
    protected void onPause() {
        super.onPause();
        //Adjust.onPause();
    }
}
