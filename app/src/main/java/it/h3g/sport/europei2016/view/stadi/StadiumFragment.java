package it.h3g.sport.europei2016.view.stadi;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.nineoldandroids.animation.Animator;

import it.h3g.sport.europei2016.MainActivity;
import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.BaseFragment;
import it.h3g.sport.europei2016.view.notifications.NotificationsActivity;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;


/**
 * Created by oneenam on 09/05/16.
 */
public class StadiumFragment extends BaseFragment {

    private UiSettings mUiSettings =null;
    private GoogleMap googleMap = null;
    private LatLng latlng = null;
    private SupportMapFragment supportMapFragment = null;
    RelativeLayout relativeLayoutMap;
    FrameLayout frameLayoutMap;
    public HashMap<Marker, JsonObject> markerHashMap = new HashMap<Marker, JsonObject>();


    ArrayList<JsonObject> stadiumList;

    Point sizeScreen;


    public static StadiumFragment newInstance(){
        StadiumFragment fragment = new StadiumFragment();
        return fragment;
    }

    public StadiumFragment(){}

    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void onResume() {
        super.onResume();

        setToolbar();


        if (googleMap == null) {
            googleMap = supportMapFragment.getMap();
            mUiSettings = googleMap.getUiSettings();
            mUiSettings.setZoomControlsEnabled(false);
            mUiSettings.setCompassEnabled(false);
            mUiSettings.setMyLocationButtonEnabled(false);
            googleMap.setMyLocationEnabled(true);
            //googleMap.setOnMarkerClickListener(this);
            googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(getActivity(), R.layout.custom_info_window));
            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    JsonObject stadium = markerHashMap.get(marker);
                    if(stadium!=null){
                        //



                        Intent intent = new Intent(getActivity(), StadiumDetailsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                        Bundle bundle = new Bundle();
                        if(stadium.has("name"))
                            bundle.putString("name", stadium.get("name").getAsString());

                        if(stadium.has("city"))
                            bundle.putString("city", stadium.get("city").getAsString());

                        if(stadium.has("year"))
                            bundle.putString("year", stadium.get("year").getAsString());

                        if(stadium.has("capability"))
                            bundle.putString("capability", stadium.get("capability").getAsString());

                        if(stadium.has("descrizione"))
                            bundle.putString("descrizione", stadium.get("descrizione").getAsString());

                        if(stadium.has("image"))
                            bundle.putString("image", stadium.get("image").getAsString());

                        //bundle.putSerializable("stadium", list);
                        //bundle.putString("stadium", stadium.getAsString());
                            intent.putExtras(bundle);
                        getActivity().startActivity(intent);


                    }

                }
            });

            //TODO
            //googleMap.setInfoWindowAdapter(new MapInfoWindowAdapter(PlaceDetailsFragment.this));
        }


        loadStadium();


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.stadium_fragment, container, false);
    }


    @Override
    public void onActivityCreated(Bundle saved){


        FragmentManager fm = getChildFragmentManager();
        supportMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.rlStadiumView);
        if (supportMapFragment == null) {
            supportMapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.rlStadiumView, supportMapFragment).commit();
        }


        super.onActivityCreated(saved);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        relativeLayoutMap = (RelativeLayout)getView().findViewById(R.id.rlStadiumView);
        frameLayoutMap = (FrameLayout)getView().findViewById(R.id.frameLayoutMap);


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        sizeScreen = new Point();
        display.getSize(sizeScreen);


    }


    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        final View customView = getActivity().getLayoutInflater().inflate(R.layout.stadium_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);


        /*imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openMenu();
            }
        });*/



        ImageButton imageButtonSearch = (ImageButton)customView.findViewById(R.id.imageButtonSearch);
        imageButtonSearch.setTag("1");

        final EditText editTextSearch = (EditText)customView.findViewById(R.id.editTextSearch);
        editTextSearch.setTag("1");



        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.closeKeyBoard(getActivity(), v);
                editTextSearch.setText("");
                editTextSearch.setVisibility(View.INVISIBLE);
                customView.findViewById(R.id.imageButtonSearch).setTag("1");
                customView.findViewById(R.id.textViewTitle).setVisibility(View.VISIBLE);
                customView.findViewById(R.id.imageButtonSearch).setVisibility(View.VISIBLE);

                mListener.openMenu();
            }
        });



        //close
        editTextSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (editTextSearch.getRight() - editTextSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        editTextSearch.setText("");
                        editTextSearch.setVisibility(View.INVISIBLE);
                        customView.findViewById(R.id.imageButtonSearch).setTag("1");
                        customView.findViewById(R.id.textViewTitle).setVisibility(View.VISIBLE);
                        customView.findViewById(R.id.imageButtonSearch).setVisibility(View.VISIBLE);

                        CommonMethods.closeKeyBoard(getActivity(), editTextSearch);

                        if(editTextSearch.getText().toString().trim().length()>0)
                            centerMap();

                        return true;
                    }
                }
                return false;
            }
        });


        editTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    //Log.i(getTag(), "Enter pressed "+editTextSearch.getText());
                    if(stadiumList!=null && stadiumList.size()>0) {
                        for (JsonObject stadium : stadiumList) {
                            if (stadium.has("name")) {
                                if (stadium.get("name").getAsString().toLowerCase(Locale.getDefault()).contains(editTextSearch.getText().toString().toLowerCase(Locale.getDefault()))) {


                                    String lat_long = "";
                                    if (stadium.has("lat_long")) {
                                        lat_long = stadium.get("lat_long").getAsString();

                                        String[] latlngArray = lat_long.split(",");

                                        try {
                                            final float latitude = Float.parseFloat(latlngArray[0]);
                                            final float longitude = Float.parseFloat(latlngArray[0]);
                                            latlng = new LatLng(latitude, longitude);
                                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 15));

                                        } catch (NumberFormatException nfe) {
                                        }
                                    }

                                }
                            }
                        }//for
                    }

                }
                return false;
            }
        });

        /*editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/



        imageButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getTag().toString().equals("1")){
                    customView.findViewById(R.id.imageButtonSearch).setTag("0");
                    customView.findViewById(R.id.textViewTitle).setVisibility(View.INVISIBLE);
                    customView.findViewById(R.id.imageButtonSearch).setVisibility(View.INVISIBLE);


                    YoYo.with(Techniques.SlideInLeft)
                            .duration(700)
                            .withListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                    /*YoYo.with(Techniques.FadeOut).duration(700)
                                            .playOn(imageButtonSearch);*/
                                    editTextSearch.setVisibility(View.VISIBLE);

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {

                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            }).playOn(v);

                }else{
                    customView.findViewById(R.id.imageButtonSearch).setTag("1");
                    customView.findViewById(R.id.textViewTitle).setVisibility(View.VISIBLE);
                    customView.findViewById(R.id.imageButtonSearch).setVisibility(View.VISIBLE);

                }

                editTextSearch.requestFocus();
                CommonMethods.showKeyboard(getActivity(), editTextSearch);
            }
        });


    }


    //
    private void loadStadium(){

        String url = CommonMethods.BASE_REQUEST_URL + File.separator + "stadium.json";
        Log.d("Diretta URL", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());

        //loading file
        Ion.with(getActivity())
                .load(url)
                .setHeader("Authorization", CommonMethods.basicAuth)
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        //System.out.println("" + downloaded + " / " + total);
                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                    }
                })
                .write(new File(getActivity().getFilesDir().getPath() + File.separator + fileName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {

                        if (e == null) {

                            if (file.exists()) {
                                System.out.println("Path: " + file.getAbsolutePath());


                                if(getActivity()!=null) {
                                    Ion.with(getActivity())
                                            .load("file://" + file.getAbsolutePath())
                                            .asJsonObject()
                                            .setCallback(new FutureCallback<JsonObject>() {
                                                @Override
                                                public void onCompleted(Exception e, JsonObject result) {

                                                    String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                    if (exception != null) {
                                                        closeProgressDialog();
                                                        Snackbar.make(getView(), exception, Snackbar.LENGTH_LONG).show();
                                                    } else {
                                                        parseStadium(result);
                                                    }
                                                }
                                            });
                                }

                            }

                        }else{
                            //show error
                        }

                    }
                });
    }

    private void parseStadium(JsonObject result){
        stadiumList = new ArrayList<>();

        if(result != null) {
            if(result.has("stadium")) {

                Object stadiums = result.get("stadium");
                if(stadiums instanceof JsonArray) {
                    JsonArray jaArray = result.getAsJsonArray("stadium");

                    if (jaArray.getClass() == JsonArray.class) {

                        for (int i = 0; i < jaArray.size(); i++) {
                            JsonObject stadium = jaArray.get(i).getAsJsonObject();
                            String title = "";
                            if(stadium.has("name"))
                                title = stadium.get("name").getAsString();

                            String snippet = "";
                            if(stadium.has("city"))
                                snippet = stadium.get("city").getAsString();

                            String lat_long = "";
                            if(stadium.has("lat_long")) {
                                lat_long = stadium.get("lat_long").getAsString();

                                String[] latlngArray = lat_long.split(",");

                                try {
                                    final float latitude = Float.parseFloat(latlngArray[0]);
                                    final float longitude = Float.parseFloat(latlngArray[0]);
                                    latlng = new LatLng(latitude, longitude);

                                        //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 19));

                                    BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.pin);


                                    Marker marker = null;
                                    marker = googleMap.addMarker(new MarkerOptions().position(
                                            new LatLng(latitude, longitude))
                                            .icon(icon)
                                            .title(title)
                                            .snippet(snippet));

                                    if (marker != null)
                                        markerHashMap.put(marker, stadium);

                                } catch (NumberFormatException e) {
                                }


                                centerMap();


                            }

                            stadiumList.add(stadium);
                        }
                    }
                }

            }
        }
    }


    private void centerMap(){

        //show area center for all markers
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(Marker mar: markerHashMap.keySet()){
            builder.include(mar.getPosition());

        }
        LatLngBounds bounds = builder.build();
        int padding = 0; // offset from edges of the map in pixels

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, sizeScreen.x-144, sizeScreen.y-144, padding);
        googleMap.animateCamera(cu);

    }

    //
    public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View mWindow;
        // private final View mContents;

        private Activity activity;


        public CustomInfoWindowAdapter(Activity activity, int custom_info_window) {
            this.activity = activity;

            mWindow = activity.getLayoutInflater().inflate(custom_info_window, null);
            //mContents = getActivity().getLayoutInflater().inflate(R.layout.custom_info_contents, null);
        }

        @Override
        public View getInfoContents(Marker marker) {
            render(marker, mWindow);
            return mWindow;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            render(marker, mWindow);
            return mWindow;
        }



        private void render(final Marker marker, View view) {
            //((ImageView) view.findViewById(R.id.badge)).setImageResource(R.drawable.abs__ic_go);
            String title = marker.getTitle();
            String  snippet = marker.getSnippet();

            TextView titleUi = ((TextView) view.findViewById(R.id.tvName));
            TextView snippetUi = ((TextView) view.findViewById(R.id.tvCity));

            if (snippet != null) {
                // Spannable string allows us to edit the formatting of the text.
                SpannableString titleText = new SpannableString(snippet);
                // titleText.setSpan(new ForegroundColorSpan(Color.BLACK), 0, titleText.length(), 0);
                snippetUi.setText(titleText);
            } else {
                snippetUi.setText("");
            }


            if (title != null) {
                // Spannable string allows us to edit the formatting of the text.
                SpannableString titleText = new SpannableString(title);
                //  titleText.setSpan(new ForegroundColorSpan(Color.BLACK), 0, titleText.length(), 0);
                titleUi.setText(titleText);
            } else {
                titleUi.setText("");
            }
            CommonMethods.applyCustomFont(activity, view);
        }

    }
}
