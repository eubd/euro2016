package it.h3g.sport.europei2016;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.tsengvn.typekit.Typekit;

import it.h3g.sport.europei2016.manager.CommonMethods;

/**
 * Created by oneenam on 06/04/16.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Fresco.initialize(getApplicationContext());

        //setting custom font
        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "avenir_roman.ttf"))
                .addBold(Typekit.createFromAsset(this, "avenir_bold.ttf"))
                .addItalic(Typekit.createFromAsset(this, "avenir_roman.ttf"))
                .addBoldItalic(Typekit.createFromAsset(this, "avenir_bold.ttf"))
                .addCustom1(Typekit.createFromAsset(this, "avenir_roman.ttf"))
                .addCustom2(Typekit.createFromAsset(this, "avenir_roman.ttf"));


        // By default News section will be active
        CommonMethods.writeToDefaults(getApplicationContext(), "menu_position_section", 0);

    }


}
