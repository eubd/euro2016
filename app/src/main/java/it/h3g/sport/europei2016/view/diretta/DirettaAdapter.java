package it.h3g.sport.europei2016.view.diretta;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.BaseAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.model.Match;


/**
 * Created by oneenam on 10/05/16.
 */
public class DirettaAdapter extends BaseAdapter {

    private static final int TYPE_AD_TOP = 0;
    private static final int TYPE_SEPARATOR = 1;
    private static final int TYPE_ITEM = 2;
    private static final int TYPE_AD_BOTTOM = 3;

    private List<Match> mData = new ArrayList<>();
    //private TreeSet<Integer> sectionHeader = new TreeSet<>();
    private ArrayList<Integer> sectionHeader = new ArrayList<>();

    private LayoutInflater mInflater;
    Activity context;

    private AdView topAd; //100
    private AdView bottomAd; //250
    AdRequest adRequest;

    int adTop = 0;
    int adBottom = 0;

    /*public DirettaAdapter(Context context) {
        this.context = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }*/

    public DirettaAdapter(Activity context) {
        this.context = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        //
        topAd = new AdView(context);
        topAd.setAdUnitId(context.getString(R.string.Diretta_Banner100));
        topAd.setAdSize(AdSize.LARGE_BANNER);
        if(CommonMethods.DEBUG_MODE)
            adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
        else
            adRequest = new AdRequest.Builder().build();

        //
        bottomAd = new AdView(context);
        bottomAd.setAdUnitId(context.getString(R.string.Diretta_Banner250));
        bottomAd.setAdSize(AdSize.MEDIUM_RECTANGLE);

        if(CommonMethods.DEBUG_MODE)
            adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
        else
            adRequest = new AdRequest.Builder().build();


        //adding 1st ad
        if(topAd!=null) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    topAd.loadAd(adRequest);
                    topAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            addAdvTop();
                        }

                        @Override
                        public void onAdFailedToLoad(int errorCode) {
                            super.onAdFailedToLoad(errorCode);
                            topAd.setVisibility(View.GONE);
                        }
                    });
                }
            });

        }



        //adding last ad
        if(bottomAd!=null) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    bottomAd.loadAd(adRequest);
                    bottomAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            addAdvBottom();
                        }

                        @Override
                        public void onAdFailedToLoad(int errorCode) {
                            super.onAdFailedToLoad(errorCode);
                            bottomAd.setVisibility(View.GONE);
                        }
                    });
                }
            });

        }

    }

    public void addAdvTop() {
        //item.setViewType("0");

        if(mData.size()!=0) {
            sectionHeader.add(0);
            Match match = new Match();
            match.setViewType("0");
            match.setHeader("ad_top");
            mData.add(0, match);
            adTop = 1;
            notifyDataSetChanged();
        }
    }


    public void addSectionHeaderItem(Match item) {
        item.setViewType("1");
        item.setHeader("header");
        sectionHeader.add(1);
        mData.add(item);
        //sectionHeader.add(TYPE_SEPARATOR);
    }

    public void addItem(Match item) {
        item.setViewType("2");
        item.setHeader("item");
        sectionHeader.add(2);
        mData.add(item);
    }

    public void addAdvBottom() {
        //item.setViewType("3");

        if(mData.size()>0) {
            sectionHeader.add(3);
            Match match = new Match();
            match.setViewType("3");
            match.setHeader("ad_bottom");
            mData.add(mData.size(), match);
            adBottom = 1;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return sectionHeader.get(position);
        //return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Match getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        int rowType = getItemViewType(position);
        Match match = mData.get(position);
        if (match != null) {
            if (match.getHeader().equals("ad_top")) {
                System.out.println("----- 1");
                return topAd;
            } else if (match.getHeader().equals("header")) {
                System.out.println("----- 2");

                convertView = mInflater.inflate(R.layout.diretta_header, null);
                TextView tvHeader = (TextView) convertView.findViewById(R.id.tvHeader);

                if (match.getMatchdate() != null) {
                    SimpleDateFormat sDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date dateMatch = sDateFormat.parse(match.getMatchdate().replace("\"", ""));

                        sDateFormat = new SimpleDateFormat("EEEE dd MMMM");
                        String dateT = sDateFormat.format(dateMatch);

                        tvHeader.setText("" + dateT);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                return convertView;
            } else if (match.getHeader().equals("item")) {
                System.out.println("----- 3");

                convertView = mInflater.inflate(R.layout.diretta_row, null);
                LinearLayout llDirettaRow = (LinearLayout) convertView.findViewById(R.id.llDirettaRow);
                TextView tvTeamHome = (TextView) convertView.findViewById(R.id.tvTeamHome);
                TextView tvGoal = (TextView) convertView.findViewById(R.id.tvGoal);
                TextView tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
                TextView tvTeamAway = (TextView) convertView.findViewById(R.id.tvTeamAway);
                SimpleDraweeView sdvTeamHome = (SimpleDraweeView) convertView.findViewById(R.id.sdvTeamHome);
                SimpleDraweeView sdvTeamAway = (SimpleDraweeView) convertView.findViewById(R.id.sdvTeamAway);

                tvTeamHome.setText("" + match.getTeamname_home());
                if (match.getStatus() != null)
                    tvStatus.setText("" + match.getStatus().toString().replace("\"", ""));

                String urlHome = match.getTeamlogo_home();
                if (urlHome != null) {
                    Uri uri = Uri.parse(urlHome);
                    sdvTeamHome.setImageURI(uri);
                }


                tvTeamAway.setText("" + match.getTeamname_away());
                String urlAway = match.getTeamlogo_away();
                if (urlAway != null) {
                    Uri uri = Uri.parse(urlAway);
                    sdvTeamAway.setImageURI(uri);
                }

                tvGoal.setText("" + match.getNumgoal_home() + "-" + match.getNumgoal_away());
                tvStatus.setText("" + match.getStatus());

                llDirettaRow.setTag(match);
                llDirettaRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (v.getTag() != null) {
                            Match match = (Match) v.getTag();
                            if (match != null) {


                                Intent showLiveMatchIntent = new Intent(context, LiveMatchActivity.class);

                                showLiveMatchIntent.putExtra("overviewURL", match.getMatchdetails());
                                showLiveMatchIntent.putExtra("liveFeedURL", match.getCommentary());
                                showLiveMatchIntent.putExtra("match", match);

                                System.out.println("Match commentary " + match.getCommentary());
                                System.out.println("Match details " + match.getMatchdetails());

                                //showLiveMatchIntent.putExtra("overviewURL", CommonMethods.BASE_REQUEST_URL + File.separator + "diretta.json");
                                context.startActivity(showLiveMatchIntent);


                            }
                        }

                    }
                });

                CommonMethods.applyCustomFont(context, convertView);
                return convertView;
            } else if (match.getHeader().equals("ad_bottom")) {
                System.out.println("----- 4");
                return bottomAd;
            }
        }
        return  null;
    }

        //int rowType = Integer.parseInt(match.getViewType());
        /*if(rowType == 0){
            return topAd;
        }else if(rowType == 3){
            return bottomAd;
        }else if(rowType == 1){

        }
        else{

        }*/

}
