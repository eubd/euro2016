package it.h3g.sport.europei2016.view.gallery;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.flurry.android.FlurryAgent;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;


public class PhotoGalleryActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_gallery);

        JsonArray photos = new JsonParser().parse(getIntent().getStringExtra("galleryContent")).getAsJsonArray();

        ViewPager vpPhotoGallery = (ViewPager) findViewById(R.id.vpPhotoGallery);
        vpPhotoGallery.setAdapter(new PhotoGalleryAdapter(photos));   }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onResume() {
        super.onResume();
       // Adjust.onResume(this);
    }
    protected void onPause() {
        super.onPause();
       // Adjust.onPause();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        FlurryAgent.onStartSession(this, "N2KRX2MP37ZHWHC22MZ7");
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }


    private class PhotoGalleryAdapter extends PagerAdapter
    {
        private JsonArray photos;
        private LayoutInflater inflater;

        PhotoGalleryAdapter(JsonArray photos) {
            this.photos = photos;
            inflater = getLayoutInflater();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return photos.size();
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
            CommonMethods.applyCustomFont(PhotoGalleryActivity.this, imageLayout);

            assert imageLayout != null;
            SimpleDraweeView sdvGalleryThumb    = (SimpleDraweeView) imageLayout.findViewById(R.id.sdvGalleryThumb);
            TextView tvPhotoCaption             = (TextView) imageLayout.findViewById(R.id.tvPhotoCaption);
            JsonObject photo = photos.get(position).getAsJsonObject();
            if(photo != null){
                if(photo.has("image")){
                    String url = photo.get("image").getAsString().replace("_t", "");
                    if(url != null){
                        Uri uri = Uri.parse(url);
                        sdvGalleryThumb.setImageURI(uri);
                    }
                }

                if(photo.has("titledidascalia") && photo.has("dida")){
                    tvPhotoCaption.setText(photo.get("titledidascalia").getAsString() + "\n" + photo.get("dida").getAsString());
                }

                view.addView(imageLayout, 0);
                return  imageLayout;
            }else{

            }
            return null;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

    }
}
