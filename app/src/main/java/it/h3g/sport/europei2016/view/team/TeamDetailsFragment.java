package it.h3g.sport.europei2016.view.team;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.BaseFragment;

/**
 * Created by mohammod.hossain on 4/17/2016.
 */
public class TeamDetailsFragment extends BaseFragment {

    //ad view
    private AdView mAdView;
    private AdView headerAd;
    AdRequest adRequest;
    ListView lvTeamDetails ;

    public static TeamDetailsFragment newInstance(Bundle bundle) {
        TeamDetailsFragment fragment = new TeamDetailsFragment();
        if(bundle!=null)
            fragment.setArguments(bundle);
        return fragment;
    }

    public TeamDetailsFragment() {
        // Required empty public constructor
    }



    @Override
    public void onStart() {
        super.onStart();
       FlurryAgent.onStartSession(getActivity(), CommonMethods.FLURRY_ID);
        setToolbar();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
       FlurryAgent.onEndSession(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.teams_details_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout llTeamsView = (LinearLayout) getView().findViewById(R.id.llTeamDetailsView);

        //adding AdView at bottom
        mAdView = new AdView(getActivity());
        mAdView.setAdUnitId(getString(R.string.banner_ad_unit_id_generic));
        mAdView.setAdSize(AdSize.BANNER);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));

        if(CommonMethods.DEBUG_MODE)
            adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
        else
            adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                mAdView.setVisibility(View.GONE);
            }
        });
        llTeamsView.addView(mAdView, params);

        Bundle bundle = getArguments();
        if(bundle != null){
            SimpleDraweeView sdvTeamLogo = (SimpleDraweeView)getView().findViewById(R.id.ivTeamLogo);
            TextView tvTeamName         = (TextView) getView().findViewById(R.id.tvTeamName);

            lvTeamDetails               = (ListView) getView().findViewById(R.id.lvTeamDetails);
            CommonMethods.applyCustomFont(getActivity(), getView());

            JsonObject teamDetails = new JsonParser().parse(getArguments().getString("teamDetails")).getAsJsonObject();

            if(teamDetails.has("logo")){
                String url = teamDetails.get("logo").getAsString();
                if(url != null){
                    Uri uri = Uri.parse(url);
                    sdvTeamLogo.setImageURI(uri);
                }
            }

            if(teamDetails.has("team_name")){
                tvTeamName.setText(teamDetails.get("team_name").getAsString());
            }

            // Creiamo un po' di dati di test
            final ArrayList<HashMap<String, String>> listItems = new ArrayList<>();

            //Scheda
            HashMap<String, String> listItem = new HashMap<>();
            listItem.put("type", "separator");
            listItem.put("text", "Scheda");
            listItems.add(listItem);

            listItem = new HashMap<>();
            //this is accordign to GUI
            String squadra = null;
            String allenatore = null;
            String colori = null;
            String partecipazioneFaseFinale = null;
            String partecipazioneEuropei = null;
            String migliorPiazzamento = null;

            //delete this
            String teamName = "";
            String coach = "";
            String finalState = "";
            String participationEuro = "";
            String bestFinish = "";

            if (teamDetails.has("team_name") && teamDetails.get("team_name") != null )
                teamName = teamDetails.get("team_name").getAsString();
            if (teamDetails.has("coach") && teamDetails.get("coach") != null )
                coach = teamDetails.get("coach").getAsString();

            if (teamDetails.has("color") && teamDetails.get("color")  != null)
                colori = teamDetails.get("color").getAsString();
            if (teamDetails.has("final_state") && teamDetails.get("final_state") != null)
                finalState = teamDetails.get("final_state").getAsString();
            if (teamDetails.has("partecipation_euro") && teamDetails.get("partecipation_euro") != null)
                participationEuro = teamDetails.get("partecipation_euro").getAsString();
            if (teamDetails.has("better_placement_euro") && teamDetails.get("better_placement_euro") != null)
                bestFinish = teamDetails.get("better_placement_euro").getAsString();
            listItem.put("type", "scheda");
            listItem.put("text", "<strong>Squadra:</strong> " + teamName + "<br />" +
                    "<strong>Colori:</strong> " + coach + "<br />" +
                    "<strong>Allenatore:</strong> " + colori + "<br />" +
                    "<strong>Partecipazione fase finale:</strong> " + finalState + "<br />" +
                    "<strong>Partecipazione Europei:</strong> " + participationEuro + "<br />" +
                    "<strong>Miglior piazzamento:</strong> " + bestFinish);
            listItems.add(listItem);

            //Palmares
            //Check GUI
            listItem = new HashMap<>();
            listItem.put("type", "separator");
            listItem.put("text", "Palmares");
            listItems.add(listItem);

            // Rimuoviamo le attribuzioni di DataSport, ne hanno già a sufficienze e il link non funzionerebbe comunque
            String fixedPalmares = "";
            if(teamDetails.has("palmares")){
                if (!teamDetails.get("palmares").isJsonNull() || !teamDetails.get("palmares").equals("{}") )
                    fixedPalmares = teamDetails.get("palmares").getAsString();

                Log.e("tag",fixedPalmares);
            }

            // Rimuoviamo newline e tabulazioni

            if(fixedPalmares != null || !fixedPalmares.equals("{}")) {
                fixedPalmares = fixedPalmares.replace("\n", "");
                fixedPalmares = fixedPalmares.replace("\t", "");
                if (fixedPalmares.contains("Dati a cura di")) {
                    fixedPalmares = fixedPalmares.substring(0, fixedPalmares.indexOf("Dati a cura di"));
                    // Rimuoviamo gli eccessivi ritorni a capo alla fine
                    if(CommonMethods.DEBUG_MODE)
                        Log.d("test", fixedPalmares.substring(fixedPalmares.length() - 6));

                    while (fixedPalmares.substring(fixedPalmares.length() - 6).equals("<br />")) {
                        fixedPalmares = fixedPalmares.substring(0, fixedPalmares.length() - 6);
                    }
                    fixedPalmares = fixedPalmares.replace("<p>", "");

                    if(CommonMethods.DEBUG_MODE)
                        Log.d("Palmares", fixedPalmares);
                }
            }
            listItem = new HashMap<>();
            listItem.put("type", "palmares");
            listItem.put("text", fixedPalmares);
            listItems.add(listItem);


            //Rosa
            listItem = new HashMap<>();
            listItem.put("type", "separator");
            listItem.put("text", "Rosa");
            listItems.add(listItem);

            if (teamDetails.has("rosa") && teamDetails.getAsJsonObject("rosa") != null) {
                if(teamDetails.getAsJsonObject("rosa").has("player")){
                    JsonArray jaRoster = teamDetails.getAsJsonObject("rosa").getAsJsonArray("player");
                    if (jaRoster != null) {
                        for (int i = 0; i < jaRoster.size(); i++) {
                            if ( !jaRoster.get(i).getAsJsonObject().get("name").getClass().equals(JsonObject.class) && !jaRoster.get(i).getAsJsonObject().get("surname").getClass().equals(JsonObject.class)) {
                                listItem = new HashMap<>();
                                listItem.put("type", "player");
                                listItem.put("num", jaRoster.get(i).getAsJsonObject().has("num")  &&  !jaRoster.get(i).getAsJsonObject().get("num").equals("{}")? jaRoster.get(i).getAsJsonObject().get("num").getAsString() : "");
                                String name = "",surName="";
                                name = jaRoster.get(i).getAsJsonObject().has("name") ? jaRoster.get(i).getAsJsonObject().get("name").getAsString() : "";
                                surName = jaRoster.get(i).getAsJsonObject().has("surname") ? jaRoster.get(i).getAsJsonObject().get("surname").getAsString() : "";

                                listItem.put("name", name + " " + surName);
                                listItem.put("role", jaRoster.get(i).getAsJsonObject().has("role") ? jaRoster.get(i).getAsJsonObject().get("role").getAsString() : "");
                                listItems.add(listItem);
                            }
                        }
                    }
                }

            }


            lvTeamDetails.setAdapter(new TeamDetailsAdapter(getActivity(), listItems));

        }
    }

    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((TeamDetailsActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        View customView = getActivity().getLayoutInflater().inflate(R.layout.teams_details_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);

        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);

        TextView tvTitle = (TextView) customView.findViewById(R.id.textViewTitle);
        tvTitle.setText(getString(R.string.Le_squadre));

        /*Bundle bundle = getArguments();
        if(bundle!=null){
            tvTitle.setText(bundle.getString("teamName"));
        }*/

        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }





    class TeamDetailsAdapter extends ArrayAdapter<HashMap<String, String>> {
        private LayoutInflater mInflater;

        public TeamDetailsAdapter(Context context, List<HashMap<String, String>> items) {
            super(context, 0, items);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            final HashMap<String, String> item = getItem(position);
            if(item != null){
                if (item.get("type").equals("separator")) {
                    view = mInflater.inflate(R.layout.list_header, null);

                    TextView tvHeaderText = (TextView)view.findViewById(R.id.tvHeaderText);
                    tvHeaderText.setText(item.get("text"));
                } else if (item.get("type").equals("player")) {
                    view = mInflater.inflate(R.layout.team_details_player, null);

                    ((TextView)view.findViewById(R.id.tvPlayerNum)).setText(item.get("num"));
                    ((TextView)view.findViewById(R.id.tvPlayerName)).setText(item.get("name"));
                    ((TextView)view.findViewById(R.id.tvPlayerRole)).setText(item.get("role"));
                } else {
                    view = mInflater.inflate(R.layout.team_details_item, null);

                    TextView tvText = (TextView) view.findViewById(R.id.tvText);
                    tvText.setText(Html.fromHtml(item.get("text")));
                }

                CommonMethods.applyCustomFont(getActivity(), view);
                return view;
            } else{

            }
            return null;
        }

    }
}
