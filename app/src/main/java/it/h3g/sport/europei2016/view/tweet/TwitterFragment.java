package it.h3g.sport.europei2016.view.tweet;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.h3g.sport.europei2016.MainActivity;
import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.BaseFragment;

/**
 * Created by oneenam on 09/05/16.
 */
public class TwitterFragment extends BaseFragment {
    String accessToken;


    public static TwitterFragment newInstance() {
        TwitterFragment fragment = new TwitterFragment();
        return fragment;
    }


    public TwitterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        setToolbar();
    }

    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        final View customView = getActivity().getLayoutInflater().inflate(R.layout.teams_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openMenu();
            }
        });

        TextView textViewTitle = (TextView)customView.findViewById(R.id.textViewTitle);
        textViewTitle.setText("Tweet VIP");

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.twitter_fragment, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        showProgressDialog();

        Ion.with(getActivity())
                .load("https://api.twitter.com/oauth2/token")
                        // embedding twitter api key and secret is a bad idea, but this isn't a real twitter app :)
                .basicAuthentication("e4LrcHB55R3WamRYHpNfA", "MIABn1DU5db3Aj0xXzhthsf4aUKMAdoWJTMxJJcY")
                .setBodyParameter("grant_type", "client_credentials")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null) {
                            Toast.makeText(getActivity(), "Impossibile caricare i tweet", Toast.LENGTH_LONG).show();
                            return;
                        }
                        accessToken = result.get("access_token").getAsString();
                        loadTweets();
                    }
                });

    }


    private void loadTweets() {
        String url = "https://api.twitter.com/1.1/lists/statuses.json?slug=i-campioni-di-serie-a&owner_screen_name=DatasportNews&include_rts=true";
        Ion.with(getActivity())
                .load(url)
                .setHeader("Authorization", "Bearer " + accessToken)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        if (e != null) {
                            Toast.makeText(getActivity(), "Impossibile caricare i tweet", Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (result != null) {
                            try {
                                //Log.d("Response", result.toString());
                                ArrayList<HashMap<String, String>> tweets = new ArrayList<HashMap<String, String>>();
                                for (int i = 0; i < result.size(); i++) {
                                    //Log.d("Tweet", "Got " + result.get(i).getAsJsonObject());
                                    HashMap<String, String> tweet = new HashMap<>();
                                    tweet.put("type", "tweet");
                                    tweet.put("id", result.get(i).getAsJsonObject().get("id").getAsString());
                                    tweet.put("text", result.get(i).getAsJsonObject().get("text").getAsString());
                                    tweet.put("avatar", result.get(i).getAsJsonObject().getAsJsonObject("user").get("profile_image_url").getAsString().replace("_normal", "_bigger"));
                                    tweet.put("name", result.get(i).getAsJsonObject().getAsJsonObject("user").get("name").getAsString());
                                    tweet.put("username", "@" + result.get(i).getAsJsonObject().getAsJsonObject("user").get("screen_name").getAsString());
                                    //Log.d("Tweet", "" + tweet);

                                    tweets.add(tweet);
                                }

                                ListView lvTweets = (ListView) getView().findViewById(R.id.lvTweets);
                                lvTweets.setAdapter(new TweetAdapter(getActivity(), tweets));
                            } catch (Exception jsonException) {

                            }

                        }

                        closeProgressDialog();
                    }
                });
    }




    class TweetAdapter extends ArrayAdapter<HashMap<String, String>> {
        private LayoutInflater mInflater;

        public TweetAdapter(Context context, List<HashMap<String, String>> items) {
            super(context, 0, items);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view;

            if (getItem(position).get("type").equals("separator")) {
                view = mInflater.inflate(R.layout.list_header, null);
                TextView tvHeaderText = (TextView)view.findViewById(R.id.tvHeaderText);
                tvHeaderText.setText(getItem(position).get("text"));
            } else {
                view = mInflater.inflate(R.layout.twitter_row, null);

                HashMap<String, String> tweet = getItem(position);
                if(tweet!=null) {

                    SimpleDraweeView sdvUserPicture = (SimpleDraweeView) view.findViewById(R.id.sdvUserPicture);
                    String url = tweet.get("avatar");
                    if (url != null) {
                        Uri uri = Uri.parse(url);
                        sdvUserPicture.setImageURI(uri);
                    }

                    //ImageLoader.getInstance().displayImage(getItem(position).get("avatar"), ((ImageView)view.findViewById(R.id.ivUserPicture)));

                    ((TextView) view.findViewById(R.id.tvName)).setText(tweet.get("name"));
                    ((TextView) view.findViewById(R.id.tvUsername)).setText(tweet.get("username"));
                    ((TextView) view.findViewById(R.id.tvTweet)).setText(tweet.get("text"));

                    view.findViewById(R.id.ibReply).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String url = "http://www.twitter.com/intent/tweet?text=" + getItem(position).get("username") + " ";
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });

                    view.findViewById(R.id.ibRetweet).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String url = "http://www.twitter.com/intent/retweet?tweet_id=" + getItem(position).get("id");
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                }
            }

            CommonMethods.applyCustomFont(getActivity(), view);
            return view;
        }

    }

}
