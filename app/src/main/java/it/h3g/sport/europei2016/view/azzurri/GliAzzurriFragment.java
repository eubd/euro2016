package it.h3g.sport.europei2016.view.azzurri;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.zenit.AdItem;
import com.zenit.ZenitAdListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import it.h3g.sport.europei2016.MainActivity;
import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.BaseFragment;
import it.h3g.sport.europei2016.view.team.TeamDetailsActivity;

/**
 * Created by mohammod.hossain on 5/10/2016.
 */
public class GliAzzurriFragment  extends BaseFragment{

    //ad view
    private AdView mAdView;
    AdRequest adRequest;

    ArrayList<JsonObject> playerList;
    ListView lvPlayers;


    public static GliAzzurriFragment newInstance() {
        GliAzzurriFragment fragment = new GliAzzurriFragment();
        return fragment;
    }


    public GliAzzurriFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        setToolbar();
    }

    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        final View customView = getActivity().getLayoutInflater().inflate(R.layout.azzurri_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openMenu();
            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.azzurri_players_fragment, container, false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mAdView!=null)
            mAdView.destroy();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout llAzzurriView = (LinearLayout) getView().findViewById(R.id.llAzzurriView);

        //adding AdView at bottom
        mAdView = new AdView(getActivity());
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(getString(R.string.banner_ad_unit_id_generic));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));


        if(CommonMethods.DEBUG_MODE)
            adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
        else
            adRequest = new AdRequest.Builder().build();

        /*mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                mAdView.setVisibility(View.GONE);
            }
        });
        llAzzurriView.addView(mAdView, params);*/



        playerList = new ArrayList<>();

        lvPlayers = (ListView)getView().findViewById(R.id.lvAzzurri);
        loadPlayers();

    }



    private void loadPlayers() {

        showProgressDialog();

        String url = CommonMethods.BASE_REQUEST_URL + File.separator + "azzurri.json";
        Log.d("AZZURRI URL", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());

        //loading file
        Ion.with(getActivity())
                .load(url)
                .setTimeout(CommonMethods.timeOut)
                .setHeader("Authorization", CommonMethods.basicAuth)
                        // have a ProgressBar get updated automatically with the percent
                        //.progressBar(progressBar)
                        // and a ProgressDialog
                        //.progressDialog(progressDialog)
                        // can also use a custom callback
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        System.out.println("" + downloaded + " / " + total);
                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                    }
                })
                .write(new File(getActivity().getFilesDir().getPath() + File.separator + fileName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {
                        // download done...
                        // do stuff with the File or error
                        if (e == null) {
                            if (file.exists()) {
                                System.out.println("Path: " + file.getAbsolutePath());


                                Ion.with(getActivity())
                                        .load("file://" + file.getAbsolutePath())
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                if (exception != null) {
                                                    closeProgressDialog();
                                                    Snackbar.make(getView(), exception, Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    parsePlayers(result);
                                                }
                                            }
                                        });

                            }
                        } else {
                            //error loading file
                        }
                    }
                });

    }


    //parsing azzurri json
    private synchronized void parsePlayers(JsonObject result){

        if(result != null) {

            if(result.has("player")){
                Object player = result.get("player");

                if(player instanceof JsonArray){
                    JsonArray jaArray = result.getAsJsonArray("player");
                    if (jaArray != null) {
                        if (jaArray.getClass() == JsonArray.class) {
                            JsonArray itemArray = jaArray.getAsJsonArray();
                            for (int i = 0; i < itemArray.size(); i++) {
                                playerList.add(itemArray.get(i).getAsJsonObject());
                            }
                        }//item
                    }
                } else if(player instanceof JsonObject){
                    JsonObject playerObj = result.getAsJsonObject("player");
                    playerList.add(playerObj);
                }
            }




        }


        //TODO
        lvPlayers.setAdapter(new PlayerAdapter(getActivity(), 0, playerList));
        closeProgressDialog();
    }

    //adapter
    private class PlayerAdapter extends ArrayAdapter<JsonObject> {
        LayoutInflater layoutInflater;
        Context context;

        List<JsonObject> list;

        public PlayerAdapter(Context context, int resource, List<JsonObject> objects) {
            super(context, resource, objects);
            this.context = context;
            this.list = objects;
            this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public JsonObject getItem(int position) {
            return this.list.get(position);
        }

        @Override
        public int getCount() {
            return this.list.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final      JsonObject player = getItem(position);
            if (player != null) {
                View playerView = LayoutInflater.from(this.context).inflate(R.layout.azzurri_player_row, parent, false);


                LinearLayout llPlayer = (LinearLayout)playerView.findViewById(R.id.llAzzurriPlayerView);

                SimpleDraweeView sdPlayerLogo = ((SimpleDraweeView)playerView.findViewById(R.id.sdvPlayerThumb));
                TextView tvPlayerName     = (TextView)playerView.findViewById(R.id.tvPlayerName);
                TextView tvPlayerRole     = (TextView)playerView.findViewById(R.id.tvPlayerRole);

                if(player.has("image")) {
                    String url = player.get("image").getAsString();
                    if (url != null) {
                        Uri uri = Uri.parse(url);
                        sdPlayerLogo.setImageURI(uri);
                    }
                }
                if(player.has("name_surname")) {
                    tvPlayerName.setText(player.get("name_surname").getAsString());
                }

                if(player.has("role")) {
                    tvPlayerRole.setText(player.get("role").getAsString());
                }


                llPlayer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (player != null) {
                            Intent intent = new Intent(getActivity(), AzzurriDetailsActivity.class);
                            try {
                                intent.putExtra("player", player.getAsJsonObject().toString());

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            Bundle bundle = new Bundle();
                            intent.putExtras(bundle);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                });

                CommonMethods.applyCustomFont(getActivity(), playerView);
                return  playerView;

            } else {

            }

            return  null;
        }
    }
}
