package it.h3g.sport.europei2016.view.gallery;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.zenit.AdItem;
import com.zenit.ZenitAdListener;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import it.h3g.sport.europei2016.MainActivity;
import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.BaseFragment;


public class PhotoGalleryFragment extends BaseFragment {

    //ad view
    private AdView mAdView;
    private AdView headerAd;
    AdRequest adRequest;

    ArrayList<JsonObject> photoList;
    ListView lvPhotoGalleries;


    //Zenit ad
    private List<AdItem> adItemList;
    private ZenitAdListener zenitAdListener;
    private View headerZenit;
    private boolean isZenitAvailable = false;
    private int adAvailable = 0;


    public static PhotoGalleryFragment newInstance() {
        PhotoGalleryFragment fragment = new PhotoGalleryFragment();
        return fragment;
    }


    public PhotoGalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        setToolbar();
    }

    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        final View customView = getActivity().getLayoutInflater().inflate(R.layout.photo_gallery_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openMenu();
            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.photo_gallery_fragment, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout llPhotoGalleryView = (LinearLayout) getView().findViewById(R.id.llPhotoGalleryView);

        //adding AdView at bottom
        mAdView = new AdView(getActivity());
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(getString(R.string.banner_ad_unit_id_generic));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));


        if(CommonMethods.DEBUG_MODE)
            adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
        else
            adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                mAdView.setVisibility(View.GONE);
            }
        });
        llPhotoGalleryView.addView(mAdView, params);

        photoList = new ArrayList<>();
        lvPhotoGalleries = (ListView)getView().findViewById(R.id.lvPhotoGalleries);

        loadPhotos();
    }

    private void loadPhotos() {
        showProgressDialog();
        String url = CommonMethods.BASE_REQUEST_URL + File.separator+"seriea/photogallery.json";
        Log.d("TEAM URL", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());

        //loading file
        Ion.with(getActivity())
                .load(url)
                .setHeader("Authorization", CommonMethods.basicAuth)
                        // have a ProgressBar get updated automatically with the percent
                        //.progressBar(progressBar)
                        // and a ProgressDialog
                        //.progressDialog(progressDialog)
                        // can also use a custom callback
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        System.out.println("" + downloaded + " / " + total);
                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                    }
                })
                .write(new File(getActivity().getFilesDir().getPath() + File.separator + fileName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {
                        // download done...
                        // do stuff with the File or error
                        if(e == null) {
                            if (file.exists()) {
                                System.out.println("Path: " + file.getAbsolutePath());

                                Ion.with(getActivity())
                                        .load("file://"+file.getAbsolutePath())
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                if (exception != null) {
                                                    closeProgressDialog();
                                                    Snackbar.make(getView(), exception, Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    parsePhotos(result);
                                                }
                                            }
                                        });

                            }
                        }else{
                            //error loading file
                        }
                    }
                });

    }

    private synchronized void parsePhotos(JsonObject result) {

        if(result != null){
            if (result.has("photogallery")){
                Object photos = result.get("photogallery");
                if(photos instanceof  JsonArray){
                    JsonArray jaArray = result.getAsJsonArray("photogallery");
                    if (jaArray != null) {
                        if (jaArray.getClass() == JsonArray.class) {
                            JsonArray itemArray = jaArray.getAsJsonArray();
                            for (int i = 0; i < itemArray.size(); i++) {
                                photoList.add(itemArray.get(i).getAsJsonObject());
                            }
                        }//item
                    }
                }else if(photos instanceof  JsonObject){
                    JsonObject jsonObj = result.getAsJsonObject("photogallery");
                    photoList.add(jsonObj);
                }
            }
        }



        lvPhotoGalleries.setAdapter(new PhotoGalleryAdapter(getActivity(), 0, photoList));
        closeProgressDialog();

    }


//adapter
    class PhotoGalleryAdapter extends ArrayAdapter<JsonObject> {
    LayoutInflater layoutInflater;
    Context context;

    List<JsonObject> list;

    public PhotoGalleryAdapter(Context context, int resource, List<JsonObject> objects) {
        super(context, resource, objects);
        this.context = context;
        this.list = objects;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public JsonObject getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final JsonObject photo = getItem(position);
        if (photo != null) {
            View photoView = LayoutInflater.from(this.context).inflate(R.layout.photo_gallery_row, parent, false);
            LinearLayout llPhoto = (LinearLayout) photoView.findViewById(R.id.llPhotoGalleryView);

            SimpleDraweeView sdvGalleryThumb = ((SimpleDraweeView) photoView.findViewById(R.id.sdvGalleryThumb));
            TextView tvPhotoTitle = ((TextView) photoView.findViewById(R.id.tvGalleryTitle));
            TextView tvPhotoSubTitle = ((TextView) photoView.findViewById(R.id.tvGallerySubtitle));

            if (photo.has("thumb")) {
                String url = photo.get("thumb").getAsString().replace("_t", "");
                if (url != null) {
                    Uri uri = Uri.parse(url);
                    sdvGalleryThumb.setImageURI(uri);
                }
            }

            if (photo.has("title")) {
                tvPhotoTitle.setText(photo.get("title").getAsString());
            }

            if (photo.has("subtitle")) {
                tvPhotoSubTitle.setText(photo.get("subtitle").getAsString());
            }

            llPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(photo != null){
                        Intent showGalleryIntent = new Intent(getActivity(), PhotoGalleryActivity.class);
                        showGalleryIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        showGalleryIntent.putExtra("galleryContent", photo.getAsJsonObject().toString());
                        startActivity(showGalleryIntent);
                    }
                }
            });

            return photoView;
        } else {

        }
        return null;
        }
    }

}