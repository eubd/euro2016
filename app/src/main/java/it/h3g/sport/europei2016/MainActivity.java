package it.h3g.sport.europei2016;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import io.presage.Presage;
import io.presage.utils.IADHandler;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.CreditFragment;
import it.h3g.sport.europei2016.view.OnFragmentInteractionListener;
import it.h3g.sport.europei2016.view.azzurri.GliAzzurriFragment;
import it.h3g.sport.europei2016.view.diretta.DirettaFragment;
import it.h3g.sport.europei2016.view.gallery.PhotoGalleryFragment;
import it.h3g.sport.europei2016.view.marcatori.StrikersFragment;
import it.h3g.sport.europei2016.view.news.NewsFragment;
import it.h3g.sport.europei2016.view.stadi.StadiumFragment;
import it.h3g.sport.europei2016.view.tabellone.TabelloneFragment;
import it.h3g.sport.europei2016.view.team.TeamsFragment;
import it.h3g.sport.europei2016.view.tweet.TwitterFragment;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener{
    //private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    //
    GoogleCloudMessaging gcm;
    String regid;

    private InterstitialAd interstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // configure Flurry
        FlurryAgent.setLogEnabled(false);
        // init Flurry
        FlurryAgent.init(this, CommonMethods.FLURRY_ID);


        //ogury
        Presage.getInstance().setContext(this.getBaseContext());
        Presage.getInstance().start();


        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.navigationView);
        /*navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                return false;
            }
        });*/


        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        //drawerLayout.openDrawer(Gravity.RIGHT);
        //ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close){
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);


            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        ListView listView = (ListView)navigationView.findViewById(R.id.listView);
        List<Menu> menuList = new ArrayList<>();
        menuList.add(new Menu(getString(R.string.News),1));
        menuList.add(new Menu(getString(R.string.Diretta),0));
        menuList.add(new Menu(getString(R.string.Tabellone),0));
        menuList.add(new Menu(getString(R.string.Marcatori),0));
        menuList.add(new Menu(getString(R.string.Gli_Azzurri),0));
        menuList.add(new Menu(getString(R.string.Le_squadre),0));
        menuList.add(new Menu(getString(R.string.Gli_stadi),0));
        //menuList.add(new Menu(getString(R.string.Fotogallery), 0));
        menuList.add(new Menu(getString(R.string.Tweet_VIP), 0));
        MenuAdapter adapter = new MenuAdapter(this,0,menuList);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CommonMethods.writeToDefaults(MainActivity.this, "menu_position_section", position);
                Menu menu = (Menu) parent.getItemAtPosition(position);
                if (menu != null) {
                    if (menu.title.equals(getString(R.string.News))) {
                        setContentFragment(NewsFragment.newInstance(), true);
                    } else if (menu.title.equals(getString(R.string.Diretta))) {
                        setContentFragment(DirettaFragment.newInstance(), true);
                    } else if (menu.title.equals(getString(R.string.Tabellone))) {
                        setContentFragment(TabelloneFragment.newInstance(), true);
                    } else if (menu.title.equals(getString(R.string.Le_squadre))) {
                        setContentFragment(TeamsFragment.newInstance(), true);
                    } else if (menu.title.equals(getString(R.string.Marcatori))) {
                        setContentFragment(StrikersFragment.newInstance(), true);
                    }else if (menu.title.equals(getString(R.string.Gli_Azzurri))) {
                        setContentFragment(GliAzzurriFragment.newInstance(), true);
                    }  else if (menu.title.equals(getString(R.string.Gli_stadi))) {
                        setContentFragment(StadiumFragment.newInstance(), true);
                    }else if (menu.title.equals(getString(R.string.Tweet_VIP))) {
                        setContentFragment(TwitterFragment.newInstance(), true);
                    }
                    /*else if(menu.title.equals(getString(R.string.Fotogallery))){
                        setContentFragment(PhotoGalleryFragment.newInstance(), true);
                    }*/
                    else {

                    }
                    //menu.setState(1);
                    //setContentFragment(new NewsFragment(), false);
                    MenuAdapter adapter1 = (MenuAdapter) parent.getAdapter();
                    if (adapter1 != null)
                        adapter1.notifyDataSetChanged();
                }

                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        setContentFragment(NewsFragment.newInstance(), false);


        //credits
        findViewById(R.id.linearLayoutCredits).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentFragment(CreditFragment.newInstance(), false);
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });


        //
        if (checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId();

            if (CommonMethods.getBooleanFromDefaults(this, "pushTokenNeedsToUpdate")) {
                CommonMethods.writeToDefaults(this, "pushTokenNeedsToUpdate", false);
                regid = "";
            }

            final String appID = CommonMethods.getStringFromDefaults(MainActivity.this, "appID");
            final String utknID = CommonMethods.getStringFromDefaults(MainActivity.this, "utknID");
            if (CommonMethods.isEmpty(appID) || CommonMethods.isEmpty(utknID)) {
                registerInBackground();
            }
        }


        //interstitial
        /*Presage.getInstance().adToServe("interstitial", new IADHandler() {

            @Override
            public void onAdNotFound() {
                // Here, just put the code you need in order to display an ad from an other network.

                // Create the interstitial.
                Log.e("MainActivity","Ogury not found. So google loading");
                interstitial = new InterstitialAd(MainActivity.this);
                interstitial.setAdUnitId(getString(R.string.Google_Interstitial));

                // Create ad request.
                AdRequest adRequest = new AdRequest.Builder().build();

                // Begin loading your interstitial.
                interstitial.loadAd(adRequest);

                interstitial.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();

                        if (interstitial.isLoaded()) {interstitial.show();
                        }
                    }
                });
            }

            @Override
            public void onAdFound() {
                Log.i("PRESAGE", "ad found");
            }

            @Override
            public void onAdClosed() {
                Log.i("PRESAGE", "ad closed");
            }
        });*/

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    public void setContentFragment(Fragment fragment, boolean addToBackStack) {

        if (fragment == null) {
            return;
        }
        final FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.content_frame);

        //show only if current fragment is not same as given fragment
        /*if (refreshView == false) {
            if (currentFragment != null && fragment.getClass().isAssignableFrom(currentFragment.getClass())) {
                return;
            }
        } else {
            refreshView = false;
        }*/

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.replace(R.id.content_frame, fragment, fragment.getClass().getName());

        if (addToBackStack) {
            //fragmentTransaction.add(R.id.content_frame, fragment, fragment.getClass().getName());
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }else{
            //fragmentTransaction.replace(R.id.content_frame, fragment, fragment.getClass().getName());
        }
        //fragmentTransaction.commitAllowingStateLoss();
        fragmentTransaction.commit();
        //fragmentManager.executePendingTransactions();

        //fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void openMenu(){
        if(drawerLayout!=null) {
            if(drawerLayout.isDrawerOpen(GravityCompat.START)){
                drawerLayout.closeDrawer(GravityCompat.START);
            }else {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        }
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/





    //Menu.
    private class Menu{
        String title;
        int state = 0;

        public Menu(String title, int state){
            this.title = title;
            this.state = state;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }
    }

    private class MenuAdapter extends ArrayAdapter<Menu>{
        List<Menu> list;
        Context context;

        public MenuAdapter(Context context, int resource, List<Menu> menuList){
            super(context, resource, menuList);
            this.context = context;
            this.list = menuList;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Menu getItem(int position) {
            return list.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View row = convertView;

            row = LayoutInflater.from(this.context).inflate(R.layout.listview_row_menu, parent, false);
            View viewState = row.findViewById(R.id.viewState);
            TextView textViewTitle = (TextView)row.findViewById(R.id.textViewTitle);

            //CommonMethods.applyCustomFont(this.context, row);

            Menu menu = getItem(position);
            if(CommonMethods.getIntFromDefaults(this.context,"menu_position_section")==position){
                menu.setState(1);
            }else{
                menu.setState(0);
            }
            if(menu!=null){
                if(menu.getState()==1)
                    viewState.setSelected(true);
                else
                    viewState.setSelected(false);
                textViewTitle.setText(menu.getTitle());
            }



            return row;
        }
    }


    // checking proper play services
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                //PLAY_SERVICES_RESOLUTION_REQUEST = 9000
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, 9000).show();
            }
            return false;
        }
        return true;
    }

    //getRegistrationId
    private String getRegistrationId(){
        Log.d("getRegistrationId","start");

        String registrationId = CommonMethods.getStringFromDefaults(MainActivity.this, "registration_id");//prefs.getString(PROPERTY_REG_ID, "");
        if(registrationId == null){
            return "";
        }

        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = CommonMethods.getIntFromDefaults(MainActivity.this, "appVersion"); //prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = CommonMethods.versionApp(MainActivity.this);
        if (registeredVersion != currentVersion) {

            HashMap<String, List<String>> parameters = new HashMap<>(CommonMethods.defaultParameters(MainActivity.this));
            parameters.put("action", Arrays.asList("action", "removePhoneToken"));
            parameters.put("token", Arrays.asList("token", registrationId));
            parameters.put("app_id", Arrays.asList("app_id", CommonMethods.getStringFromDefaults(MainActivity.this, "appID")));
            parameters.put("utknid", Arrays.asList("utknid", CommonMethods.getStringFromDefaults(MainActivity.this, "utknID")));

            Ion.with(this)
                    .load(CommonMethods.MESSAGING_ENDPOINT)
                    .setHeader("Authorization", CommonMethods.basicAuth)
                    .setBodyParameters(parameters)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            Log.d("removePhoneToken", "Result: " + result);
                        }
                    });


            return "";
        }
        Log.d("getRegistrationId","registrationId: "+registrationId);

        return registrationId;
    }

    //get app id/token id from backend and store/remove phone token
    private void sendRegistrationIdToBackend(){
        //Log.d("sendRegistrationIdToBackend", "start");

        final String appID = CommonMethods.getStringFromDefaults(MainActivity.this, "appID");
        final String utknID = CommonMethods.getStringFromDefaults(MainActivity.this, "utknID");

        //Log.d("sendRegistrationIdToBackend","appID: "+appID+"\tutknID: "+utknID);

        if (!CommonMethods.isEmpty(appID) && !CommonMethods.isEmpty(utknID)) {
            //there is app id, remove phone token
            HashMap<String, List<String>> parameters = new HashMap<>(CommonMethods.defaultParameters(MainActivity.this));
            parameters.put("action", Arrays.asList("action", "removePhoneToken"));
            parameters.put("app_id", Arrays.asList("app_id", appID));
            parameters.put("utknid", Arrays.asList("utknid", utknID));


            Ion.with(MainActivity.this)
                    .load(CommonMethods.MESSAGING_ENDPOINT)
                    .setHeader("Authorization", CommonMethods.basicAuth)
                    .setBodyParameters(parameters)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {

                            CommonMethods.writeToDefaults(MainActivity.this, "token", regid);

                            //store phone token
                            HashMap<String, List<String>> parameters = new HashMap<>(CommonMethods.defaultParameters(MainActivity.this));
                            parameters.put("action", Arrays.asList("action", "storePhoneToken"));
                            parameters.put("app_id", Arrays.asList("app_id", appID));
                            parameters.put("utknid", Arrays.asList("utknid", utknID));
                            parameters.put("token", Arrays.asList("token", regid));

                            CommonMethods.writeToDefaults(MainActivity.this, "appID", appID);
                            CommonMethods.writeToDefaults(MainActivity.this, "utknID", utknID);

                            Ion.with(MainActivity.this)
                                    .load(CommonMethods.MESSAGING_ENDPOINT)
                                    .setHeader("Authorization", CommonMethods.basicAuth)
                                    .setBodyParameters(parameters)
                                    .asJsonObject()
                                    .setCallback(new FutureCallback<JsonObject>() {
                                        @Override
                                        public void onCompleted(Exception e, JsonObject result) {
                                            Log.d("storePhoneToken 1", "Result: " + result);
                                        }
                                    });

                        }
                    });
        }else{
            //there is no app id
            //Get app id
            HashMap<String, List<String>> parameters = new HashMap<>(CommonMethods.defaultParameters(MainActivity.this));
            parameters.put("action", Arrays.asList("action", "getAppID"));
            parameters.put("bundle_id", Arrays.asList("bundle_id", getPackageName()));

            Ion.with(MainActivity.this)
                    .load(CommonMethods.MESSAGING_ENDPOINT)
                    .setHeader("Authorization", CommonMethods.basicAuth)
                    .setBodyParameters(parameters)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (result.get("status").getAsInt() == 0)
                                Log.d("getAppID", "Error: " + result.getAsJsonObject("errors").get("description").getAsString());
                            else {
                                final String appID = result.getAsJsonObject("data").get("app_id").getAsString();
                                CommonMethods.writeToDefaults(MainActivity.this, "appID", appID);

                                //getUtknid
                                HashMap<String, List<String>> parameters = new HashMap<>(CommonMethods.defaultParameters(MainActivity.this));
                                parameters.put("action", Arrays.asList("action", "getUtknid"));
                                parameters.put("request", Arrays.asList("request", "1"));

                                Ion.with(MainActivity.this)
                                        .load(CommonMethods.MESSAGING_ENDPOINT)
                                        .setHeader("Authorization", CommonMethods.basicAuth)
                                        .setBodyParameters(parameters)
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                final String utknID = result.getAsJsonObject("data").get("utknid").getAsString();
                                                CommonMethods.writeToDefaults(MainActivity.this, "utknID", utknID);

                                                //store phone token
                                                HashMap<String, List<String>> parameters = new HashMap<>(CommonMethods.defaultParameters(MainActivity.this));
                                                parameters.put("action", Arrays.asList("action", "storePhoneToken"));
                                                parameters.put("app_id", Arrays.asList("app_id", appID));
                                                parameters.put("utknid", Arrays.asList("utknid", utknID));
                                                parameters.put("token", Arrays.asList("token", regid));

                                                CommonMethods.writeToDefaults(MainActivity.this, "appID", appID);
                                                CommonMethods.writeToDefaults(MainActivity.this, "utknID", utknID);

                                                Ion.with(MainActivity.this)
                                                        .load(CommonMethods.MESSAGING_ENDPOINT)
                                                        .setHeader("Authorization", CommonMethods.basicAuth)
                                                        .setBodyParameters(parameters)
                                                        .asJsonObject()
                                                        .setCallback(new FutureCallback<JsonObject>() {
                                                            @Override
                                                            public void onCompleted(Exception e, JsonObject result) {
                                                                Log.d("storePhoneToken 2", "Result: " + result);
                                                            }
                                                        });

                                            }
                                        });

                            }
                        }
                    });

        }

    }



    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    public void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
                    }
                    regid = gcm.register(CommonMethods.SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    //storeRegistrationId(context, regid);
                    CommonMethods.writeToDefaults(MainActivity.this,"registration_id",regid);
                    CommonMethods.writeToDefaults(MainActivity.this, "appVersion", CommonMethods.versionApp(MainActivity.this));
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.d("NotificationDebug", msg);
            }
        }.execute(null, null, null);
    }


}
