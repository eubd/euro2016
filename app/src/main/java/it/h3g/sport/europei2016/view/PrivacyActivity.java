package it.h3g.sport.europei2016.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;
//import com.adjust.sdk.Adjust;
import com.flurry.android.FlurryAgent;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;


public class PrivacyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);

         /*
          replace for toolbar
         */
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewTitle.setText("Informativa privacy");

        ImageButton imageButton = (ImageButton) findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        // Mostriamo i termini di servizio
        WebView wbPrivacyTerms = (WebView)findViewById(R.id.wbPrivacyTerms);
        wbPrivacyTerms.loadUrl("file:///android_asset/terms.html");
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }*/

    /*
        @hossain
        using toolbar instead actionbar
     */



    protected void onResume() {
        super.onResume();
        //Adjust.onResume(this);
    }
    protected void onPause() {
        super.onPause();
        //Adjust.onPause();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        FlurryAgent.onStartSession(this, CommonMethods.FLURRY_ID);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
