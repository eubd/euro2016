package it.h3g.sport.europei2016.view.diretta;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.viewpagerindicator.TabPageIndicator;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.h3g.sport.europei2016.MainActivity;
import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.model.Match;
import it.h3g.sport.europei2016.view.BaseFragment;

/**
 * Created by oneenam on 03/05/16.
 */
public class DirettaFragment extends BaseFragment {
    //ArrayList<JsonObject> matchList;
    ArrayList<Match> giaList;
    ArrayList<Match> oggiList;
    ArrayList<Match> daList;

    ViewPager vpDirettaContent;
    TabPageIndicator tpiSections;


    public static DirettaFragment newInstance(){
        DirettaFragment fragment = new DirettaFragment();
        return fragment;
    }

    public DirettaFragment(){}

    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void onResume() {
        super.onResume();

        setToolbar();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.diretta_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*DisplayMetrics metrics = getResources().getDisplayMetrics();
        int densityDpi = (int)(metrics.density * 160f);
        Log.e(getTag(),"Density "+metrics.density);
        Log.e(getTag(),"Density "+ getResources().getDisplayMetrics().density);*/


        //matchList = new ArrayList<>();

        giaList = new ArrayList<>();
        oggiList = new ArrayList<>();
        daList = new ArrayList<>();






        /*tpiSections.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                //TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,48 + (90+i), getResources().getDisplayMetrics()))
                //((RelativeLayout.LayoutParams) getView().findViewById(R.id.ivChevron).getLayoutParams()).leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48 + (144 * i), getResources().getDisplayMetrics());
                Log.e(getTag(), "onPageSelected " + i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });*/


        CommonMethods.applyCustomFont(getActivity(), getView());

        loadDiretta();


    }


    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        final View customView = getActivity().getLayoutInflater().inflate(R.layout.diretta_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openMenu();
            }
        });

    }


    private void loadDiretta(){
        String url = CommonMethods.BASE_REQUEST_URL + File.separator + "diretta.json";
        Log.d("Diretta URL", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());

        //loading file
        Ion.with(getActivity())
                .load(url)
                .setHeader("Authorization", CommonMethods.basicAuth)
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        //System.out.println("" + downloaded + " / " + total);
                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                    }
                })
                .write(new File(getActivity().getFilesDir().getPath() + File.separator + fileName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {
                        if (e == null) {

                            if (file.exists()) {
                                System.out.println("Path: " + file.getAbsolutePath());


                                Ion.with(getActivity())
                                        .load("file://" + file.getAbsolutePath())
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                if (exception != null) {
                                                    closeProgressDialog();
                                                    Snackbar.make(getView(), exception, Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    parseDiretta(result);
                                                }
                                            }
                                        });


                            }

                        } else {
                            //show error
                        }
                    }
                });
    }

    private void parseDiretta(JsonObject result){
        if(result != null) {

            if(result.has("league")){
                Object leagues = result.get("league");
                if(leagues instanceof JsonArray) {
                    JsonArray jaArray = result.getAsJsonArray("league");

                    if (jaArray.getClass() == JsonArray.class) {

                        for (int i = 0; i < jaArray.size(); i++) {
                            JsonObject league = jaArray.get(i).getAsJsonObject();

                            if(league.has("match")){

                                Object matchs = league.get("match");
                                if(matchs instanceof JsonArray) {
                                    JsonArray matchArray = league.getAsJsonArray("match");
                                    if (matchArray.getClass() == JsonArray.class) {
                                        for (int j = 0; j < matchArray.size(); j++) {
                                            JsonObject match = matchArray.get(j).getAsJsonObject();

                                            if(match.has("@attributes")){
                                                JsonObject attributes = match.getAsJsonObject("@attributes");
                                                if(attributes.has("matchdate")){
                                                    JsonElement matchDate = attributes.get("matchdate");
                                                    if(matchDate!=null){

                                                        try {
                                                            SimpleDateFormat sDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                                            Date dateMatch = sDateFormat.parse(matchDate.getAsString());

                                                            String dateT = sDateFormat.format(new Date());
                                                            Log.e(getTag(),"Date: "+dateT);
                                                            Date date = sDateFormat.parse(dateT);

                                                            if(dateMatch.before(date)){
                                                                giaList.add(getMatch(match));
                                                            }else if(dateMatch.after(date)){
                                                                daList.add(getMatch(match));
                                                            }else {
                                                                oggiList.add(getMatch(match));
                                                            }

                                                        } catch (ParseException e) {
                                                            Log.e(getTag(),"Date parse exception on calenario adapter: "+e.toString());
                                                        }


                                                    }
                                                }
                                            }//json object
                                        }
                                    }

                                }else if(matchs instanceof JsonObject) {

                                    JsonObject match = league.getAsJsonObject("match");


                                    if(match.has("@attributes")){
                                        JsonObject attributes = match.getAsJsonObject("@attributes");
                                        if(attributes.has("matchdate")){
                                            JsonElement matchDate = attributes.get("matchdate");
                                            if(matchDate!=null){

                                                try {
                                                    SimpleDateFormat sDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                                    Date dateMatch = sDateFormat.parse(matchDate.getAsString());

                                                    String dateT = sDateFormat.format(new Date());
                                                    Log.e(getTag(),"Date: "+dateT);
                                                    Date date = sDateFormat.parse(dateT);

                                                    if(dateMatch.before(date)){
                                                        giaList.add(getMatch(match));
                                                    }else if(dateMatch.after(date)){
                                                        daList.add(getMatch(match));
                                                    }else {
                                                        oggiList.add(getMatch(match));
                                                    }

                                                } catch (ParseException e) {
                                                    Log.e(getTag(),"Date parse exception on calenario adapter: "+e.toString());
                                                }


                                            }
                                        }
                                    }//json object

                                }
                            }
                        }

                    }
                }

            }

            if(getView()!=null) {
                vpDirettaContent = (ViewPager) getView().findViewById(R.id.vpDirettaContent);
                vpDirettaContent.setAdapter(new DirettaPagerAdapter(getActivity().getSupportFragmentManager(), giaList, oggiList, daList));
                tpiSections = (TabPageIndicator) getView().findViewById(R.id.tpiSections);
                // Hack per la libreria di stocazzo
                tpiSections.setVisibility(View.VISIBLE);
                tpiSections.setViewPager(vpDirettaContent);
                tpiSections.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            }

        }

    }

    private Match getMatch(JsonObject result){
        Match match = new Match();

        if(result!=null && result.has("@attributes")){
            JsonObject attributes = result.getAsJsonObject("@attributes");

            if(attributes!=null && attributes.has("idmatch")){
                JsonElement idmatch = attributes.get("idmatch");
                if(idmatch!=null) {
                    match.setIdmatch(idmatch.toString().replace("\"", ""));
                }
            }

            if(attributes!=null && attributes.has("status")){
                JsonElement status = attributes.get("status");
                if(status!=null) {
                    match.setStatus(status.toString().replace("\"", ""));
                }
            }

            if(attributes!=null && attributes.has("matchdate")){
                JsonElement matchdate = attributes.get("matchdate");
                if(matchdate!=null) {
                    match.setMatchdate(matchdate.toString().replace("\"", ""));
                }
            }
            if(attributes!=null && attributes.has("matchtime")){
                JsonElement matchtime = attributes.get("matchtime");
                if(matchtime!=null) {
                    match.setMatchtime(matchtime.toString().replace("\"", ""));
                }
            }
        }



        JsonObject teamhome = result.getAsJsonObject("teamhome");
        if(teamhome!=null){
            if(teamhome.has("@attributes")){
                JsonObject attributes = teamhome.getAsJsonObject("@attributes");
                if(attributes.has("teamname")){

                    JsonElement teamname = attributes.get("teamname");
                    if(teamname!=null) {
                        match.setTeamname_home(teamname.getAsString());
                    }
                }

                if(attributes.has("numgoal")){
                    JsonElement numgoal = attributes.get("numgoal");
                    if(numgoal!=null){
                        if(numgoal.getAsString().trim().length()>0) {
                            match.setNumgoal_home(numgoal.getAsString());
                        }
                        else {
                            match.setNumgoal_home("0");
                        }
                    }else{
                        match.setNumgoal_home("0");                    }
                }
                if(attributes.has("teamlogo")){
                    JsonElement teamlogo = attributes.get("teamlogo");
                    if(teamlogo!=null){
                        String url = CommonMethods.BASE_URL + teamlogo.getAsString();
                        if(url!=null){
                            match.setTeamlogo_home(url);
                        }
                    }
                }
            }
        }


        //teamaway
        if(result.has("teamaway")){
            JsonObject teamaway = result.getAsJsonObject("teamaway");
            if(teamaway!=null){
                if(teamaway.has("@attributes")){
                    JsonObject attributes = teamaway.getAsJsonObject("@attributes");
                    if(attributes.has("teamname")){
                        JsonElement teamname = attributes.get("teamname");
                        if(teamname!=null) {
                            match.setTeamname_away(teamname.getAsString());
                        }
                    }

                    if(attributes.has("numgoal")){
                        JsonElement numgoal = attributes.get("numgoal");
                        if(numgoal!=null){
                            if(numgoal.getAsString().trim().length()>0) {
                                match.setNumgoal_away(numgoal.getAsString());
                            }
                            else {
                                match.setNumgoal_away("0");
                            }
                        }else{
                            match.setNumgoal_away("0");
                        }
                    }

                    if(attributes.has("teamlogo")){
                        JsonElement teamlogo = attributes.get("teamlogo");
                        if(teamlogo!=null){
                            String url = CommonMethods.BASE_URL + teamlogo.getAsString();
                            if(url!=null){
                                match.setTeamlogo_away(url);
                            }
                        }
                    }
                }
            }
        }


        //links
        if(result.has("links")){

            JsonObject links = result.getAsJsonObject("links");
            if(links.has("link")){

                Object linkObj = links.get("link");
                if(linkObj instanceof JsonArray){

                    JsonArray jaArray = links.getAsJsonArray("link");

                    if(jaArray!=null && (jaArray.getClass() == JsonArray.class)){
                        JsonArray itemArray = jaArray.getAsJsonArray();
                        for (int i = 0; i < itemArray.size(); i++) {
                            JsonObject link = itemArray.get(i).getAsJsonObject();

                            if(link!=null && link.has("@attributes")){
                                JsonObject attributes = link.getAsJsonObject("@attributes");
                                if (attributes.has("type")) {
                                    JsonElement type = attributes.get("type");

                                    if(type!=null && type.getAsString().equals("commentary")){
                                        if(attributes.has("path")){
                                            match.setCommentary(CommonMethods.BASE_URL + attributes.get("path").getAsString().replace(".xml",".json"));
                                        }
                                    }

                                    if(type!=null && type.getAsString().equals("matchdetails")){
                                        if(attributes.has("path")){
                                            match.setMatchdetails(CommonMethods.BASE_URL + attributes.get("path").getAsString().replace(".xml",".json"));
                                            //match.setMatchdetails(CommonMethods.BASE_URL + "/data/europei/giornate/1_giornata/3223966_matchdetails.json");
                                        }
                                    }
                                    if(type!=null && type.getAsString().equals("media")){
                                        if(attributes.has("path")){
                                            match.setMedia(attributes.get("path").getAsString());
                                        }
                                    }
                                }
                            }

                        }
                    }

                }
            }
        }

        return match;
    }


    /**
     * All fragments
     * */

    //adapter
    private class DirettaPagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<Match> giaList;
        ArrayList<Match> oggiList;
        ArrayList<Match> daList;

        public DirettaPagerAdapter(FragmentManager fm, ArrayList<Match> giaList, ArrayList<Match> oggiList,ArrayList<Match> daList) {
            super(fm);
            this.giaList = giaList;
            this.oggiList = oggiList;
            this.daList = daList;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return GiaGiocateFragment.newInstance(this.giaList);
            }else if (position == 1) {
                return OggiInDirettaFragment.newInstance(this.oggiList);
            }else {
                return DaGiocareFragment.newInstance(this.daList);
            }
        }


        @Override
        public CharSequence getPageTitle(int position) {
            switch (position)
            {
                case 0:
                    return getString(R.string.Gia_giocate);
                case 1:
                    return getString(R.string.Oggi_in_diretta);
                case 2:
                    return getString(R.string.Da_giocare);
                default:
                    return getString(R.string.Gia_giocate);
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

    }

    //================================================================================
    //già giocate
    public static class GiaGiocateFragment extends Fragment{
        ListView listView;
        DirettaAdapter adapter;

        private AdView topAd; //100
        private AdView bottomAd; //250
        AdRequest adRequest;



        public static GiaGiocateFragment newInstance(ArrayList<Match> matchList){
            GiaGiocateFragment fragment = new GiaGiocateFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("matchs", matchList);
            fragment.setArguments(bundle);
            return fragment;
        }

        public GiaGiocateFragment(){}

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.diretta_gia_giocate_fragment, container, false);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);


            Bundle bundle = getArguments();
            if (bundle != null) {
                listView = (ListView)getView().findViewById(R.id.listView);
                adapter = new DirettaAdapter(getActivity());

                //adding all items
                ArrayList<Match> matchList = bundle.getParcelableArrayList("matchs");

                Map<String, List<Match>> map = new HashMap<>();
                for (Match match : matchList) {

                    if (match.getMatchdate() != null) {
                        String key = match.getMatchdate();
                        if (map.get(key) == null) {
                            map.put(key, new ArrayList<Match>());
                        }
                        map.get(key).add(match);

                    }
                }//

                if(map.keySet().size()>0) {
                    for (String date : map.keySet()) {
                        ArrayList<Match> leagues = new ArrayList<>(map.get(date));
                        adapter.addSectionHeaderItem(leagues.get(0));

                        for (Match match : leagues) {
                            adapter.addItem(match);
                        }
                    }
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                });
            }

        }

    }

    //oggi in diretta
    public static class OggiInDirettaFragment extends Fragment{
        ListView listView;
        DirettaAdapter adapter;

        private AdView topAd; //100
        private AdView bottomAd; //250
        AdRequest adRequest;

        public static OggiInDirettaFragment newInstance(ArrayList<Match> matchList){
            OggiInDirettaFragment fragment = new OggiInDirettaFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("matchs", matchList);
            fragment.setArguments(bundle);
            return fragment;
        }

        public OggiInDirettaFragment(){}


        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.diretta_oggi_in_diretta_fragment, container, false);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            Bundle bundle = getArguments();
            if (bundle != null) {
                listView = (ListView)getView().findViewById(R.id.listView);
                adapter = new DirettaAdapter(getActivity());

                //adding all items
                ArrayList<Match> matchList = bundle.getParcelableArrayList("matchs");

                Map<String, List<Match>> map = new HashMap<>();
                for (Match match : matchList) {

                    if (match.getMatchdate() != null) {
                        String key = match.getMatchdate();
                        if (map.get(key) == null) {
                            map.put(key, new ArrayList<Match>());
                        }
                        map.get(key).add(match);

                    }
                }//

                if(map.keySet().size()>0) {
                    for (String date : map.keySet()) {
                        ArrayList<Match> leagues = new ArrayList<>(map.get(date));
                        adapter.addSectionHeaderItem(leagues.get(0));

                        for (Match match : leagues) {
                            adapter.addItem(match);
                        }
                    }
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                });
            }

        }
    }

    //da giocare
    public static class DaGiocareFragment extends BaseFragment{

        ListView listView;
        DirettaAdapter adapter;

        private AdView topAd; //100
        private AdView bottomAd; //250
        AdRequest adRequest;

        public static DaGiocareFragment newInstance(ArrayList<Match> matchList){
            DaGiocareFragment fragment = new DaGiocareFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("matchs", matchList);
            fragment.setArguments(bundle);
            return fragment;
        }

        public DaGiocareFragment(){}


        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.diretta_da_giocare_fragment, container, false);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            Bundle bundle = getArguments();
            if (bundle != null) {
                listView = (ListView)getView().findViewById(R.id.listView);
                adapter = new DirettaAdapter(getActivity());

                //adding all items
                ArrayList<Match> matchList = bundle.getParcelableArrayList("matchs");

                Map<String, List<Match>> map = new HashMap<>();
                for (Match match : matchList) {

                    if (match.getMatchdate() != null) {
                        String key = match.getMatchdate();
                        if (map.get(key) == null) {
                            map.put(key, new ArrayList<Match>());
                        }
                        map.get(key).add(match);

                    }
                }//

                if(map.keySet().size()>0) {
                    int i = 0;
                    for (String date : map.keySet()) {
                        ArrayList<Match> leagues = new ArrayList<>(map.get(date));
                        Match ma = new Match();
                        ma.setId(i);
                        ma.setMatchdate(leagues.get(0).getMatchdate());
                        ma.setHeader("header");
                        adapter.addSectionHeaderItem(ma);

                        for (Match match : leagues) {
                            match.setId(i++);
                            match.setHeader("item");
                            adapter.addItem(match);
                        }
                    }
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                });
            }


        }
    }

}