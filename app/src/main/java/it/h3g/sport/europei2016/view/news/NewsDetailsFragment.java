package it.h3g.sport.europei2016.view.news;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;


import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.BaseFragment;

import com.flurry.android.FlurryAgent;

/**
 * Created by oneenam on 06/04/16.
 */
public class NewsDetailsFragment extends BaseFragment{
    TextView tvNewsDescription;

    public static NewsDetailsFragment newInstance(Bundle bundle) {

        NewsDetailsFragment fragment = new NewsDetailsFragment();

        //Bundle args = new Bundle();
        //args.putBoolean("calciomercato", calciomercato);
        if(bundle!=null)
            fragment.setArguments(bundle);

        return fragment;
    }

    public NewsDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(getActivity(), CommonMethods.FLURRY_ID);
        setToolbar();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        FlurryAgent.onEndSession(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.news_details_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle!=null){
            //Log.e(getTag(),"Title: "+bundle.getString("title"));

            SimpleDraweeView sdvNewsPhoto = (SimpleDraweeView)getView().findViewById(R.id.sdvNewsPhoto);
            TextView tvNewsDate = (TextView)getView().findViewById(R.id.tvNewsDate);
            TextView tvNewsTitle = (TextView)getView().findViewById(R.id.tvNewsTitle);
            TextView tvNewsSubtitle = (TextView)getView().findViewById(R.id.tvNewsSubtitle);
            tvNewsDescription = (TextView)getView().findViewById(R.id.tvNewsDescription);

            CommonMethods.applyCustomFont(getActivity(), getView());

            String url = bundle.getString("photoURL");
            if(url!=null){
                Uri uri = Uri.parse(url);
                sdvNewsPhoto.setImageURI(uri);
            }


            tvNewsTitle.setText(Html.fromHtml(bundle.getString("articleTitle")));
            tvNewsSubtitle.setText(Html.fromHtml(bundle.getString("articleSubtitle")));
            tvNewsDate.setText(bundle.getString("articleDate"));
            tvNewsDescription.setText(Html.fromHtml(bundle.getString("articleContent")));
        }
    }


    /** action bar */
    private void setToolbar(){

        ActionBar actionBar = ((NewsDetailsActivity) getActivity()).getSupportActionBar();

        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        View customView = getActivity().getLayoutInflater().inflate(R.layout.news_details_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar =(Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);


        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);

        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        ImageButton imageButtonText = (ImageButton)customView.findViewById(R.id.imageButtonText);
        ImageButton imageButtonShare = (ImageButton)customView.findViewById(R.id.imageButtonShare);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        imageButtonText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                float fontSize = tvNewsDescription.getTextSize();
                float density = getResources().getDisplayMetrics().scaledDensity;
                int realSize = (int)(fontSize/density);
                Log.e(getTag(),""+realSize);

                switch (realSize)
                {
                    case 14:
                        tvNewsDescription.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                        break;
                    case 16:
                        tvNewsDescription.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        break;
                    case 18:
                        tvNewsDescription.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                        break;
                }
            }
        });


        imageButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = getArguments();
                if(bundle!=null) {

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(bundle.getString("articleTitle")) + CommonMethods.PLAY_STORE);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);

                }
            }
        });


    }

}
