package it.h3g.sport.europei2016.view.marcatori;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.zenit.AdItem;
import com.zenit.ZenitAdListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.h3g.sport.europei2016.MainActivity;
import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.BaseFragment;


public class StrikersFragment extends BaseFragment {

    //ad view
    private AdView mAdView;
    AdRequest adRequest;

    ArrayList<JsonObject> strikerList;
    ListView lvstrikers;




    public static StrikersFragment newInstance() {
        StrikersFragment fragment = new StrikersFragment();
        return fragment;
    }

    public StrikersFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        setToolbar();
    }

    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        final View customView = getActivity().getLayoutInflater().inflate(R.layout.marcatori_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openMenu();
            }
        });

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.strikers_fragment, container, false);
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout llStrikersView = (LinearLayout) getView().findViewById(R.id.llStrikersView);

        //adding AdView at bottom
        mAdView = new AdView(getActivity());
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(getString(R.string.banner_ad_unit_id_generic));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));


        if(CommonMethods.DEBUG_MODE)
            adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
        else
            adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                mAdView.setVisibility(View.GONE);
            }
        });
        llStrikersView.addView(mAdView, params);



        strikerList = new ArrayList<>();

        lvstrikers = (ListView)getView().findViewById(R.id.lvTeams);
        loadStrikers();

    }


    private void loadStrikers(){
        showProgressDialog();

        String url = CommonMethods.BASE_REQUEST_URL + File.separator+"marcatori.json";
        Log.d("TEAM URL", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());

        //loading file
        Ion.with(getActivity())
                .load(url)
                .setTimeout(CommonMethods.timeOut)
        .setHeader("Authorization", CommonMethods.basicAuth)
                        // have a ProgressBar get updated automatically with the percent
                        //.progressBar(progressBar)
                        // and a ProgressDialog
                        //.progressDialog(progressDialog)
                        // can also use a custom callback
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        System.out.println("" + downloaded + " / " + total);
                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                    }
                })
                .write(new File(getActivity().getFilesDir().getPath() + File.separator + fileName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {
                        // download done...
                        // do stuff with the File or error
                        if(e == null) {
                            if (file.exists()) {
                                System.out.println("Path: " + file.getAbsolutePath());
                                
                                Ion.with(getActivity())
                                        .load("file://" + file.getAbsolutePath())
                                        .setTimeout(CommonMethods.timeOut)
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                if (exception != null) {
                                                    closeProgressDialog();
                                                    Snackbar.make(getView(), exception, Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    parseStrikers(result);
                                                }
                                            }
                                        });

                            }
                        }else{
                            //error loading file
                        }
                    }
                });

    }


    private void parseStrikers(JsonObject result) {

        ArrayList<HashMap<String, String>> strikers = new ArrayList<>();
        HashMap<String, String> striker = new HashMap<>();
        striker.put("type", "header");
        strikers.add(striker);
        JsonElement player = result.getAsJsonObject("player");
        if(player instanceof JsonArray){
            JsonArray jaStrikers = player.getAsJsonArray();
            for (int i = 0; i < jaStrikers.size(); i++) {
                striker = new HashMap<>();
                striker.put("type", "striker");
                striker.put("teamname", jaStrikers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("team").getAsString());
                striker.put("name", jaStrikers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
                striker.put("goals", jaStrikers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("points").getAsString());
                striker.put("teamlogo", jaStrikers.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("teamlogo").getAsString());
                strikers.add(striker);
            }


        }else if(player instanceof JsonObject){
            JsonObject playerObj = player.getAsJsonObject();

            striker = new HashMap<>();
            striker.put("type", "striker");
            striker.put("teamname", playerObj.getAsJsonObject().getAsJsonObject("@attributes").get("team").getAsString());
            striker.put("name", playerObj.getAsJsonObject().getAsJsonObject("@attributes").get("surname").getAsString());
            striker.put("goals", playerObj.getAsJsonObject().getAsJsonObject("@attributes").get("points").getAsString());
            striker.put("teamlogo", playerObj.getAsJsonObject().getAsJsonObject("@attributes").get("team_logo").getAsString());
            strikers.add(striker);
        }

        ListView lvStrikers = (ListView) getView().findViewById(R.id.lvStrikers);
        lvStrikers.setAdapter(new StrikersAdapter(getActivity(), strikers));
        closeProgressDialog();
    }

    class StrikersAdapter extends ArrayAdapter<HashMap<String, String>> {
        private LayoutInflater mInflater;

        public StrikersAdapter(Context context, List<HashMap<String, String>> items) {
            super(context, 0, items);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            // Carichiamo il layout opportuno per l'elemento (primo, ultimo o in mezzo)
            if (getItem(position).get("type").equals("header"))
            {
                view = mInflater.inflate(R.layout.strikers_header, null);
                CommonMethods.applyCustomFont(getActivity(), view);
            } else {
                view = mInflater.inflate(R.layout.strikers_item, null);
                CommonMethods.applyCustomFont(getActivity(), view);

                TextView tvTeamRanking = (TextView) view.findViewById(R.id.tvTeamRanking);
                tvTeamRanking.setText(position + "°");
                SimpleDraweeView sdTeamLogo = ((SimpleDraweeView)view.findViewById(R.id.ivTeamLogo));
                String url = CommonMethods.BASE_URL+getItem(position).get("teamlogo").replace("_t", "");
                if(url != null){
                    Uri uri = Uri.parse(url);
                    sdTeamLogo.setImageURI(uri);
                }
                ((TextView) view.findViewById(R.id.tvTeamName)).setText(getItem(position).get("teamname"));
                ((TextView)view.findViewById(R.id.tvPlayerName)).setText(getItem(position).get("name"));
                ((TextView)view.findViewById(R.id.tvGoalsScored)).setText(getItem(position).get("goals"));
            }

            return view;
        }

    }
}
