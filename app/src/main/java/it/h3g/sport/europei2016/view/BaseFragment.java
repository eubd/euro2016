package it.h3g.sport.europei2016.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;

import com.tsengvn.typekit.TypekitContextWrapper;

import it.h3g.sport.europei2016.R;


public abstract class BaseFragment extends Fragment {
    protected OnFragmentInteractionListener mListener;
    private boolean openMenuOnBackPress = false;


    private ProgressDialog progressDialog;

    protected void showProgressDialog(){

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.Loading));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

    }
    protected void closeProgressDialog(){
        if(progressDialog.isShowing())
            progressDialog.dismiss();
    }


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

        //font
        TypekitContextWrapper.wrap(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public boolean openMenuOnBackPress() {
        return openMenuOnBackPress;
    }

    public void setOpenMenuOnBackPress(boolean openMenuOnBackPress) {
        this.openMenuOnBackPress = openMenuOnBackPress;
    }


    protected int getActionBarSize() {
        Activity activity = getActivity();
        if (activity == null) {
            return 0;
        }
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = activity.obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    protected int getScreenHeight() {
        Activity activity = getActivity();
        if (activity == null) {
            return 0;
        }
        return activity.findViewById(android.R.id.content).getHeight();
    }

}
