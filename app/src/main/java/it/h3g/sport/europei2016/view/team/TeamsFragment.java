package it.h3g.sport.europei2016.view.team;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.zenit.AdItem;
import com.zenit.ZenitAdListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import it.h3g.sport.europei2016.MainActivity;
import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.BaseFragment;

/**
 * created by hossain
 * @date 16 May 2016
 */
public class TeamsFragment extends BaseFragment {

    //ad view
    private AdView mAdView;
    private AdView headerAd;
    AdRequest adRequest;

    ArrayList<JsonObject> teamList;
    ListView lvTeams;

    //Zenit ad
    private List<AdItem> adItemList;
    private ZenitAdListener zenitAdListener;
    private View headerZenit;
    private boolean isZenitAvailable = false;
    private int adAvailable = 0;

    public static TeamsFragment newInstance() {
        TeamsFragment fragment = new TeamsFragment();
        return fragment;
    }


    public TeamsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        setToolbar();
    }

    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        final View customView = getActivity().getLayoutInflater().inflate(R.layout.teams_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openMenu();
            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.teams_fragment, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout llTeamsView = (LinearLayout) getView().findViewById(R.id.llTeamsView);

        //adding AdView at bottom
        mAdView = new AdView(getActivity());
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(getString(R.string.banner_ad_unit_id_generic));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));


        if(CommonMethods.DEBUG_MODE)
            adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
        else
            adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                mAdView.setVisibility(View.GONE);
            }
        });
        llTeamsView.addView(mAdView, params);



        teamList = new ArrayList<>();

        lvTeams = (ListView)getView().findViewById(R.id.lvTeams);
        loadTeams();

    }

    private void loadTeams() {

        showProgressDialog();

        String url = CommonMethods.BASE_REQUEST_URL + File.separator + "teams.json";
        Log.d("TEAM URL", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());

        //loading file
        Ion.with(getActivity())
                .load(url)
                .setHeader("Authorization", CommonMethods.basicAuth)
                        // have a ProgressBar get updated automatically with the percent
                        //.progressBar(progressBar)
                        // and a ProgressDialog
                        //.progressDialog(progressDialog)
                        // can also use a custom callback
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        System.out.println("" + downloaded + " / " + total);
                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                    }
                })
                .write(new File(getActivity().getFilesDir().getPath() + File.separator + fileName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {
                        // download done...
                        // do stuff with the File or error
                        if(e == null) {
                            if (file.exists()) {
                                System.out.println("Path: " + file.getAbsolutePath());


                                Ion.with(getActivity())
                                        .load("file://"+file.getAbsolutePath())
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                if (exception != null) {
                                                    closeProgressDialog();
                                                    Snackbar.make(getView(), exception, Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    parseTeams(result);
                                                }
                                            }
                                        });

                            }
                        }else{
                            //error loading file
                        }
                    }
                });

    }




    //parsing teams json
    private synchronized void parseTeams(JsonObject result){

        if(result != null) {

            if(result.has("team")){
                Object team = result.get("team");

                if(team instanceof JsonArray){
                    JsonArray jaArray = result.getAsJsonArray("team");
                    if (jaArray != null) {
                        if (jaArray.getClass() == JsonArray.class) {
                            JsonArray itemArray = jaArray.getAsJsonArray();
                            for (int i = 0; i < itemArray.size(); i++) {
                                teamList.add(itemArray.get(i).getAsJsonObject());
                            }
                        }//item
                    }
                } else if(team instanceof JsonObject){
                    JsonObject playerObj = result.getAsJsonObject("team");
                    teamList.add(playerObj);
                }
            }




        }


        //TODO
        lvTeams.setAdapter(new TeamAdapter(getActivity(), 0, teamList));
        closeProgressDialog();
    }




    //adapter
    private class TeamAdapter extends ArrayAdapter<JsonObject> {
        LayoutInflater layoutInflater;
        Context context;

        List<JsonObject> list;

        public TeamAdapter(Context context, int resource, List<JsonObject> objects) {
            super(context, resource, objects);
            this.context = context;
            this.list = objects;
            this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public JsonObject getItem(int position) {
            return this.list.get(position);
        }

        @Override
        public int getCount() {
            return this.list.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final      JsonObject team = getItem(position);
            if (team != null) {
                View teamView = LayoutInflater.from(this.context).inflate(R.layout.team_row, parent, false);
                //1st
                LinearLayout rlTeam = (LinearLayout)teamView.findViewById(R.id.llTeamView);

                SimpleDraweeView sdTeamLogo = ((SimpleDraweeView)teamView.findViewById(R.id.ivTeamLogo));
                TextView     tvTeamName     = (TextView)teamView.findViewById(R.id.tvTeamName);


                if(team.has("logo")) {
                    String url = team.get("logo").getAsString();
                    if (url != null) {
                        Uri uri = Uri.parse(url);
                        sdTeamLogo.setImageURI(uri);
                    }
                }
                if(team.has("team_name")) {
                    tvTeamName.setText(team.get("team_name").getAsString());
                }


                // rlTeam.setTag(team);
                rlTeam.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (team != null) {
                            Intent intent = new Intent(getActivity(), TeamDetailsActivity.class);

                            try {
                                intent.putExtra("teamName", team.get("team_name").getAsString());
                                intent.putExtra("teamDetails", team.getAsJsonObject().toString());

                            } catch (Exception e) {
                                //  intent.putExtra("articleDate", "");
                            }

                            Bundle bundle = new Bundle();
                            intent.putExtras(bundle);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                });

                return  teamView;

            } else {

            }

            return  null;
        }
    }

}
