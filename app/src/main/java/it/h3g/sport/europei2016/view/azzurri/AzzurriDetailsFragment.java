package it.h3g.sport.europei2016.view.azzurri;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.BaseFragment;
import it.h3g.sport.europei2016.view.team.TeamDetailsActivity;

/**
 * Created by mohammod.hossain on 4/17/2016.
 */
public class AzzurriDetailsFragment extends BaseFragment {

    //ad view
    private AdView mAdView;
    private AdView headerAd;
    AdRequest adRequest;
    ListView lvPlayerDetails;

    public static AzzurriDetailsFragment newInstance(Bundle bundle) {
        AzzurriDetailsFragment fragment = new AzzurriDetailsFragment();
        if(bundle!=null)
            fragment.setArguments(bundle);
        return fragment;
    }

    public AzzurriDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
       FlurryAgent.onStartSession(getActivity(), CommonMethods.FLURRY_ID);
        setToolbar();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
       FlurryAgent.onEndSession(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.player_details_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout llPlayerDetailsView = (LinearLayout) getView().findViewById(R.id.llAzzurriPlayerDetailsView);

        //adding AdView at bottom
        mAdView = new AdView(getActivity());
        mAdView.setAdUnitId(getString(R.string.banner_ad_unit_id_generic));
        mAdView.setAdSize(AdSize.BANNER);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));

        if(CommonMethods.DEBUG_MODE)
            adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
        else
            adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                mAdView.setVisibility(View.GONE);
            }
        });
        //llPlayerDetailsView.addView(mAdView, params);

        Bundle bundle = getArguments();
        if(bundle != null){
            SimpleDraweeView sdPlayerLogo   = ((SimpleDraweeView)llPlayerDetailsView.findViewById(R.id.sdvPlayerThumb));
            TextView tvPlayerName           = (TextView)llPlayerDetailsView.findViewById(R.id.tvPlayerName);
            TextView tvPlayerRole           = (TextView)llPlayerDetailsView.findViewById(R.id.tvPlayerRole);
            lvPlayerDetails                 = (ListView) getView().findViewById(R.id.lvPlayerDetails);
            CommonMethods.applyCustomFont(getActivity(), getView());

            JsonObject player = new JsonParser().parse(getArguments().getString("player")).getAsJsonObject();

            if(player.has("image")) {
                String url = player.get("image").getAsString();
                if (url != null) {
                    Uri uri = Uri.parse(url);
                    sdPlayerLogo.setImageURI(uri);
                }
            }
            if(player.has("name_surname")) {
                tvPlayerName.setText(player.get("name_surname").getAsString());
            }

            if(player.has("role")) {
                tvPlayerRole.setText(player.get("role").getAsString());
            }

            // Creiamo un po' di dati di test
            final ArrayList<HashMap<String, String>> listItems = new ArrayList<>();

            //details
            HashMap<String, String> listItem = new HashMap<>();

            listItem = new HashMap<>();
            //this is accordign to GUI

            String name = "";
            String role = "";
            String nationalClub = "";
            String club = "";
            String birthPlace = "";
            String heightWeight = "";
            String number = "";
            String presenceGolNational = "";
            String presenceGolQualification = "";
            String career = "";

            if (player.has("name_surname") && player.get("name_surname") != null )
                name = player.get("name_surname").getAsString();

            if (player.has("role") && player.get("role") != null )
                role = player.get("role").getAsString();

            if (player.has("club") && player.get("club") != null)
                club = player.get("club").getAsString();

            if (player.has("club_national") && player.get("club_national")  != null)
                nationalClub = player.get("club_national").getAsString();

            if (player.has("number") && player.get("number") != null)
                number = player.get("number").getAsString();

            if (player.has("height") && player.get("height") != null &&  player.has("weight")  )
                if(player.get("weight").isJsonObject() && player.get("weight") != null && player.get("weight").getAsJsonObject().toString().equals("{}")){
                    heightWeight = player.get("height").getAsString();
                }else{
                    heightWeight = player.get("height").getAsString()+"; " +player.get("weight").getAsString();
                }

            if (player.has("born_and_place") && player.get("born_and_place") != null)
                birthPlace = player.get("born_and_place").getAsString();

            if (player.has("presence_gol_national") && player.get("presence_gol_national") != null)
                presenceGolNational = player.get("presence_gol_national").getAsString();

            if (player.has("presence_gol_qualification") && player.get("presence_gol_qualification") != null)
                presenceGolQualification = player.get("presence_gol_qualification").getAsString();

            if (player.has("career") && player.get("career") != null)
                career = player.get("career").getAsString();

            listItem.put("text", "<strong>Name:</strong> " + name + "<br />" +
                    "<strong>Role:</strong> " + role + "<br />" +
                    "<strong>Squarde:</strong> " + nationalClub + "<br />" +
                    "<strong>N di maglia:</strong> " + number + "<br />" +
                    "<strong>Altezza e peso:</strong> " + heightWeight + "<br />" +
                    "<strong>Luogo e data di nascita:</strong> " + birthPlace+ "<br />" +
                    "<strong>Club:</strong> " + club + "<br />" +
                    "<strong>Preseze  e gol in nazionale:</strong> " + presenceGolNational + "<br />" +
                    "<strong>Preseze  e gol in nelle classificazioni:</strong> " + presenceGolQualification + "<br />" +
                     "<strong>Carriera:</strong> " + career   );

            listItems.add(listItem);
            lvPlayerDetails.setAdapter(new AzzurriDetailsAdapter(getActivity(), listItems));
        }
    }

    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((AzzurriDetailsActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        View customView = getActivity().getLayoutInflater().inflate(R.layout.player_details_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);

        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);

        TextView tvTitle = (TextView) customView.findViewById(R.id.textViewTitle);
        tvTitle.setText(getString(R.string.Gli_Azzurri));

        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    private class AzzurriDetailsAdapter extends ArrayAdapter<HashMap<String, String>> {
        private LayoutInflater mInflater;

        public AzzurriDetailsAdapter(Context context, List<HashMap<String, String>> items) {
            super(context, 0, items);
            mInflater = LayoutInflater.from(context);
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            final HashMap<String, String> item = getItem(position);
            view = mInflater.inflate(R.layout.player_details_item, null);
            TextView tvText = (TextView) view.findViewById(R.id.tvText);
            if(item != null){


                tvText.setText(Html.fromHtml(item.get("text")));


            }
            CommonMethods.applyCustomFont(getActivity(), view);
            return view;
        }

    }
}
