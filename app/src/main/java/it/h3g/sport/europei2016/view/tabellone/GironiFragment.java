package it.h3g.sport.europei2016.view.tabellone;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;

/**
 * Created by oneenam on 12/05/16.
 */
public class GironiFragment extends Fragment {
    ArrayList<JsonObject> gironiList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tabellone_gironi_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        gironiList = new ArrayList<>();


        String url = CommonMethods.BASE_REQUEST_URL + File.separator + "classifica.json";
        Log.d("Classifica URL", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());

        //loading file
        Ion.with(getActivity())
                .load(url)
                .setHeader("Authorization", CommonMethods.basicAuth)
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        //System.out.println("" + downloaded + " / " + total);
                    }
                })
                .write(new File(getActivity().getFilesDir().getPath() + File.separator + fileName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {
                        // download done...
                        // do stuff with the File or error
                        if (e == null) {
                            if (file.exists()) {
                                System.out.println("Path: " + file.getAbsolutePath());


                                Ion.with(getActivity())
                                        .load("file://" + file.getAbsolutePath())
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                if (exception != null) {
                                                    Snackbar.make(getView(), exception, Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    //TODO
                                                    parseGironi(result);
                                                }
                                            }
                                        });


                            }
                        } else {
                            //error loading file
                        }
                    }
                });


    }


    private void parseGironi(JsonObject result){
        if(result!=null){
            if(result.has("standings")){
                Object standings = result.get("standings");

                if(standings instanceof JsonArray){
                    JsonArray jaArray = result.getAsJsonArray("standings");
                    if (jaArray != null) {
                        if (jaArray != null) {
                            if (jaArray.getClass() == JsonArray.class) {
                                JsonArray itemArray = jaArray.getAsJsonArray();
                                for (int i = 0; i < itemArray.size(); i++) {

                                    JsonObject standing = itemArray.get(i).getAsJsonObject();
                                    if(standing!=null) {

                                        if (standing.has("@attributes")) {
                                            JsonObject attributes = standing.getAsJsonObject("@attributes");
                                            if (attributes.has("competition")) {
                                                JsonElement competition = attributes.get("competition");

                                                JsonObject header = new JsonObject();
                                                header.addProperty("header", "" + competition.getAsString());
                                                gironiList.add(header);
                                            }
                                        }


                                        if(standing.has("item")){
                                            Object items = standing.get("item");
                                            if(items instanceof  JsonArray){
                                                JsonArray array = standing.getAsJsonArray("item");
                                                if(array!=null){
                                                    if (array.getClass() == JsonArray.class) {
                                                        //JsonArray array = matchArray.getAsJsonArray();
                                                        for (int j = 0; j < array.size(); j++) {
                                                            gironiList.add(array.get(j).getAsJsonObject());
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }

                                    //gironiList.add(itemArray.get(i).getAsJsonObject());

                                }
                            }//item
                        }
                    }
                }
            }
            if(gironiList.size()>0){
                Log.e(getTag(),"Total Gironi: "+gironiList.size());

                ListView listView = (ListView)getView().findViewById(R.id.listView);
                listView.setAdapter(new GironiAdapter(getActivity(), 0, gironiList));

            }
        }
    }//


    //
    private class GironiAdapter extends ArrayAdapter<JsonObject> {
        List<JsonObject> list;
        Context context;
        public GironiAdapter(Context context, int resource, List<JsonObject> objects) {
            super(context, resource, objects);
            this.context = context;
            this.list = objects;
        }

        @Override
        public JsonObject getItem(int position) {
            return this.list.get(position);
        }

        @Override
        public int getCount() {
            int size = this.list.size();
            return size;
        }


        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            JsonObject item = getItem(position);
            if(item!=null){
                if(item.has("header")) {
                    //header
                    JsonElement type = item.get("header");
                    View header = LayoutInflater.from(this.context).inflate(R.layout.tabellone_gironi_header, parent, false);
                    TextView tvHeader = (TextView)header.findViewById(R.id.tvHeader);
                    if(type!=null){
                        tvHeader.setText(""+type.getAsString());
                    }

                    CommonMethods.applyCustomFont(this.context, header);
                    return header;
                }else{
                    //main row
                    View row = LayoutInflater.from(this.context).inflate(R.layout.tabellone_gironi_row, parent, false);

                    SimpleDraweeView ivTeamLogo = (SimpleDraweeView)row.findViewById(R.id.ivTeamLogo);
                    TextView tvTeamName = (TextView)row.findViewById(R.id.tvTeamName);
                    TextView tvPts = (TextView)row.findViewById(R.id.tvPts);
                    TextView tvG = (TextView)row.findViewById(R.id.tvG);
                    TextView tvV = (TextView)row.findViewById(R.id.tvV);
                    TextView tvP = (TextView)row.findViewById(R.id.tvP);
                    TextView tvS = (TextView)row.findViewById(R.id.tvS);


                    ///data/images/logos/

                    //date
                    if(item.has("@attributes")){
                        JsonObject attributes = item.getAsJsonObject("@attributes");
                        if(attributes.has("team_name")) {
                            JsonElement team_name = attributes.get("team_name");
                            if(team_name!=null){
                                tvTeamName.setText(""+team_name.getAsString());
                            }else{
                                tvTeamName.setText("");
                            }
                        }

                        if(attributes.has("team_logo")) {
                            JsonElement team_logo = attributes.get("team_logo");
                            String url = CommonMethods.BASE_URL + team_logo.getAsString();
                            if(url!=null){
                                Uri uri = Uri.parse(url);
                                ivTeamLogo.setImageURI(uri);
                            }

                        }

                        if(attributes.has("points")) {
                            JsonElement points = attributes.get("points");
                            if(points!=null){
                                tvPts.setText(""+points.getAsString());
                            }else{
                                tvPts.setText("0");
                            }
                        }

                    }



                    //total
                    if(item.has("total")){
                        JsonObject total = item.getAsJsonObject("total");

                        if(total!=null){
                            if(total.has("@attributes")){
                                JsonObject attributes = total.getAsJsonObject("@attributes");

                                if(attributes.has("g")){
                                    JsonElement g = attributes.get("g");
                                    if(g!=null)
                                        tvG.setText(""+g.getAsString());
                                    else
                                        tvG.setText("-");
                                }

                                if(attributes.has("v")){
                                    JsonElement v = attributes.get("v");
                                    if(v!=null)
                                        tvV.setText(""+v.getAsString());
                                    else
                                        tvV.setText("-");
                                }

                                if(attributes.has("p")){
                                    JsonElement p = attributes.get("p");
                                    if(p!=null)
                                        tvP.setText(""+p.getAsString());
                                    else
                                        tvP.setText("-");
                                }

                                if(attributes.has("gs")){
                                    JsonElement gs = attributes.get("gs");
                                    if(gs!=null)
                                        tvS.setText(""+gs.getAsString());
                                    else
                                        tvS.setText("-");
                                }


                            }
                        }
                    }


                    row.setTag(item);

                    CommonMethods.applyCustomFont(this.context, row);

                    return row;
                }
            }

            return null;
        }
    }




}

