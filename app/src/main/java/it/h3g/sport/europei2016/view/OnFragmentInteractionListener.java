package it.h3g.sport.europei2016.view;


import android.support.v4.app.Fragment;

public interface OnFragmentInteractionListener {

    public void setContentFragment(Fragment fragment, boolean addToBackStack);

    public void openMenu();
}
