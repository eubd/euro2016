package it.h3g.sport.europei2016.view.diretta;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.viewpagerindicator.TabPageIndicator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.model.Match;

/**
 * Created by oneenam on 10/05/16.
 */
public class DirettaDetailsActivity extends AppCompatActivity {

    TextView tvTeamHome;
    TextView tvGoal;
    TextView tvStatus;
    TextView tvTeamAway;
    SimpleDraweeView sdvTeamHome;
    SimpleDraweeView sdvTeamAway;

    ViewPager vpDirettaContent;
    TabPageIndicator tpiSections;
    CronacaFragment fragmentCronaca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diretta_details_activity);


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        //toolbar.setBackgroundColor((((CampionatoApplication) getApplication()).barColor));
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        //textViewTitle.setText(getString(R.string.Notifiche));
        textViewTitle.setText("");

        ImageButton imageButton = (ImageButton) findViewById(R.id.imageButton);
        ImageButton imageButtonRefresh = (ImageButton) findViewById(R.id.imageButtonRefresh);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fragmentCronaca = new CronacaFragment();
        imageButtonRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentCronaca.update();
            }
        });

        //Bundle bundle = getIntent().getExtras();

        //getIntent().getParcelableExtra()

        tvTeamHome = (TextView) findViewById(R.id.tvTeamHome);
        tvGoal = (TextView) findViewById(R.id.tvGoal);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvTeamAway = (TextView) findViewById(R.id.tvTeamAway);
        sdvTeamHome = (SimpleDraweeView) findViewById(R.id.sdvTeamHome);
        sdvTeamAway = (SimpleDraweeView) findViewById(R.id.sdvTeamAway);


        if (getIntent() != null) {
            Match match = getIntent().getParcelableExtra("match"); //bundle.getParcelable("match");
            if (match != null) {
                textViewTitle.setText("" + match.getTeamname_home() + " - " + match.getTeamname_away());
            }


            tvTeamHome.setText("" + match.getTeamname_home());
            tvStatus.setText("" + match.getStatus().toString().replace("\"", ""));
            String urlHome = match.getTeamlogo_home();
            if (urlHome != null) {
                Uri uri = Uri.parse(urlHome);
                sdvTeamHome.setImageURI(uri);
            }


            tvTeamAway.setText("" + match.getTeamname_away());
            String urlAway = match.getTeamlogo_away();
            if (urlAway != null) {
                Uri uri = Uri.parse(urlAway);
                sdvTeamAway.setImageURI(uri);
            }

            tvGoal.setText("" + match.getNumgoal_home() + "-" + match.getNumgoal_away());
            tvStatus.setText("" + match.getStatus());


            vpDirettaContent = (ViewPager) findViewById(R.id.vpDirettaContent);
            vpDirettaContent.setAdapter(new DirettaDetailsPagerAdapter(getSupportFragmentManager(), DirettaDetailsActivity.this, match));
            tpiSections = (TabPageIndicator) findViewById(R.id.tpiSections);
            // Hack per la libreria di stocazzo
            tpiSections.setVisibility(View.VISIBLE);
            tpiSections.setViewPager(vpDirettaContent);
            tpiSections.setBackgroundColor(ContextCompat.getColor(DirettaDetailsActivity.this, R.color.colorPrimary));


            //CommonMethods.applyCustomFont(DirettaDetailsActivity.this, findViewById(R.id.llDirettaDetails));
        }
    }



    //adapter
    public class DirettaDetailsPagerAdapter extends FragmentPagerAdapter {
        Context context;
        Match match;
        public DirettaDetailsPagerAdapter(FragmentManager fm, Context context, Match match) {
            super(fm);
            this.context = context;
            this.match = match;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                //CronacaFragment fragment = new CronacaFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("match",match);
                fragmentCronaca.setArguments(bundle);
                //fragment.update();
                return fragmentCronaca;

                //return CronacaFragment.newInstance(this.match);
            } else if (position == 1) {
                return TabellinoFragment.newInstance();
            } else if (position == 2) {
                return FormazioniFragment.newInstance();
            } else if (position == 3) {
                return PagelleFragment.newInstance();
            } else {
                Bundle bundle = new Bundle();
                bundle.putParcelable("match",match);
                fragmentCronaca.setArguments(bundle);
                return fragmentCronaca;
            }
        }


        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return this.context.getString(R.string.Cronaca);
                case 1:
                    return this.context.getString(R.string.Tabellino);
                case 2:
                    return this.context.getString(R.string.Formazioni);
                case 3:
                    return this.context.getString(R.string.Pagelle);
                default:
                    return this.context.getString(R.string.Cronaca);
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

    }


    //Cronaca
    public static class CronacaFragment extends Fragment {

        ProgressBar progressBar;
        ListView listView;
        ArrayList<HashMap<String, String>> commentary;

        public void update() {
            if (getView() != null) {
                loadComentary();
            }
        }


        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.diretta_details_cronaca_fragment, container, false);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            progressBar = (ProgressBar)getView().findViewById(R.id.progressBar);
            listView = (ListView)getView().findViewById(R.id.listView);


            loadComentary();
        }

        private void loadComentary(){

            Bundle bundle = getArguments();
            if (bundle != null) {
                Match match = bundle.getParcelable("match");

                if(match!=null){
                    String url = CommonMethods.BASE_URL + match.getCommentary();
                    if(url!=null){
                        url = url.replace(".xml",".json");
                        Log.e(getTag(),"commentary url: "+url);


                        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());

                        //loading file
                        Ion.with(getActivity())
                                .load(url)
                                .setHeader("Authorization", CommonMethods.basicAuth)
                                .progress(new ProgressCallback() {
                                    @Override
                                    public void onProgress(long downloaded, long total) {
                                        //System.out.println("" + downloaded + " / " + total);
                                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                                    }
                                })
                                .write(new File(getActivity().getFilesDir().getPath() + File.separator + fileName))
                                .setCallback(new FutureCallback<File>() {
                                    @Override
                                    public void onCompleted(Exception e, File file) {
                                        if (e == null) {

                                            if (file.exists()) {
                                                System.out.println("Path: " + file.getAbsolutePath());


                                                Ion.with(getActivity())
                                                        .load("file://" + file.getAbsolutePath())
                                                        .asJsonObject()
                                                        .setCallback(new FutureCallback<JsonObject>() {
                                                            @Override
                                                            public void onCompleted(Exception e, JsonObject result) {

                                                                String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                                if (exception != null) {
                                                                    progressBar.setVisibility(View.GONE);
                                                                    Snackbar.make(getView(), exception, Snackbar.LENGTH_LONG).show();
                                                                } else {
                                                                    parseCommentary(result);
                                                                }
                                                            }
                                                        });
                                            }

                                        } else {
                                            //show error
                                        }
                                    }
                                });

                    }
                }
            }

        }


        private void parseCommentary(JsonObject result){
            //refreshLiveView() {
            commentary = new ArrayList<>();
            if(result!=null) {
                JsonArray jaEvents;

                if(result.has("eventmatch")) {
                    if (result.get("eventmatch").getClass().equals(JsonObject.class)) {
                        jaEvents = new JsonArray();
                        jaEvents.add(result.getAsJsonObject("eventmatch"));
                    } else {
                        jaEvents = result.getAsJsonArray("eventmatch");
                    }

                    for (int i = 0; i < jaEvents.size(); i++) {
                        HashMap<String, String> event = new HashMap<>();

                        event.put("text", jaEvents.get(i).getAsJsonObject().get("description").getAsString());
                        if (jaEvents.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("extratime").getAsInt() != 0)
                            event.put("time", jaEvents.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("minute").getAsString() + "' + " + jaEvents.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("extratime").getAsString());
                        else
                            event.put("time", jaEvents.get(i).getAsJsonObject().getAsJsonObject("@attributes").get("minute").getAsString() + "'");
                        if (jaEvents.get(i).getAsJsonObject().getAsJsonObject("card").getAsJsonObject("@attributes").get("type").getAsString().equals("giallo"))
                            event.put("type", "yellowCard");
                        else if (jaEvents.get(i).getAsJsonObject().getAsJsonObject("card").getAsJsonObject("@attributes").get("type").getAsString().equals("rosso"))
                            event.put("type", "redCard");
                        else if (jaEvents.get(i).getAsJsonObject().getAsJsonObject("goal").getAsJsonObject("@attributes").get("type").getAsString().equals("tiro"))
                            event.put("type", "goal");
                        else if (jaEvents.get(i).getAsJsonObject().getAsJsonObject("substitution").getAsJsonObject("@attributes").get("type").getAsString().equals("sostituzione"))
                            event.put("type", "substitution");
                        else
                            event.put("type", "generic");

                        commentary.add(event);
                    }
                }

                listView.setAdapter(new CronacaAdapter(getActivity(), commentary));
                progressBar.setVisibility(View.GONE);
            }

        }

        //adapter
        class CronacaAdapter extends BaseAdapter {
            private LayoutInflater mInflater;
            ArrayList<HashMap<String, String>> items;
            AdView headerAd;
            int adAvailable = 0;

            public CronacaAdapter(Context context, ArrayList<HashMap<String, String>> items) {
                mInflater = LayoutInflater.from(context);
                this.items = items;

                headerAd = new AdView(getActivity());
                headerAd.setAdSize(AdSize.MEDIUM_RECTANGLE);
                headerAd.setLayoutParams(new AbsListView.LayoutParams(AdView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
                headerAd.setAdUnitId(getString(R.string.banner_ad_unit_id_diretta_big));

                headerAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        Log.d("Ad", "Got Ad");
                        adAvailable = 1;
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        super.onAdFailedToLoad(errorCode);
                        adAvailable = 0;
                        notifyDataSetChanged();
                    }
                });

                AdRequest adRequest;

                if(CommonMethods.DEBUG_MODE)
                    adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
                else
                    adRequest = new AdRequest.Builder().build();


                headerAd.loadAd(adRequest);
            }

            @Override
            public int getCount() {
                return items.size() + adAvailable;
            }

            @Override
            public Object getItem(int i) {
                return items.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                // Carichiamo il layout opportuno per l'elemento (primo, ultimo o in mezzo)
                if (position == 0 && adAvailable == 1) {
                    return headerAd;
                } else if (position == 0 && adAvailable == 0) {
                    if (getCount() == 1)
                        view = mInflater.inflate(R.layout.diretta_cronaca_row_last, null);
                    else
                        view = mInflater.inflate(R.layout.diretta_cronaca_row_first, null);
                } else if (position == getCount()-1) {
                    view = mInflater.inflate(R.layout.diretta_cronaca_row, null);
                } else {
                    if (adAvailable == 1 && position == 1)
                    {
                        view = mInflater.inflate(R.layout.diretta_cronaca_row_first, null);
                    } else {
                        view = mInflater.inflate(R.layout.diretta_cronaca_row, null);
                    }
                }

                CommonMethods.applyCustomFont(getActivity(), view);

                TextView tvTime = (TextView) view.findViewById(R.id.tvTime);
                ImageView ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
                TextView tvText = (TextView) view.findViewById(R.id.tvText);

                int adjustedPosition = position - adAvailable;

                tvTime.setText(items.get(adjustedPosition).get("time"));
                String icon = items.get(adjustedPosition).get("type");
                if (icon.equals("yellowCard"))
                {
                    ivIcon.setImageResource(R.drawable.cartellino_giallo);
                } else if (icon.equals("redCard")) {
                    ivIcon.setImageResource(R.drawable.cartellino_rosso);
                } else if (icon.equals("substitution")) {
                    ivIcon.setImageResource(R.drawable.ico_sostituizione);
                } else if (icon.equals("goal")) {
                    ivIcon.setImageResource(R.drawable.palla);
                } else if (icon.equals("generic")) {
                    ivIcon.setImageResource(R.drawable.cartellino_grigio);
                }

                tvText.setText(items.get(adjustedPosition).get("text"));

                return view;
            }

        }

    }


    //Tabellino
    public static class TabellinoFragment extends Fragment {

        public static TabellinoFragment newInstance(){
            TabellinoFragment fragment = new TabellinoFragment();
            return fragment;
        }

        public TabellinoFragment(){}

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.diretta_details_tabellino_fragment, container, false);
        }
    }

    //Formazioni
    public static class FormazioniFragment extends Fragment {

        public static FormazioniFragment newInstance(){
            FormazioniFragment fragment = new FormazioniFragment();
            return fragment;
        }

        public FormazioniFragment(){}

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.diretta_details_formazioni_fragment, container, false);
        }
    }


    //Pagelle
    public static class PagelleFragment extends Fragment {

        public static PagelleFragment newInstance(){
            PagelleFragment fragment = new PagelleFragment();
            return fragment;
        }

        public PagelleFragment(){}

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.diretta_details_pagelle_fragment, container, false);
        }
    }

}
