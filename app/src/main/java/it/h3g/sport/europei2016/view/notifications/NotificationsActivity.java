package it.h3g.sport.europei2016.view.notifications;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

//import com.adjust.sdk.Adjust;
import com.facebook.drawee.view.SimpleDraweeView;
import com.flurry.android.FlurryAgent;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;

public class NotificationsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);


         /*
          replace for toolbar
         */
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        //toolbar.setBackgroundColor((((CampionatoApplication) getApplication()).barColor));
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView textViewTitle = (TextView)findViewById(R.id.textViewTitle);
        textViewTitle.setText(getString(R.string.Notifiche));

        ImageButton imageButton = (ImageButton)findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }


    /*protected Toolbar mActionBarToolbar;
    protected Toolbar getActionBarToolbar() {
        if (mActionBarToolbar == null) {
            mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
            //  Log.e("mActionBarToolbar::::",""+mActionBarToolbar);
            if (mActionBarToolbar != null) {
                setSupportActionBar(mActionBarToolbar);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false);

            }
        }
        return mActionBarToolbar;
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onResume() {
        super.onResume();
        //Adjust.onResume(this);
    }
    protected void onPause() {
        super.onPause();
        //Adjust.onPause();
    }

    JsonArray checkmarks;

    @Override
    protected void onStart()
    {
        super.onStart();
        FlurryAgent.onStartSession(this, CommonMethods.FLURRY_ID);

        //final String path = ((CampionatoApplication) getApplication()).mSerie;
        //final String path = "seriea";
            Log.d("URL", CommonMethods.BASE_REQUEST_URL + "/teams.json");

            Ion.with(this)
                    .load(CommonMethods.BASE_REQUEST_URL + "/teams.json")
                    .setHeader("Authorization", CommonMethods.basicAuth)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {

                            if (result != null) {
                                if (result.has("team")) {

                                    Object object = result.get("team");

                                    if (object instanceof JsonArray) {

                                        JsonArray jaTeams = result.getAsJsonArray("team");

                                        if (jaTeams.getClass() == JsonArray.class) {
                                            final ArrayList<HashMap<String, String>> teams = new ArrayList<HashMap<String, String>>();
                                            HashMap<String, String> team;
                                            for (int i = 0; i < jaTeams.size(); i++) {
                                                team = new HashMap<String, String>();
                                                team.put("type", "team");
                                                JsonObject teamOBJ = jaTeams.get(i).getAsJsonObject();
                                                if(teamOBJ.has("@attributes")) {
                                                    JsonObject attributes = teamOBJ.getAsJsonObject("@attributes");

                                                    if(attributes.has("idteam"))
                                                        team.put("id", attributes.get("idteam").getAsString());
                                                    /*if(attributes.has("teamlogo"))
                                                        team.put("logo", attributes.get("teamlogo").getAsString());
                                                    team.put("name", jaTeams.get(i).getAsJsonObject().get("club").getAsString());*/
                                                }

                                                if(teamOBJ.has("logo"))
                                                    team.put("logo", teamOBJ.get("logo").getAsString());
                                                team.put("name", teamOBJ.get("team_name").getAsString());


                                                teams.add(team);
                                            }

                                            FileInputStream fis = null;
                                            try {
                                                File file = new File(getFilesDir().getPath() + File.separator);
                                                file.mkdirs();  // Creiamo la cartella se necessario

                                                fis = new FileInputStream(getFilesDir().getPath() + File.separator + "checkmarks.json");
                                                checkmarks = new JsonParser().parse(new InputStreamReader(fis)).getAsJsonArray();
                                            } catch (FileNotFoundException e1) {
                                                checkmarks = new JsonArray();
                                                for (int i = 0; i < jaTeams.size(); i++) {
                                                    JsonObject checkmark = new JsonObject();
                                                    checkmark.addProperty("news", false);
                                                    checkmark.addProperty("goals", false);
                                                    checkmarks.add(checkmark);
                                                }
                                            }

                                            ListView lvTeams = (ListView) findViewById(R.id.lvTeams);
                                            lvTeams.setAdapter(new NotificationsAdapter(NotificationsActivity.this, teams));
                                        }
                                    }
                                }
                            }
                        }
                    });
    }

    @Override
    protected void onStop()
    {
        super.onStop();

        if (checkmarks != null) {
            FileOutputStream fos = null;
            try {
                //String path = "seriea";
                File file = new File(getFilesDir().getPath());
                file.mkdirs();  // Creiamo la cartella se necessario
                fos = new FileOutputStream(getFilesDir().getPath() + File.separator + "checkmarks.json");
                fos.write(checkmarks.toString().getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FlurryAgent.onEndSession(this);
    }

    //adapter
    class NotificationsAdapter extends ArrayAdapter<HashMap<String, String>> {
        private LayoutInflater mInflater;

        public NotificationsAdapter(Context context, List<HashMap<String, String>> items) {
            super(context, 0, items);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = mInflater.inflate(R.layout.notification_item, null);


            HashMap<String, String> item = getItem(position);
            if(item!=null) {
                String url = item.get("logo").replace("_t", "");
                if (url != null) {
                    Uri uri = Uri.parse(url);
                    ((SimpleDraweeView)view.findViewById(R.id.ivTeamLogo)).setImageURI(uri);
                }

                ((TextView) view.findViewById(R.id.tvTeamName)).setText(item.get("name"));
                view.findViewById(R.id.bNews).setSelected(checkmarks.get(position).getAsJsonObject().get("news").getAsBoolean());
                view.findViewById(R.id.bGoals).setSelected(checkmarks.get(position).getAsJsonObject().get("goals").getAsBoolean());

                view.findViewById(R.id.bNews).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        view.setSelected(!view.isSelected());
                        checkmarks.get(position).getAsJsonObject().remove("news");
                        checkmarks.get(position).getAsJsonObject().addProperty("news", view.isSelected());

                        String registrationId = CommonMethods.getStringFromDefaults(NotificationsActivity.this, "registration_id");

                        // Copiamo per non modificare i global parameters
                        HashMap<String, List<String>> parameters = new HashMap<>(CommonMethods.defaultParameters(NotificationsActivity.this));
                        parameters.put("action", Arrays.asList("action", "updateToken"));
                        parameters.put("token", Arrays.asList("token", registrationId));
                        parameters.put("app_id", Arrays.asList("app_id", CommonMethods.getStringFromDefaults(NotificationsActivity.this, "appID")));
                        parameters.put("utknid", Arrays.asList("utknid", CommonMethods.getStringFromDefaults(NotificationsActivity.this, "utknID")));
                        parameters.put("team_id", Arrays.asList("team_id", getItem(position).get("id")));
                        parameters.put("for_news", Arrays.asList("team_id", view.isSelected() ? "1" : "0"));

                        Ion.with(NotificationsActivity.this)
                                .load(CommonMethods.MESSAGING_ENDPOINT)
                                .setHeader("Authorization", CommonMethods.basicAuth)
                                        //.setHeader("Authorization", "Basic " + Base64.encodeToString("campionato2015:c@m+i0n@t02015".getBytes(), 0))
                                .setBodyParameters(parameters)
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        Log.d("NotificationDebug", "Result: " + result);
                                    }
                                });
                    }
                });

                view.findViewById(R.id.bGoals).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        view.setSelected(!view.isSelected());
                        checkmarks.get(position).getAsJsonObject().remove("goals");
                        checkmarks.get(position).getAsJsonObject().addProperty("goals", view.isSelected());


                        String registrationId = CommonMethods.getStringFromDefaults(NotificationsActivity.this, "registration_id");

                        // Copiamo per non modificare i global parameters
                        HashMap<String, List<String>> parameters = new HashMap<>(CommonMethods.defaultParameters(NotificationsActivity.this));

                        parameters.put("action", Arrays.asList("action", "updateToken"));
                        parameters.put("token", Arrays.asList("token", registrationId));
                        parameters.put("app_id", Arrays.asList("app_id", CommonMethods.getStringFromDefaults(NotificationsActivity.this, "appID")));
                        parameters.put("utknid", Arrays.asList("utknid", CommonMethods.getStringFromDefaults(NotificationsActivity.this, "utknID")));
                        parameters.put("team_id", Arrays.asList("team_id", getItem(position).get("id")));
                        parameters.put("for_gol", Arrays.asList("team_id", view.isSelected() ? "1" : "0"));

                        Ion.with(NotificationsActivity.this)
                                .load(CommonMethods.MESSAGING_ENDPOINT)
                                .setHeader("Authorization", CommonMethods.basicAuth)
                                //.setHeader("Authorization", "Basic " + Base64.encodeToString("campionato2015:c@m+i0n@t02015".getBytes(), 0))
                                .setBodyParameters(parameters)
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        Log.d("NotificationDebug", "Result: " + result);
                                    }
                                });
                    }
                });
            }

            return view;
        }
    }

}
