package it.h3g.sport.europei2016.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import it.h3g.sport.europei2016.MainActivity;
import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;

/**
 * Created by oneenam on 09/05/16.
 */
public class CreditFragment extends BaseFragment {

    public static CreditFragment newInstance() {
        CreditFragment fragment = new CreditFragment();
        return fragment;
    }


    public CreditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        setToolbar();
    }

    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        final View customView = getActivity().getLayoutInflater().inflate(R.layout.teams_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openMenu();
            }
        });

        TextView textViewTitle = (TextView)customView.findViewById(R.id.textViewTitle);
        textViewTitle.setText("Credits");

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.credit_fragment, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView textViewVersion = (TextView)getView ().findViewById(R.id.textViewVersion);
        textViewVersion.setText("Versione "+CommonMethods.appVersion(getActivity()));

        TextView textViewInformativa = (TextView)getView ().findViewById(R.id.textViewInformativa);
        textViewInformativa.setText(Html.fromHtml("<u>Informativa privacy</u>"));
        textViewInformativa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PrivacyActivity.class);
                getActivity().startActivity(intent);

            }
        });

        CommonMethods.applyCustomFont(getActivity(), getView());
    }

}
