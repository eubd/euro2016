package it.h3g.sport.europei2016.view.tabellone;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.viewpagerindicator.TabPageIndicator;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.h3g.sport.europei2016.MainActivity;
import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.model.Match;
import it.h3g.sport.europei2016.view.BaseFragment;

/**
 * Created by oneenam on 03/05/16.
 */
public class TabelloneFragment extends BaseFragment {
    private AdView mAdView;
    AdRequest adRequest;

    ArrayList<Match> calendarList;
    ArrayList<Match> direttaList;


    ViewPager vpTabelloneContent;
    TabPageIndicator tpiSections;

    public static TabelloneFragment newInstance(){
        TabelloneFragment fragment = new TabelloneFragment();
        return fragment;
    }

    public TabelloneFragment(){}

    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void onResume() {
        super.onResume();

        setToolbar();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mAdView!=null)
            mAdView.destroy();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tabellone_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int densityDpi = (int)(metrics.density * 160f);
        Log.e(getTag(),"Density "+metrics.density);
        Log.e(getTag(), "Density " + getResources().getDisplayMetrics().density);


        LinearLayout llTabelloneView = (LinearLayout) getView().findViewById(R.id.llTabelloneView);
        //adding AdView at bottom
        mAdView = new AdView(getActivity());
        mAdView.setAdUnitId(getString(R.string.banner_ad_unit_id_news));
        mAdView.setAdSize(AdSize.BANNER);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));

        if(CommonMethods.DEBUG_MODE)
            adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
        else
            adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                mAdView.setVisibility(View.GONE);
            }
        });

        llTabelloneView.addView(mAdView, params);


        // Hack per la libreria di stocazzo
        /*tpiSections.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                //TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,48 + (90+i), getResources().getDisplayMetrics()))
                //((RelativeLayout.LayoutParams) getView().findViewById(R.id.ivChevron).getLayoutParams()).leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48 + (144 * i), getResources().getDisplayMetrics());
                Log.e(getTag(), "onPageSelected " + i);
                //RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) getView().findViewById(R.id.ivChevron).getLayoutParams();
                //lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                //getView().findViewById(R.id.ivChevron).setLayoutParams(lp);

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        */

        CommonMethods.applyCustomFont(getActivity(), getView());

        calendarList = new ArrayList<>();
        direttaList = new ArrayList<>();

        vpTabelloneContent = (ViewPager)getView().findViewById(R.id.vpTabelloneContent);
        tpiSections = (TabPageIndicator)getView().findViewById(R.id.tpiSections);


        loadCalendarioAndEliminaDiretta();




    }


    /** action bar */
    private void setToolbar() {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        final View customView = getActivity().getLayoutInflater().inflate(R.layout.tabellone_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar = (Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);
        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openMenu();
            }
        });

    }


    //loading calendario and elimina diretta
    private void loadCalendarioAndEliminaDiretta(){

        showProgressDialog();

        //Testing
        /*boolean calciomercato = false;
        if (getArguments() != null)
            calciomercato = getArguments().getBoolean("calciomercato");*/

        String url = CommonMethods.BASE_REQUEST_URL + File.separator + "calendario.json";
        Log.d("Calendario URL", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());

        //loading file
        Ion.with(getActivity())
                .load(url)
                .setHeader("Authorization", CommonMethods.basicAuth)
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        //System.out.println("" + downloaded + " / " + total);
                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                    }
                })
                .write(new File(getActivity().getFilesDir().getPath() + File.separator + fileName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {
                        // download done...
                        // do stuff with the File or error
                        if (e == null) {
                            if (file.exists()) {
                                System.out.println("Path: " + file.getAbsolutePath());


                                Ion.with(getActivity())
                                        .load("file://" + file.getAbsolutePath())
                                        .asJsonObject()
                                        .setCallback(new FutureCallback<JsonObject>() {
                                            @Override
                                            public void onCompleted(Exception e, JsonObject result) {

                                                String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                if (exception != null) {
                                                    closeProgressDialog();
                                                    Snackbar.make(getView(), exception, Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    parseCalendario(result);
                                                }
                                            }
                                        });


                            }
                        } else {
                            //error loading file
                        }
                    }
                });


    }


    //parsing news json
    private void parseCalendario(JsonObject result) {
        if(result != null) {
            if(result.has("league")){
                Object leagues = result.get("league");

                if(leagues instanceof JsonArray){
                    JsonArray jaArray = result.getAsJsonArray("league");
                    if (jaArray != null) {

                            if (jaArray.getClass() == JsonArray.class) {
                                JsonArray itemArray = jaArray.getAsJsonArray();
                                for (int i = 0; i < itemArray.size(); i++) {
                                    if(i>5){
                                        //elimina diretta
                                        JsonObject league = itemArray.get(i).getAsJsonObject();

                                        if(league.has("@attributes")){
                                            JsonObject attributes = league.getAsJsonObject("@attributes");
                                            if(attributes.has("description")) {
                                                JsonElement description = attributes.get("description");
                                                //Log.e(getTag(), "" + description.getAsString());

                                                /*JsonObject header = new JsonObject();
                                                header.addProperty("header", "" + description.getAsString().replace("Euro2016 ", ""));
                                                direttaList.add(header);*/

                                                if(description!=null){
                                                    Match match = new Match();
                                                    match.setHeader("" + description.getAsString().replace("Euro2016 ", ""));
                                                    direttaList.add(match);
                                                }


                                            }
                                        }


                                        if(league.has("match")){
                                            Object matchs = league.get("match");
                                            if(matchs instanceof  JsonArray){
                                                JsonArray matchArray = league.getAsJsonArray("match");
                                                if(matchArray!=null){
                                                    if (matchArray.getClass() == JsonArray.class) {

                                                        for (int j = 0; j < matchArray.size(); j++) {
                                                            direttaList.add(getMatch(matchArray.get(j).getAsJsonObject()));
                                                        }
                                                    }
                                                }
                                            }else if(matchs instanceof JsonObject){

                                                direttaList.add(getMatch(league.getAsJsonObject("match")));
                                                //direttaList.add(league.getAsJsonObject("match"));
                                            }
                                        }

                                        //direttaList.add(itemArray.get(i).getAsJsonObject());
                                    }else {

                                        //for calendario
                                        JsonObject league = itemArray.get(i).getAsJsonObject();

                                        if(league.has("@attributes")){
                                            JsonObject attributes = league.getAsJsonObject("@attributes");
                                            if(attributes.has("description")) {
                                                JsonElement description = attributes.get("description");
                                                Log.e(getTag(), "" + description.getAsString());

                                                /*JsonObject header = new JsonObject();
                                                header.addProperty("header", "" + description.getAsString().replace("Euro2016 ", ""));
                                                calendarList.add(header);*/
                                                if(description!=null){
                                                    Match match = new Match();
                                                    match.setHeader("" + description.getAsString().replace("Euro2016 ", ""));
                                                    calendarList.add(match);
                                                }

                                            }

                                        }

                                        if(league.has("match")){
                                            Object matchs = league.get("match");
                                            if(matchs instanceof  JsonArray){
                                                JsonArray matchArray = league.getAsJsonArray("match");
                                                if(matchArray!=null){
                                                    if (matchArray.getClass() == JsonArray.class) {
                                                        //JsonArray array = matchArray.getAsJsonArray();
                                                        for (int j = 0; j < matchArray.size(); j++) {
                                                            calendarList.add(getMatch(matchArray.get(j).getAsJsonObject()));
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //calendarList.add(itemArray.get(i).getAsJsonObject());
                                    }
                                }
                            }//item

                    }
                } /*else if(leagues instanceof JsonObject){
                    JsonObject playerObj = result.getAsJsonObject("league");
                    //calendarList.add(playerObj);
                }*/

            }
            TabellonePagerAdapter adapter = new TabellonePagerAdapter(getActivity().getSupportFragmentManager(), getActivity(), calendarList, direttaList);
            vpTabelloneContent.setAdapter(adapter);
            tpiSections.setViewPager(vpTabelloneContent);
            tpiSections.setVisibility(View.VISIBLE);
            tpiSections.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            adapter.notifyDataSetChanged();

        }
        closeProgressDialog();
    }


    private synchronized Match getMatch(JsonObject result){
        Match match = new Match();

        if(result!=null && result.has("@attributes")){
            JsonObject attributes = result.getAsJsonObject("@attributes");

            if(attributes!=null && attributes.has("idmatch")){
                JsonElement idmatch = attributes.get("idmatch");
                if(idmatch!=null) {
                    match.setIdmatch(idmatch.toString().replace("\"", ""));
                }
            }

            if(attributes!=null && attributes.has("status")){
                JsonElement status = attributes.get("status");
                if(status!=null) {
                    match.setStatus(status.toString().replace("\"", ""));
                }
            }

            if(attributes!=null && attributes.has("matchdate")){
                JsonElement matchdate = attributes.get("matchdate");
                if(matchdate!=null) {
                    match.setMatchdate(matchdate.toString().replace("\"", ""));
                }
            }
            if(attributes!=null && attributes.has("matchtime")){
                JsonElement matchtime = attributes.get("matchtime");
                if(matchtime!=null) {
                    match.setMatchtime(matchtime.toString().replace("\"", ""));
                }
            }
        }



        JsonObject teamhome = result.getAsJsonObject("teamhome");
        if(teamhome!=null){
            if(teamhome.has("@attributes")){
                JsonObject attributes = teamhome.getAsJsonObject("@attributes");
                if(attributes.has("teamname")){

                    JsonElement teamname = attributes.get("teamname");
                    if(teamname!=null) {
                        match.setTeamname_home(teamname.getAsString());
                    }
                }

                if(attributes.has("numgoal")){
                    JsonElement numgoal = attributes.get("numgoal");
                    if(numgoal!=null){
                        if(numgoal.getAsString().trim().length()>0) {
                            match.setNumgoal_home(numgoal.getAsString());
                        }
                        else {
                            match.setNumgoal_home("0");
                        }
                    }else{
                        match.setNumgoal_home("0");                    }
                }
                if(attributes.has("teamlogo")){
                    JsonElement teamlogo = attributes.get("teamlogo");
                    if(teamlogo!=null){
                        String url = CommonMethods.BASE_URL + teamlogo.getAsString();
                        if(url!=null){
                            match.setTeamlogo_home(url);
                        }
                    }
                }
            }
        }


        //teamaway
        if(result.has("teamaway")){
            JsonObject teamaway = result.getAsJsonObject("teamaway");
            if(teamaway!=null){
                if(teamaway.has("@attributes")){
                    JsonObject attributes = teamaway.getAsJsonObject("@attributes");
                    if(attributes.has("teamname")){
                        JsonElement teamname = attributes.get("teamname");
                        if(teamname!=null) {
                            match.setTeamname_away(teamname.getAsString());
                        }
                    }

                    if(attributes.has("numgoal")){
                        JsonElement numgoal = attributes.get("numgoal");
                        if(numgoal!=null){
                            if(numgoal.getAsString().trim().length()>0) {
                                match.setNumgoal_away(numgoal.getAsString());
                            }
                            else {
                                match.setNumgoal_away("0");
                            }
                        }else{
                            match.setNumgoal_away("0");
                        }
                    }

                    if(attributes.has("teamlogo")){
                        JsonElement teamlogo = attributes.get("teamlogo");
                        if(teamlogo!=null){
                            String url = CommonMethods.BASE_URL + teamlogo.getAsString();
                            if(url!=null){
                                match.setTeamlogo_away(url);
                            }
                        }
                    }
                }
            }
        }




        return match;
    }



    //================================================================================

    //adapter


    //================================================================================
    //già giocate


    //================================================================================
    //oggi in diretta


    //================================================================================
    //da giocare

}