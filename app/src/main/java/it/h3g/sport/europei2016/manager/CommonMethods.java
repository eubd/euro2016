package it.h3g.sport.europei2016.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Network;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import it.h3g.sport.europei2016.R;


public class CommonMethods {
    public static boolean DEBUG_MODE = true;
    public static String TEST_DEVICE_ADV_E = "181E704FDAC6C849CB7F8D699BF41961";
    public static String TEST_DEVICE_ADV_H = "";

    //public static String BASE_REQUEST_URL = "http://calcio.apps.tre.it/data";
    public static String BASE_REQUEST_URL = "http://europei.apps.tre.it/data/europei";
    public static String BASE_URL = "http://europei.apps.tre.it";
    //public static String BASE_URL = "http://calcio.apps.tre.it";
    public static String MESSAGING_ENDPOINT = "http://europei.apps.tre.it";
    //public static String MESSAGING_ENDPOINT = "http://calcio.apps.tre.it";

    public static String basicAuth = "Basic " + Base64.encodeToString("europei:nq2Dn3CW".getBytes(), Base64.NO_WRAP);
    //public static String basicAuth = "Basic " + Base64.encodeToString("campionato2015:c@m+i0n@t02015".getBytes(), Base64.NO_WRAP);
    public static String SENDER_ID = "254058671755";


    public static String PLAY_STORE = "\nScarica l’app Diretta Calcio: http://bit.ly/1nAsBWb";
    public static String FLURRY_ID = "6S3H95YKTKH3SPR9HM2G";


    //unused
    public static final String DATABASE_NAME = "europei2016";
    public static final int DATABASE_VERSION = 1;
    public static String normalFont = "avenir_roman.ttf";
    public static String boldFont = "avenir_bold.ttf";

    public static int timeOut = 60 * 60 * 1000;

    /** SharedPreferences **/
    public static String getStringFromDefaults(Context context, String key)
    {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPreferences.getString(key, null);
        } else
            return null;
    }

    public static boolean getBooleanFromDefaults(Context context, String key)
    {
        if (context != null)
        {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPreferences.getBoolean(key, false);
        } else
            return false;
    }

    public static int getIntFromDefaults(Context context, String key)
    {
        if (context != null)
        {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPreferences.getInt(key, -1);
        } else
            return -1;
    }

    public static void writeToDefaults(Context context, String key, int value)
    {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(key, value);
            editor.commit();
        }
    }

    public static void writeToDefaults(Context context, String key, boolean value)
    {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(key, value);
            editor.commit();
        }
    }

    public static void writeToDefaults(Context context, String key, String value)
    {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.commit();
        }
    }

    public static void applyCustomFont(Context context, View rootView) {
        if (rootView instanceof  ViewGroup) {
            ViewGroup list = (ViewGroup)rootView;
            for (int i = 0; i < list.getChildCount(); i++) {
                View view = list.getChildAt(i);
                if (view instanceof ViewGroup) {
                    applyCustomFont(context, (ViewGroup) view);
                } else if (view instanceof TextView) {
                    TextView textView = (TextView) view;
                    Typeface customTypeface = textView.getTypeface();
                    if (customTypeface != null) {
                        if (customTypeface.getStyle() == Typeface.BOLD) {
                            customTypeface = Typeface.createFromAsset(context.getAssets(), boldFont);
                        } else {
                            customTypeface = Typeface.createFromAsset(context.getAssets(), normalFont);
                        }
                        textView.setTypeface(customTypeface);
                    } else {
                        customTypeface = Typeface.createFromAsset(context.getAssets(), normalFont);
                        textView.setTypeface(customTypeface);
                    }
                }else if (view instanceof Button) {
                    Button button = (Button) view;
                    Typeface customTypeface = button.getTypeface();
                    if (customTypeface != null) {
                        if (customTypeface.getStyle() == Typeface.BOLD) {
                            customTypeface = Typeface.createFromAsset(context.getAssets(), boldFont);
                        } else {
                            customTypeface = Typeface.createFromAsset(context.getAssets(), normalFont);
                        }
                        button.setTypeface(customTypeface);
                    } else {
                        customTypeface = Typeface.createFromAsset(context.getAssets(), normalFont);
                        button.setTypeface(customTypeface);
                    }
                }
            }
        } else if (rootView instanceof TextView) {
            TextView textView = (TextView) rootView;
            Typeface customTypeface = textView.getTypeface();
            if (customTypeface != null) {
                if (customTypeface.getStyle() == Typeface.BOLD) {
                    customTypeface = Typeface.createFromAsset(context.getAssets(), boldFont);
                } else {
                    customTypeface = Typeface.createFromAsset(context.getAssets(), normalFont);
                }
                textView.setTypeface(customTypeface);
            } else {
                customTypeface = Typeface.createFromAsset(context.getAssets(), normalFont);
                textView.setTypeface(customTypeface);
            }
        }else if (rootView instanceof Button) {
            Button button = (Button) rootView;
            Typeface customTypeface = button.getTypeface();
            if (customTypeface != null) {
                if (customTypeface.getStyle() == Typeface.BOLD) {
                    customTypeface = Typeface.createFromAsset(context.getAssets(), boldFont);
                } else {
                    customTypeface = Typeface.createFromAsset(context.getAssets(), normalFont);
                }
                button.setTypeface(customTypeface);
            } else {
                customTypeface = Typeface.createFromAsset(context.getAssets(), normalFont);
                button.setTypeface(customTypeface);
            }
        }
    }



    /** using background*/
    public static void setBackground(Context context, View view, int resource){

        int sdk = Build.VERSION.SDK_INT;

        if(view!=null){
            if(view instanceof ImageView){

                ImageView imageView = (ImageView)view;
                Drawable drawable = imageView.getBackground();
                if(drawable!=null) drawable.setCallback(null);

                if(sdk < Build.VERSION_CODES.JELLY_BEAN)
                    imageView.setBackgroundResource(resource);
                else
                    imageView.setBackground(ContextCompat.getDrawable(context, resource));

            }else if(view instanceof Button){
                Button button = (Button)view;
                if(sdk < Build.VERSION_CODES.JELLY_BEAN)
                    button.setBackgroundResource(resource);
                else
                    button.setBackground(ContextCompat.getDrawable(context, resource));

            }else if(view instanceof RelativeLayout){
                RelativeLayout relativeLayout = (RelativeLayout)view;
                if(sdk < Build.VERSION_CODES.JELLY_BEAN)
                    relativeLayout.setBackgroundResource(resource);
                else
                    relativeLayout.setBackground(ContextCompat.getDrawable(context, resource));
            }else if(view instanceof LinearLayout){
                LinearLayout linearLayout = (LinearLayout)view;
                if(sdk < Build.VERSION_CODES.JELLY_BEAN)
                    linearLayout.setBackgroundResource(resource);
                else
                    linearLayout.setBackground(ContextCompat.getDrawable(context, resource));
            }else if(view instanceof ImageButton){
                ImageButton imageButton = (ImageButton)view;
                if(sdk < Build.VERSION_CODES.JELLY_BEAN)
                    imageButton.setBackgroundResource(resource);
                else
                    imageButton.setBackground(ContextCompat.getDrawable(context, resource));
            }else{
                if(sdk < Build.VERSION_CODES.JELLY_BEAN)
                    view.setBackgroundResource(resource);
                else
                    view.setBackground(ContextCompat.getDrawable(context, resource));

            }

        }

    }


    /** close keyboard */
    public static void closeKeyBoard(Context context, View view){
        //View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        /*InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (inputManager.isAcceptingText()) {
            //check if no view has focus:
            //View v = this.getCurrentFocus();
            if(view!=null)
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }*/

    }

    public static void showKeyboard(Context context, View view){
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            //inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        }catch (Exception e){
            e.printStackTrace();
        }
    }




    /** check internet connection */
    public static boolean isConnectingToInternet(Context mContext){

        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        }else {
            if (connectivityManager != null) {
                //noinspection deprecation
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
        }
        Toast.makeText(mContext,mContext.getString(R.string.Internet_connection_does_not_seem_to_be_available),Toast.LENGTH_LONG).show();
        return false;
    }


    //checking exception for the backend response
    public static String checkNetworkException(Context context, Exception e){
        if(e!=null){
            if (e instanceof UnknownHostException) {

                if(context!=null) {
                    return context.getResources().getString(R.string.Internet_connection_does_not_seem_to_be_available);
                }
                else
                    return "Internet connection does not seem to be available";

            }else if(e instanceof TimeoutException){
                return e.toString();
            }else {
                if(context!=null) {
                    return context.getResources().getString(R.string.Something_wrong)+"\n"+e.toString();
                }
                else
                    return "Something wrong:"+ e.toString();

            }
        }
        return null;
    }


    //checking which language is this
    public static boolean isItalian(Context context){
        String lng = Locale.getDefault().getDisplayLanguage();
        if(lng.contains("English")){
            return false;
        }
        return true;
    }


    /**
     * @detail current language
     * */
    public static String osLang() {
        String localLang = Locale.getDefault().getDisplayLanguage();

        if (localLang != null) {
            // Italian
            if (localLang.equalsIgnoreCase("it")
                    || localLang.equalsIgnoreCase("it_IT")
                    || localLang.equalsIgnoreCase("italiano")) {
                return "it";
            }
            // English
            else if (localLang.equalsIgnoreCase("en")
                    || localLang.equalsIgnoreCase("en_US")
                    || localLang.equalsIgnoreCase("en_UK")
                    || localLang.equalsIgnoreCase("en_CA")
                    || localLang.equalsIgnoreCase("english")) {
                return "en";
            }
        }

        return "it-IT";
    }

    /**
     * @detail application version
     * */
    public static String appVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    /*
    * @Details int format app version
    * */
    public static int versionApp(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return -1;
        }
    }

    /**
     * @detail get device id
     * */
    public String getDeviceId(Context context){
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String str = telephonyManager.getDeviceId();
        return str;
    }




    /**
     * @detail application version
     * */
    public static String osVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * @detail user device manufacturer and model
     * */

    public static String deviceModel(){
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;

        return manufacturer +"-"+model;
    }


    public static float getScale(Context context) {
        //if (scale == 0)
            //scale  = context.getResources().getDisplayMetrics().densityDpi / 160f;
        return context.getResources().getDisplayMetrics().densityDpi / 160f;
    }

    public static float dpToPx(Context context, float dp) {
        return dp * getScale(context);
    }


    public static Map<String, List<String>> defaultParameters(Context context){
        Map<String, List<String>> parameters = new HashMap<>();
        parameters.put("lang", Arrays.asList(osLang()));
        parameters.put("app_version",Arrays.asList(appVersion(context)));
        parameters.put("platform",Arrays.asList("android"));
        parameters.put("os_version",Arrays.asList(osVersion()));
        return parameters;
    }

    public static boolean isEmpty(Object object){
        if(object instanceof String){
            if(object==null)
                return true;

            if(object.toString().trim().length()==0)
                return true;
        }
        if(object instanceof ArrayList){
            if(object==null)
                return true;

            if(((ArrayList) object).size()==0)
                return true;
        }

        return false;
    }



}
