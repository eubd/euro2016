package it.h3g.sport.europei2016.view.news;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;


import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.nineoldandroids.animation.Animator;
import com.zenit.AdItem;
import com.zenit.ZenitAdListener;


import it.h3g.sport.europei2016.MainActivity;
import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.view.BaseFragment;
import it.h3g.sport.europei2016.view.notifications.NotificationsActivity;

/**
 * Created by oneenam on 06/04/16.
 */
public class NewsFragment extends BaseFragment{
    //ad view
    //private AdView mAdView;
    //private AdView headerAd;
    AdRequest adRequest;

    ArrayList<JsonObject> newsList;
    ArrayList<JsonObject> filteredNews;
    ListView lvNews;


    //Zenit ad
    private List<AdItem> adItemList;
    private ZenitAdListener zenitAdListener;
    private View headerZenit;
    private boolean isZenitAvailable = false;
    private int adAvailable = 0;

    //
    public static NewsFragment newInstance() {

        NewsFragment fragment = new NewsFragment();

        /*Bundle args = new Bundle();
        args.putBoolean("calciomercato", calciomercato);
        fragment.setArguments(args);*/

        return fragment;
    }

    public NewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void onResume() {
        super.onResume();

        setToolbar();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.news_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout llNewsView = (LinearLayout) getView().findViewById(R.id.llNewsView);

        //adding AdView at bottom
        /*mAdView = new AdView(getActivity());
        mAdView.setAdUnitId(getString(R.string.banner_ad_unit_id_news));
        mAdView.setAdSize(AdSize.BANNER);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));


        if(CommonMethods.DEBUG_MODE)
            adRequest = new AdRequest.Builder().addTestDevice(CommonMethods.TEST_DEVICE_ADV_E).build();
        else
            adRequest = new AdRequest.Builder().build();

        if(mAdView!=null) {
            mAdView.loadAd(adRequest);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    mAdView.setVisibility(View.GONE);
                }
            });
            llNewsView.addView(mAdView, params);
        }*/



        /*getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {*/


                //Zenit
                //Fetching ad
        //TODO
                adItemList = new ArrayList<>();
                /*try {


                    AdFetcher adFetcher = new AdFetcher(getActivity(), getString(R.string.zenit_app_key), new AdFetcher.Listener() {
                        @Override
                        public void onComplete() {
                            AdMap adMap = AdHolder.getAds();
                            if (adMap == null) {
                                //No ad
                                adAvailable = 0;
                                isZenitAvailable = false;
                                if (zenitAdListener != null) {
                                    zenitAdListener.onZenitAdLoadFail();
                                }
                            } else {
                                adAvailable = 1;
                                isZenitAvailable = true;
                                if (CommonMethods.DEBUG_MODE)
                                    Log.e(getTag(), "Total ad found: " + adMap.getAdItems().size());

                                for (AdItem adItem : adMap.getAdItems()) {
                                    adItem.trackAdLoadEvent(zenitAdListener);
                                    adItemList.add(adItem);
                                }
                                //adItemList = adMap.getAdItems();

                                if (zenitAdListener != null) {
                                    zenitAdListener.onZenitAdLoaded();
                                }

                            }
                        }
                    });
                    ExecutorService executor = Executors.newCachedThreadPool();
                    executor.submit(adFetcher);
                }catch (Exception e){
                    isZenitAvailable = false;
                }*/

                //Google ad
                /*headerAd = new AdView(getActivity());
                headerAd.setAdSize(AdSize.MEDIUM_RECTANGLE);
                headerAd.setLayoutParams(new AbsListView.LayoutParams(AdView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
                headerAd.setAdUnitId(getString(R.string.banner_ad_unit_id_news_big));

                headerAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        //adAvailable = 1;
                        //notifyDataSetChanged();
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        super.onAdFailedToLoad(errorCode);
                        //adAvailable = 0;
                        //notifyDataSetChanged();
                    }
                });*/

                //AdRequest adRequest = new AdRequest.Builder().build();
                //headerAd.loadAd(adRequest);
                //


                //headerZenit = layoutInflater.inflate(R.layout.list_ad_item, null);
                /*headerZenit = LayoutInflater.from(getActivity()).inflate(R.layout.ad_item_row, null, false);
                TextView tvTitle = (TextView) headerZenit.findViewById(R.id.tvTitle);
                TextView tvSubtitle = (TextView) headerZenit.findViewById(R.id.tvSubtitle);
                ImageView ivIcon = (ImageView) headerZenit.findViewById(R.id.ivIcon);


                if (adItemList.size() > 0) {
                    final AdItem adItem = adItemList.get(0);
                    if (tvTitle != null) {
                        tvTitle.setText(adItem.getTitle());
                    }
                    if (tvSubtitle != null) {
                        tvSubtitle.setText(adItem.getSubTitle());
                    }
                    if (ivIcon != null) {
                        ivIcon.setImageBitmap(adItem.getImage());
                    }

                    adItem.trackAdShowEvent(zenitAdListener);
                    headerZenit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //adClicked(position);
                            adItem.trackAdHideEvent(zenitAdListener);
                            if (zenitAdListener != null) {
                                zenitAdListener.onZenitAdClicked();
                            }
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(adItem.getClickUrl()));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    });
                }*/



            /*}
        });*/

        newsList = new ArrayList<>();

        lvNews = (ListView)getView().findViewById(R.id.lvNews);

        /*getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });*/

        if(CommonMethods.isConnectingToInternet(getActivity()))
            loadNews();


    }


    /** action bar */
    private void setToolbar(){

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setBackgroundDrawable(new ColorDrawable(R.color.color1));
        final View customView = getActivity().getLayoutInflater().inflate(R.layout.news_toolbar, null);
        CommonMethods.applyCustomFont(getActivity(), customView);
        actionBar.setCustomView(customView);
        Toolbar toolbar =(Toolbar) customView.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetsRelative(0, 0);

        ImageButton imageButton = (ImageButton)customView.findViewById(R.id.imageButton);
        ImageButton imageButtonSearch = (ImageButton)customView.findViewById(R.id.imageButtonSearch);
        imageButtonSearch.setTag("1");
        ImageButton imageButtonNotification = (ImageButton)customView.findViewById(R.id.imageButtonNotification);
        final EditText editTextSearch = (EditText)customView.findViewById(R.id.editTextSearch);
        editTextSearch.setTag("1");



        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.closeKeyBoard(getActivity(), v);
                editTextSearch.setText("");
                editTextSearch.setVisibility(View.INVISIBLE);
                customView.findViewById(R.id.imageButtonSearch).setTag("1");
                customView.findViewById(R.id.textViewTitle).setVisibility(View.VISIBLE);
                customView.findViewById(R.id.imageButtonSearch).setVisibility(View.VISIBLE);


                mListener.openMenu();
            }
        });



        editTextSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (editTextSearch.getRight() - editTextSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        editTextSearch.setText("");
                        editTextSearch.setVisibility(View.INVISIBLE);
                        customView.findViewById(R.id.imageButtonSearch).setTag("1");
                        customView.findViewById(R.id.textViewTitle).setVisibility(View.VISIBLE);
                        customView.findViewById(R.id.imageButtonSearch).setVisibility(View.VISIBLE);

                        CommonMethods.closeKeyBoard(getActivity(), editTextSearch);

                        return true;
                    }
                }
                return false;
            }
        });


        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                NewsAdapter adapter = (NewsAdapter) lvNews.getAdapter();
                if (adapter != null) {
                    adapter.getFilter().filter(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*editTextSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    String s = editTextSearch.getText().toString();
                    if(CommonMethods.isEmpty(s)){

                    }else {
                        Log.e(getTag(), s);
                    }
                    CommonMethods.closeKeyBoard(getActivity(), editTextSearch);

                    return true;

                }
                return false;
            }
        });*/


        imageButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getTag().toString().equals("1")){
                    customView.findViewById(R.id.imageButtonSearch).setTag("0");
                    customView.findViewById(R.id.textViewTitle).setVisibility(View.INVISIBLE);
                    customView.findViewById(R.id.imageButtonSearch).setVisibility(View.INVISIBLE);


                    YoYo.with(Techniques.SlideInLeft)
                            .duration(700)
                            .withListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                    /*YoYo.with(Techniques.FadeOut).duration(700)
                                            .playOn(imageButtonSearch);*/
                                    editTextSearch.setVisibility(View.VISIBLE);

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {

                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            }).playOn(v);

                }else{
                    customView.findViewById(R.id.imageButtonSearch).setTag("1");
                    customView.findViewById(R.id.textViewTitle).setVisibility(View.VISIBLE);
                    customView.findViewById(R.id.imageButtonSearch).setVisibility(View.VISIBLE);

                }

                editTextSearch.requestFocus();
                CommonMethods.showKeyboard(getActivity(), editTextSearch);
            }
        });


        imageButtonNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NotificationsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
        //TextView textViewTitle = (TextView)customView.findViewById(R.id.textViewTitle);

        /*String colorCode = CommonMethods.getStringFromDefaults(getActivity(), "colorCode");
        if(colorCode!=null)
            parent.setBackgroundColor(Color.parseColor(colorCode));
        else
            parent.setBackgroundColor(getResources().getColor(R.color.color1));*/
    }

    //loading news
    private void loadNews(){

        //showProgressDialog();

        //Testing
        /*boolean calciomercato = false;
        if (getArguments() != null)
            calciomercato = getArguments().getBoolean("calciomercato");*/

        String url = CommonMethods.BASE_REQUEST_URL + File.separator+ "news.json";
        Log.d("NEWS URL", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());

        //loading file
        Ion.with(getActivity())
                .load(url)
                .setHeader("Authorization", CommonMethods.basicAuth)
                        // have a ProgressBar get updated automatically with the percent
                        //.progressBar(progressBar)
                        // and a ProgressDialog
                        //.progressDialog(progressDialog)
                        // can also use a custom callback
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        System.out.println("" + downloaded + " / " + total);
                        //File file = new File(getActivity().getFilesDir().getPath() + File.separator + path + File.separator);
                    }
                })
                .write(new File(getActivity().getFilesDir().getPath() + File.separator + fileName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {
                        // download done...
                        // do stuff with the File or error
                        if(e == null) {
                            if (file.exists()) {
                                System.out.println("Path: " + file.getAbsolutePath());


                                if(getActivity()!=null) {
                                    Ion.with(getActivity())
                                            .load("file://" + file.getAbsolutePath())
                                            .asJsonObject()
                                            .setCallback(new FutureCallback<JsonObject>() {
                                                @Override
                                                public void onCompleted(Exception e, JsonObject result) {

                                                    String exception = CommonMethods.checkNetworkException(getActivity(), e);
                                                    if (exception != null) {
                                                        closeProgressDialog();
                                                        Snackbar.make(getView(), exception, Snackbar.LENGTH_LONG).show();
                                                    } else {
                                                        parseNews(result);
                                                    }
                                                }
                                            });
                                }



                            }
                        }else{
                            //error loading file
                        }
                    }
                });


    }

    //parsing news json
    private synchronized void parseNews(JsonObject result){

        if(result!=null){
            JsonElement channel = result.get("channel");
            if(channel!=null){
                JsonObject channelJson = channel.getAsJsonObject();
                if(channelJson!=null){
                    JsonElement item = channelJson.get("item");
                    if(item.getClass() == JsonArray.class){
                        JsonArray itemArray = item.getAsJsonArray();
                        for(int i = 0; i<itemArray.size(); i++){
                            newsList.add(itemArray.get(i).getAsJsonObject());
                        }
                    }//item
                    else{
                        if(item.getClass() == JsonObject.class){
                            newsList.add(item.getAsJsonObject());
                        }
                    }
                }//channelJson
            }//channel
        }//result


        Log.e(getTag(),"Total news without ad: "+newsList.size());

        /*int numberOfAds = newsList.size() / 6;
        int increment = 0;
        for (int i = 0; i <= numberOfAds; i++)
        {
            //TODO
            JsonObject ad = new JsonObject();
            ad.addProperty("type", "adv_"); // type -1 represents have to show ad
            newsList.add((i*6)+increment++, ad);
        }*/


        Log.e(getTag(),"Total news with ad: "+newsList.size());
        filteredNews = new ArrayList<>(newsList);

        lvNews.setAdapter(new NewsAdapter(getActivity(), 0, newsList));
        //new NewsAdapter(getActivity(),0, null);

        //closeProgressDialog();
    }


    //adapter
    private class NewsAdapter extends ArrayAdapter<JsonObject>{
        LayoutInflater layoutInflater;
        Context context;
        int numberOfNewsPerRow = 3;
        List<JsonObject> list;
        private Filter filter;

        public NewsAdapter(Context context, int resource, List<JsonObject> objects) {
            super(context, resource, objects);
            this.context = context;
            this.list = objects;
            this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(this.list.size()<numberOfNewsPerRow)
                numberOfNewsPerRow = this.list.size();


        }

        @Override
        public JsonObject getItem(int position) {
            return this.list.get(position);
        }

        @Override
        public int getCount() {

            int size = this.list.size();

            int div = size / numberOfNewsPerRow;
            int mod = size % numberOfNewsPerRow;

            // Per qualche motivo Math.ceil non funziona; arrotondiamo manualmente
            if (mod > 0 && mod < numberOfNewsPerRow)
            {
                mod = 1;
            }

            int numberOfAds = Math.max((size / 6) - 9, 0);
            int totalCount = adAvailable + div + mod + numberOfAds;
            Log.e(getTag(),"totalCount "+totalCount);
            return totalCount;

            //return this.list.size();
        }
        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //return super.getView(position, convertView, parent);

            //
            /*if (adAvailable == 1 && position == 0) {
                if(isZenitAvailable){
                    if(headerZenit!=null)
                        return headerAd;
                        //return headerZenit;
                    else return headerAd;

                }else return headerAd;
            }*/



            int indexCorrection = (position - adAvailable)/numberOfNewsPerRow;
            if (position == 0)
                indexCorrection = 0;
            final int actualPosition = Math.max(0, (numberOfNewsPerRow * (position - adAvailable - indexCorrection)) + indexCorrection);


            //JsonObject news = getItem(actualPosition);
            //final int actualPosition = position;

            JsonObject news = getItem(actualPosition);
            if(news!=null){
                JsonElement type = news.get("type");
                if(type!=null)
                    Log.e(getTag(),"Type: "+ type.getAsString());

                if(news.get("type")!=null){
                    //TODO there is two different add

                    /*if(isZenitAvailable){
                        if(headerZenit!=null)
                            return headerZenit;
                        else
                            return headerAd;
                    }else{
                        return headerAd;
                    }*/

                    return null;

                    //Show ad
                    //View  adView = layoutInflater.inflate(R.layout.ad_item_row, null);
                    /*View adView = LayoutInflater.from(this.context).inflate(R.layout.ad_item_row, parent, false);
                    return adView;*/
                }else{
                    //news section
                    //View  newsView = layoutInflater.inflate(R.layout.news_row, null);
                    View newsView = LayoutInflater.from(this.context).inflate(R.layout.news_row, parent, false);


                    //1st
                    RelativeLayout rlBigNews = (RelativeLayout)newsView.findViewById(R.id.rlBigNews);

                    SimpleDraweeView sdvNewsPhoto = (SimpleDraweeView)newsView.findViewById(R.id.sdvNewsPhoto);
                    TextView tvNewsDate = (TextView)newsView.findViewById(R.id.tvNewsDate);
                    TextView tvNewsTitle = (TextView)newsView.findViewById(R.id.tvNewsTitle);
                    TextView tvNewsSubtitle = (TextView)newsView.findViewById(R.id.tvNewsSubtitle);

                    JsonArray photos = news.get("enclosure").getAsJsonArray();
                    if (photos.size() > 0){

                        String url = photos.get(0).getAsJsonObject().get("@attributes").getAsJsonObject().get("url").getAsString();
                        if(url!=null){
                            Uri uri = Uri.parse(url);
                            sdvNewsPhoto.setImageURI(uri);
                        }

                    }

                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss ZZZ");
                        Date date = dateFormat.parse(news.get("pubDate").getAsString());
                        dateFormat = new SimpleDateFormat("dd/MM/yy - kk:mm");
                        tvNewsDate.setText(dateFormat.format(date));
                    } catch (ParseException e) {
                        tvNewsDate.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                    JsonElement title = news.get("title");
                    if(title!=null) {
                        tvNewsTitle.setText(Html.fromHtml(title.getAsString()));
                        Log.e(getTag(),"Title "+title.getAsString());
                    }
                    /*tvNewsTitle.post(new Runnable() {
                        @Override
                        public void run() {
                            if (tvNewsTitle.getLineCount() > 1) {
                                tvNewsSubtitle.setVisibility(View.GONE);
                            }
                        }
                    });*/
                    JsonElement subtitle = news.get("subtitle");
                    if(subtitle!=null)
                        tvNewsSubtitle.setText(Html.fromHtml(subtitle.getAsString()));

                    rlBigNews.setTag(news);
                    rlBigNews.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            JsonObject news = (JsonObject)v.getTag();
                            if(news!=null){
                                JsonElement title = news.get("title");
                                if(title!=null){
                                    //Log.e(getTag(),"Title: "+title);
                                    Intent intent = new Intent(getActivity(), NewsDetailsActivity.class);

                                    try {
                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss ZZZ");
                                        Date date = dateFormat.parse(news.get("pubDate").getAsString());
                                        dateFormat = new SimpleDateFormat("dd/MM/yy - kk:mm");
                                        intent.putExtra("articleDate", dateFormat.format(date));
                                        intent.putExtra("articleTitle", news.get("title").getAsString());
                                        intent.putExtra("articleSubtitle", news.get("subtitle").getAsString());
                                        intent.putExtra("articleContent", news.get("description").getAsString());
                                        intent.putExtra("photoURL", news.get("enclosure").getAsJsonArray().get(0).getAsJsonObject().get("@attributes").getAsJsonObject().get("url").getAsString());

                                    } catch (Exception e) {
                                        intent.putExtra("articleDate", "");
                                    }

                                    //Log.e(getTag(), "Title: " + title);
                                    Bundle bundle = new Bundle();
                                    //bundle.putString("title", title.getAsString());
                                    intent.putExtras(bundle);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    //mListener.setContentFragment(NewsDetailsFragment.newInstance(bundle), true);

                                }

                            }
                        }
                    });
                    //2nd
                    RelativeLayout rlFirstNews = (RelativeLayout)newsView.findViewById(R.id.rlFirstNews);
                    if (actualPosition + 1 == this.list.size()) {
                        // Non c'è una seconda notizia, nascondiamo la seconda riga
                        rlFirstNews.setVisibility(View.GONE);
                    } else {
                        SimpleDraweeView sdvFirstNewsPhoto = (SimpleDraweeView)newsView.findViewById(R.id.sdvFirstNewsPhoto);
                        TextView tvFirstNewsDate = (TextView)newsView.findViewById(R.id.tvFirstNewsDate);
                        TextView tvFirstNewsTitle = (TextView)newsView.findViewById(R.id.tvFirstNewsTitle);
                        TextView tvFirstNewsSubtitle = (TextView)newsView.findViewById(R.id.tvFirstNewsSubtitle);

                        int index1st = actualPosition + 1;
                        JsonObject news1st = getItem(index1st);
                        if(news1st!=null) {

                            JsonElement enclosure = news1st.get("enclosure");
                            if(enclosure!=null) {

                                photos = enclosure.getAsJsonArray();
                                if (photos.size() > 0) {
                                    String url = photos.get(0).getAsJsonObject().get("@attributes").getAsJsonObject().get("url").getAsString();
                                    if (url != null) {
                                        Uri uri = Uri.parse(url);
                                        sdvFirstNewsPhoto.setImageURI(uri);
                                    }
                                }

                                try {
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss ZZZ");
                                    Date date = dateFormat.parse(news1st.get("pubDate").getAsString());
                                    dateFormat = new SimpleDateFormat("dd/MM/yy - kk:mm");
                                    tvFirstNewsDate.setText(dateFormat.format(date));
                                } catch (ParseException e) {
                                    tvFirstNewsDate.setVisibility(View.GONE);
                                    e.printStackTrace();
                                }


                                title = news1st.get("title");
                                if (title != null)
                                    tvFirstNewsTitle.setText(Html.fromHtml(title.getAsString()));


                                subtitle = news1st.get("subtitle");
                                if (subtitle != null)
                                    tvFirstNewsSubtitle.setText(Html.fromHtml(subtitle.getAsString()));


                                rlFirstNews.setTag(news1st);
                                rlFirstNews.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        JsonObject news = (JsonObject) v.getTag();
                                        if (news != null) {
                                            JsonElement title = news.get("title");
                                            if (title != null) {

                                                Intent intent = new Intent(getActivity(), NewsDetailsActivity.class);

                                                try {
                                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss ZZZ");
                                                    Date date = dateFormat.parse(news.get("pubDate").getAsString());
                                                    dateFormat = new SimpleDateFormat("dd/MM/yy - kk:mm");
                                                    intent.putExtra("articleDate", dateFormat.format(date));
                                                    intent.putExtra("articleTitle", news.get("title").getAsString());
                                                    intent.putExtra("articleSubtitle", news.get("subtitle").getAsString());
                                                    intent.putExtra("articleContent", news.get("description").getAsString());
                                                    intent.putExtra("photoURL", news.get("enclosure").getAsJsonArray().get(0).getAsJsonObject().get("@attributes").getAsJsonObject().get("url").getAsString());

                                                } catch (Exception e) {
                                                    intent.putExtra("articleDate", "");
                                                }

                                                //Log.e(getTag(), "Title: " + title);
                                                Bundle bundle = new Bundle();
                                                //bundle.putString("title", title.getAsString());
                                                intent.putExtras(bundle);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                            }

                                        }
                                    }
                                });
                            }else {
                                rlFirstNews.setVisibility(View.GONE);
                            }
                            //
                        }


                    }

                    //3rd
                    RelativeLayout rlSecondNews = (RelativeLayout)newsView.findViewById(R.id.rlSecondNews);
                    int index2nd = actualPosition + 2;
                    if (index2nd < this.list.size()) {
                        SimpleDraweeView sdvSecondNewsPhoto = (SimpleDraweeView) newsView.findViewById(R.id.sdvSecondNewsPhoto);
                        TextView tvSecondNewsDate = (TextView) newsView.findViewById(R.id.tvSecondNewsDate);
                        TextView tvSecondNewsTitle = (TextView) newsView.findViewById(R.id.tvSecondNewsTitle);
                        TextView tvSecondNewsSubtitle = (TextView) newsView.findViewById(R.id.tvSecondNewsSubtitle);

                        JsonObject news2nd = getItem(index2nd);

                        if(news2nd!=null) {
                            JsonElement enclosure = news2nd.get("enclosure");
                            if(enclosure!=null) {

                                photos = enclosure.getAsJsonArray();
                                if (photos.size() > 0) {
                                    String url = photos.get(0).getAsJsonObject().get("@attributes").getAsJsonObject().get("url").getAsString();
                                    if (url != null) {
                                        Uri uri = Uri.parse(url);
                                        sdvSecondNewsPhoto.setImageURI(uri);
                                    }
                                }

                                try {
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss ZZZ");
                                    Date date = dateFormat.parse(news2nd.get("pubDate").getAsString());
                                    dateFormat = new SimpleDateFormat("dd/MM/yy - kk:mm");
                                    tvSecondNewsDate.setText(dateFormat.format(date));
                                } catch (ParseException e) {
                                    tvSecondNewsDate.setVisibility(View.GONE);
                                    e.printStackTrace();
                                }

                                title = news2nd.get("title");
                                if (title != null)
                                    tvSecondNewsTitle.setText(Html.fromHtml(title.getAsString()));


                                subtitle = news2nd.get("subtitle");
                                if (subtitle != null)
                                    tvSecondNewsSubtitle.setText(Html.fromHtml(subtitle.getAsString()));


                                rlSecondNews.setTag(news2nd);
                                rlSecondNews.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        JsonObject news = (JsonObject) v.getTag();
                                        if (news != null) {
                                            JsonElement title = news.get("title");
                                            if (title != null) {

                                                Intent intent = new Intent(getActivity(), NewsDetailsActivity.class);

                                                try {
                                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss ZZZ");
                                                    Date date = dateFormat.parse(news.get("pubDate").getAsString());
                                                    dateFormat = new SimpleDateFormat("dd/MM/yy - kk:mm");
                                                    intent.putExtra("articleDate", dateFormat.format(date));
                                                    intent.putExtra("articleTitle", news.get("title").getAsString());
                                                    intent.putExtra("articleSubtitle", news.get("subtitle").getAsString());
                                                    intent.putExtra("articleContent", news.get("description").getAsString());
                                                    intent.putExtra("photoURL", news.get("enclosure").getAsJsonArray().get(0).getAsJsonObject().get("@attributes").getAsJsonObject().get("url").getAsString());

                                                } catch (Exception e) {
                                                    intent.putExtra("articleDate", "");
                                                }

                                                //Log.e(getTag(), "Title: " + title);
                                                Bundle bundle = new Bundle();
                                                //bundle.putString("title", title.getAsString());
                                                intent.putExtras(bundle);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);

                                            }

                                        }
                                    }
                                });
                            }else {
                                rlSecondNews.setVisibility(View.GONE);
                            }
                        }

                    } else {
                        rlSecondNews.setVisibility(View.GONE);
                    }

                    return newsView;
                }
                //news.get(position).get
            }

            return null;
        }


        @Override
        public Filter getFilter() {
            if (filter == null)
                filter = new AppFilter<JsonObject>(list);
            return filter;
        }



        private class AppFilter<T> extends Filter {

            private ArrayList<JsonObject> sourceObjects;

            public AppFilter(List<JsonObject> objects) {
                sourceObjects = new ArrayList<JsonObject>();
                synchronized (this) {
                    sourceObjects.addAll(objects);
                }
            }

            @Override
            protected FilterResults performFiltering(CharSequence chars) {
                String filterSeq = chars.toString().toLowerCase();
                FilterResults result = new FilterResults();
                if (filterSeq != null && filterSeq.length() > 0) {
                    ArrayList<JsonObject> filter = new ArrayList<>();

                    for (JsonObject object : sourceObjects) {
                        JsonElement title = object.get("title");
                        if(title!=null) {
                            if (title.getAsString().toLowerCase().contains(filterSeq))
                                filter.add(object);
                            //if (object.getEventTitle().toString().toLowerCase().contains(filterSeq))
                            //filter.add(object);
                        }
                    }
                    result.count = filter.size();
                    result.values = filter;
                } else {
                    // add all objects
                    synchronized (this) {
                        result.values = sourceObjects;
                        result.count = sourceObjects.size();
                    }
                }
                return result;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                ArrayList<T> filtered = (ArrayList<T>) results.values;
                notifyDataSetChanged();
                clear();
                for (int i = 0, l = filtered.size(); i < l; i++) {
                    add((JsonObject) filtered.get(i));
                }
                notifyDataSetInvalidated();
            }
        }
    }



}
