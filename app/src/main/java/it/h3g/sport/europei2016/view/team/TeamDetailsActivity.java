package it.h3g.sport.europei2016.view.team;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.view.OnFragmentInteractionListener;



public class TeamDetailsActivity  extends AppCompatActivity implements OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null)
                setContentFragment(TeamDetailsFragment.newInstance(bundle), false);
        }
    }

    @Override
    public void setContentFragment(Fragment fragment, boolean addToBackStack) {

        if (fragment == null) {
            return;
        }
        final FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.content_frame);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.replace(R.id.content_frame, fragment, fragment.getClass().getName());

        if (addToBackStack) {
            //fragmentTransaction.add(R.id.content_frame, fragment, fragment.getClass().getName());
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }else{
            //fragmentTransaction.replace(R.id.content_frame, fragment, fragment.getClass().getName());
        }

        fragmentTransaction.commit();

    }

    @Override
    public void openMenu() {

    }
}