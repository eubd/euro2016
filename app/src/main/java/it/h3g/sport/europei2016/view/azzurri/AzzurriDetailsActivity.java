package it.h3g.sport.europei2016.view.azzurri;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.view.OnFragmentInteractionListener;
import it.h3g.sport.europei2016.view.team.TeamDetailsFragment;

/**
 * Created by mohammod.hossain on 5/11/2016.
 */
public class AzzurriDetailsActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null)
                setContentFragment(AzzurriDetailsFragment.newInstance(bundle), false);
        }
    }

    @Override
    public void setContentFragment(Fragment fragment, boolean addToBackStack) {

        if (fragment == null) {
            return;
        }
        final FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.content_frame);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment, fragment.getClass().getName());

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }else{
        }

        fragmentTransaction.commit();

    }

    @Override
    public void openMenu() {

    }
}
