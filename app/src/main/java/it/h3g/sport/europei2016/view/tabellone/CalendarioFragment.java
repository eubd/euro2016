package it.h3g.sport.europei2016.view.tabellone;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.h3g.sport.europei2016.R;
import it.h3g.sport.europei2016.manager.CommonMethods;
import it.h3g.sport.europei2016.model.Match;

/**
 * Created by oneenam on 12/05/16.
 */
public class CalendarioFragment extends Fragment {

        /*public static CalendarioFragment newInstance(ArrayList<Match> calendarList){
            CalendarioFragment fragment = new CalendarioFragment();
            Bundle bundle = new Bundle();
            //bundle.putSerializable("calendar",calendarList);
            bundle.putParcelableArrayList("calendar",calendarList);
            fragment.setArguments(bundle);
            return fragment;
        }

        public CalendarioFragment(){}*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        //getArguments().remove("calendar");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tabellone_calendario_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle!=null){
            //ArrayList<JsonObject> calendarList = (ArrayList<JsonObject>)bundle.getSerializable("calendar");
            ArrayList<Match> calendarList =bundle.getParcelableArrayList("calendar");
            if(calendarList!=null){
                Log.e(getTag(), "Total " + calendarList.size());
                ListView listView = (ListView)getView().findViewById(R.id.listView);
                listView.setAdapter(new CalendarioAdapter(getActivity(),0, calendarList));
            }
        }

    }


    //
    private class CalendarioAdapter extends ArrayAdapter<Match> {
        List<Match> list;
        Context context;
        public CalendarioAdapter(Context context, int resource, List<Match> objects) {
            super(context, resource, objects);
            this.context = context;
            this.list = objects;
        }

        @Override
        public Match getItem(int position) {
            return this.list.get(position);
        }

        @Override
        public int getCount() {
            int size = this.list.size();
            return size;
        }


        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Match match = getItem(position);
            if(match!=null){
                if(match.getHeader()!=null) {
                    //header
                    //JsonElement type = match.get("header");
                    View header = LayoutInflater.from(this.context).inflate(R.layout.tabellone_calendario_and_diretta_header, parent, false);
                    TextView tvHeader = (TextView)header.findViewById(R.id.tvHeader);
                    //if(type!=null)
                    {
                        tvHeader.setText(""+match.getHeader());
                    }

                    CommonMethods.applyCustomFont(this.context, header);
                    return header;
                }else{
                    //main row
                    View row = LayoutInflater.from(this.context).inflate(R.layout.tabellone_calendario_and_diretta_row, parent, false);
                    TextView tvDay = (TextView)row.findViewById(R.id.tvDay);
                    TextView tvMonth = (TextView)row.findViewById(R.id.tvMonth);

                    SimpleDraweeView ivTeamHome = (SimpleDraweeView)row.findViewById(R.id.ivTeamHome);
                    TextView tvTeamHome = (TextView)row.findViewById(R.id.tvTeamHome);

                    TextView tvGoal = (TextView)row.findViewById(R.id.tvGoal);
                    TextView tvTime = (TextView)row.findViewById(R.id.tvTime);

                    TextView tvTeamAway = (TextView)row.findViewById(R.id.tvTeamAway);
                    SimpleDraweeView ivTeamAway = (SimpleDraweeView)row.findViewById(R.id.ivTeamAway);

                    //updated

//date
                    if(match.getMatchdate()!=null) {
                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            Date date = dateFormat.parse(match.getMatchdate());

                            dateFormat = new SimpleDateFormat("dd");
                            tvDay.setText(dateFormat.format(date));

                            dateFormat = new SimpleDateFormat("MMMM");
                            tvMonth.setText(dateFormat.format(date));

                        } catch (ParseException e) {
                            tvDay.setText("-");
                            tvMonth.setText("-");
                            Log.e(getTag(), "Date parse exception on calenario adapter: " + e.toString());
                        }
                    }

                    tvTime.setText(match.getMatchtime());


                    StringBuilder goals = new StringBuilder();
                    //teamhome
                    tvTeamHome.setText(""+match.getTeamname_home());

                    goals.append(match.getNumgoal_home());
                    goals.append(" - ");

                    String url =  match.getTeamlogo_home();
                    if(url!=null){
                        Uri uri = Uri.parse(url);
                        ivTeamHome.setImageURI(uri);
                    }



                    //teamaway
                    tvTeamAway.setText(""+match.getTeamname_away());
                    goals.append(match.getNumgoal_away());
                    String urlAway =  match.getTeamlogo_away();
                    if(urlAway!=null){
                        Uri uri = Uri.parse(urlAway);
                        ivTeamAway.setImageURI(uri);
                    }



                    //goals
                    tvGoal.setText(""+goals.toString());

                    row.setTag(match);
                    //updated

                    //date
                        /*if(match.has("@attributes")){
                            JsonObject attributes = match.getAsJsonObject("@attributes");
                            if(attributes.has("matchdate")) {
                                JsonElement matchdate = attributes.get("matchdate");

                                if(matchdate!=null){
                                    try {
                                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                        Date date = dateFormat.parse(matchdate.getAsString());

                                        dateFormat = new SimpleDateFormat("dd");
                                        tvDay.setText(dateFormat.format(date));

                                        dateFormat = new SimpleDateFormat("MMMM");
                                        tvMonth.setText(dateFormat.format(date));

                                    } catch (ParseException e) {
                                        tvDay.setText("-");
                                        tvMonth.setText("-");
                                        Log.e(getTag(),"Date parse exception on calenario adapter: "+e.toString());
                                    }
                                }
                            }

                            if(attributes.has("matchtime")) {
                                JsonElement matchtime = attributes.get("matchtime");
                                if(matchtime!=null){
                                    tvTime.setText(""+matchtime.getAsString());
                                }else{
                                    tvTime.setText("--");
                                }
                            }
                        }
                        StringBuilder goals = new StringBuilder();
                        //teamhome
                        if(match.has("teamhome")){
                            JsonObject teamhome = match.getAsJsonObject("teamhome");
                            if(teamhome!=null){
                                if(teamhome.has("@attributes")){
                                    JsonObject attributes = teamhome.getAsJsonObject("@attributes");
                                    if(attributes.has("teamname")){

                                        JsonElement teamname = attributes.get("teamname");
                                        if(teamname!=null)
                                            tvTeamHome.setText(""+teamname.getAsString());
                                    }

                                    if(attributes.has("numgoal")){
                                        JsonElement numgoal = attributes.get("numgoal");
                                        if(numgoal!=null){
                                            if(numgoal.getAsString().trim().length()>0) {
                                                goals.append(numgoal.getAsString());
                                                goals.append(" - ");
                                            }
                                            else {
                                                goals.append("0");
                                                goals.append(" - ");
                                            }
                                        }else{
                                            goals.append("0");
                                            goals.append("-");
                                        }
                                    }
                                    if(attributes.has("teamlogo")){
                                        JsonElement teamlogo = attributes.get("teamlogo");
                                        if(teamlogo!=null){
                                            String url = CommonMethods.BASE_URL + teamlogo.getAsString();
                                            if(url!=null){
                                                Uri uri = Uri.parse(url);
                                                ivTeamHome.setImageURI(uri);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //teamaway
                        if(match.has("teamaway")){
                            JsonObject teamaway = match.getAsJsonObject("teamaway");
                            if(teamaway!=null){
                                if(teamaway.has("@attributes")){
                                    JsonObject attributes = teamaway.getAsJsonObject("@attributes");
                                    if(attributes.has("teamname")){
                                        JsonElement teamname = attributes.get("teamname");
                                        if(teamname!=null)
                                            tvTeamAway.setText(""+teamname.getAsString());
                                        else
                                            tvTeamAway.setText("");
                                    }

                                    if(attributes.has("numgoal")){
                                        JsonElement numgoal = attributes.get("numgoal");
                                        if(numgoal!=null){
                                            if(numgoal.getAsString().trim().length()>0)
                                                goals.append(numgoal.getAsString());
                                            else
                                                goals.append("0");
                                        }else{
                                            goals.append("0");
                                        }
                                    }

                                    if(attributes.has("teamlogo")){
                                        JsonElement teamlogo = attributes.get("teamlogo");
                                        if(teamlogo!=null){
                                            String url = CommonMethods.BASE_URL + teamlogo.getAsString();
                                            if(url!=null){
                                                Uri uri = Uri.parse(url);
                                                ivTeamAway.setImageURI(uri);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //goals
                        tvGoal.setText(""+goals.toString());

                        row.setTag(match);*/

                    CommonMethods.applyCustomFont(this.context, row);

                    return row;
                }
            }

            return null;
        }
    }



}//end

